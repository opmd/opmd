SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


DROP TABLE IF EXISTS `affectation_poste`;
CREATE TABLE `affectation_poste` (
  `vehicule` smallint(5) UNSIGNED NOT NULL,
  `poste` tinyint(3) UNSIGNED NOT NULL,
  `personnel` mediumint(8) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `centre`;
CREATE TABLE `centre` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `libelle` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `abreviation` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `position` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `commune`;
CREATE TABLE `commune` (
  `id` int(11) UNSIGNED NOT NULL,
  `libelle` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `code_postal` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `etat_vehicule`;
CREATE TABLE `etat_vehicule` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `libelle` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `couleur_fond` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `couleur_texte` varchar(15) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `etat_vehicule` (`id`, `libelle`, `couleur_fond`, `couleur_texte`) VALUES
(1, 'départ caserne', 'red', 'white'),
(3, 'vl reconnaissance', 'cyan', 'black'),
(5, 'véhicule indispo', 'black', 'white'),
(10, 'Clotûre inter', 'lime', 'black');

DROP TABLE IF EXISTS `grade`;
CREATE TABLE `grade` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `libelle` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `abreviation` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `position` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `grade` (`id`, `libelle`, `abreviation`, `position`) VALUES
(1, 'Général', 'COL', 5),
(2, 'Colonel', 'COL', 10),
(3, 'Lieutenant-colonel', 'LCL', 15),
(4, 'Commandant', 'CDT', 20),
(5, 'Capitaine', 'CPT', 25),
(6, 'Lieutenant', 'LTN', 30),
(7, 'INFIRMIER', 'INF', 7),
(8, 'Adjudant-chef', 'ADC', 40),
(9, 'Adjudant', 'ADJ', 45),
(10, 'Sergent-chef', 'SCH', 50),
(11, 'Sergent', 'SGT', 55),
(12, 'Caporal-chef', 'CCH', 60),
(13, 'Caporal', 'CPL', 65),
(14, 'SP1', 'SP1', 70);

DROP TABLE IF EXISTS `groupes`;
CREATE TABLE `groupes` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` tinyint(4) NOT NULL,
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `groups` (`id`, `nom`) VALUES
(1, 'Gestion'),
(2, 'Administrateur'),
(3, 'Opérateur'),
(4, 'Visiteur');

DROP TABLE IF EXISTS `horaire`;
CREATE TABLE `horaire` (
  `id` int(10) UNSIGNED NOT NULL,
  `personnel` tinyint(3) UNSIGNED NOT NULL,
  `arrivee` int(10) UNSIGNED NOT NULL,
  `depart` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `intervention`;
CREATE TABLE `intervention` (
  `id` int(10) UNSIGNED NOT NULL,
  `codis_lier` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateheure` int(10) UNSIGNED DEFAULT NULL,
  `fin` int(10) UNSIGNED NOT NULL,
  `dernieremodif` int(10) UNSIGNED NOT NULL,
  `nom_appelant` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prenom_appelant` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone_appelant` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nature_id` smallint(5) UNSIGNED NOT NULL,
  `commune` int(10) UNSIGNED NOT NULL,
  `adresse` text COLLATE utf8_unicode_ci NOT NULL,
  `numero_rue` varchar(5) DEFAULT NULL,
  `etage` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appt` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `complement` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message_radio` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `differee` tinyint(1) NOT NULL DEFAULT 0,
  `precision_int` smallint(5) UNSIGNED NOT NULL,
  `etat` int(1) NOT NULL DEFAULT 0,
  `status` tinyint(2) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `intervention_presence_externe`;
CREATE TABLE `intervention_presence_externe` (
  `id_intervention` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_presence_externe` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `quantite` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `materiel`;
CREATE TABLE `materiel` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `libelle` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `intervention` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `etat` tinyint(3) UNSIGNED NOT NULL,
  `centre` tinyint(3) UNSIGNED NOT NULL,
  `groupe` tinyint(4) NOT NULL,
  `visible` tinyint(2) UNSIGNED NOT NULL DEFAULT 1,
  `quantite` int(11) NOT NULL DEFAULT 0,
  `utiliser` int(11) NOT NULL DEFAULT 0,
  `dispo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `nature_intervention`;
CREATE TABLE `nature_intervention` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `libelle` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `nature_intervention` (`id`, `libelle`) VALUES
(1, 'SAP - SECOURS AUX PERSONNES'),
(2, 'AUN - AUTRES NATURES'),
(3, 'FMPA - MANOEUVRE / EXERCICE'),
(4, 'ESS - ESSAI - ALERTE'),
(5, 'INC - HABITATION'),
(6, 'INC - ERP / ETARER AVEC LOCAUX A SOMMEIL'),
(7, 'INC - ERP / ETARER SANS LOCAUX A SOMMEIL'),
(8, 'INC - ETABLSSEMENT SPECIAUX'),
(9, 'INC - MOYENS DE TRANSPORT'),
(10, 'INC - AGRICOLE'),
(11, 'INC - INDUSTRIEL'),
(12, 'INC - VEGETAUX'),
(13, 'INC - ELECTRIQUE EN INTERIEUR / EXTRIEUR'),
(14, 'INC - AUTRES INCENDIES'),
(15, 'INC - ALARM'),
(16, 'INC - PARC DE STATIONNEMENT COUVERT'),
(17, 'AVP - ROUTIER'),
(18, 'AVP - FERROVIAIRE');

DROP TABLE IF EXISTS `permanence`;
CREATE TABLE `permanence` (
  `id` int(10) UNSIGNED NOT NULL,
  `nature` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `debut` int(10) UNSIGNED NOT NULL,
  `fin` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `personnel`;
CREATE TABLE `personnel` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `grade` tinyint(3) UNSIGNED NOT NULL,
  `nom` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `centre` tinyint(3) UNSIGNED NOT NULL,
  `absent` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `poste_vehicule`;
CREATE TABLE `poste_vehicule` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `libelle` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `obligatoire` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `poste_vehicule` (`id`, `libelle`, `obligatoire`) VALUES
(1, 'CA / DIV', 1),
(2, 'COND / PL', 1),
(3, 'EQU 1 / SAP', 0),
(4, 'EQU 2 / SAP', 0),
(5, 'CE 1 / INC', 0),
(6, 'EQU 1 / INC', 0),
(7, 'CE 2 / INC', 0),
(8, 'EQU 2 / INC', 0),
(9, 'CUSAL', 0),
(10, 'SAL', 0),
(11, 'COND / CRT', 0),
(12, 'EQU 1 / CHIM', 0),
(13, 'EQU 2 / CHIM', 0),
(14, 'COND / EPA', 0),
(15, 'CA / MEA', 0),
(16, 'EQU / MEA', 0),
(17, 'EQU / DIV', 0),
(18, 'CA / INC', 0),
(19, 'CA / SAP', 0),
(20, 'CA / SR', 0),
(21, 'COND / SR', 0),
(22, 'COND / VL', 0),
(23, 'COND / CCF', 0),
(24, 'COND / VTU', 0),
(25, 'COND FPT', 0),
(26, 'OCDG', 0),
(27, 'COND / SAP', 0),
(28, 'EQU 1 / SR', 0),
(29, 'REPORTER', 0),
(30, 'RCASAP', 0),
(31, 'REQSAP', 0),
(32, 'RCATE', 0),
(33, 'RCASR', 0),
(34, 'RCAMEA', 0),
(35, 'COND/CCR', 0),
(36, 'ISP', 0),
(37, 'OP1', 0),
(38, 'OP2', 0),
(39, 'OP3', 0),
(40, 'OP4', 0),
(41, 'OP5', 0);

DROP TABLE IF EXISTS `precision_intervention`;
CREATE TABLE `precision_intervention` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `nature` tinyint(3) UNSIGNED NOT NULL,
  `libelle` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `precision_intervention` (`id`, `nature`, `libelle`) VALUES
(1, 1, 'DOULEUR - THORACIQUE'),
(2, 1, 'TS AVEC RISQUE IMMINENT MEDICAMENTEUX'),
(3, 1, 'ACCOUCHEMENT IMMINENT OU EN COURS'),
(4, 1, 'HEMORRAGIES - SEVERES'),
(5, 1, 'BRULURE'),
(6, 1, 'SECTION COMPLETE DE MEMBRE OU DE DOIGT'),
(7, 1, 'ECRASEMENT - ENSEVELISSEMENT'),
(8, 1, 'REPOND PAS AUX APPELS'),
(9, 1, 'CHUTE AVEC IMPOSSIBILITE DE SE LEVER'),
(10, 1, 'CHUTE D\'UNE HAUTEUR > A 3 M'),
(11, 1, 'DEFENESTRATION'),
(12, 1, 'DETRESSE - VP'),
(13, 1, 'NOYADE'),
(14, 1, 'LEVEE DE DOUTE SUAP'),
(15, 1, 'PENDAISON'),
(16, 1, 'ELECTRISATION - FOUDROIEMENT'),
(17, 1, 'TS AVEC RISQUE IMMINENT NON MEDICAMENTEUX'),
(18, 1, 'DUDPICION D\'INTOXICATION AU CO'),
(19, 1, 'INTOXICATION COLLECTIVE'),
(20, 1, 'AUTRES ASSISTANCE SUAP'),
(21, 1, 'RIXE OU ACCIDENT AVEC PLAIE ARME BLANCHE - FEU'),
(22, 1, 'SAUVETAGE DE PERSONNE - MISE EN SECURITE'),
(23, 1, 'ARRET CARDIAQUE / MORT SUBITE'),
(24, 1, 'DETRESSE RESPIRATOIRE'),
(25, 1, 'DETRESSE RESPIRATOIRE - SUSPICION COVID'),
(26, 1, 'ALTERATION DE LA CONSCIENCE'),
(27, 2, 'EN ATTENTE SERVICE EXTERIEUR'),
(28, 2, 'DEPLACEMENT ENGIN'),
(29, 2, 'SERVICE SECURITE'),
(30, 2, 'SOUTIEN PSYCHOLOGIQUE'),
(31, 2, 'ACTIVATION DU CENTRE DE VACCINATION SDIS'),
(32, 2, 'RENFORT SUR CENTRE DE VACCINATION'),
(33, 2, 'REQUISITION MOYENS/PERSONNELS'),
(34, 3, 'MANOEUVRE FMPA'),
(35, 3, 'MANOEUVRE EXERCICE'),
(36, 4, 'ESSAI BIP'),
(37, 4, 'ESSAI ALERTE OD'),
(38, 4, 'ESSAI ALERTE INC'),
(39, 4, 'ESSAI ALERTE SAP'),
(40, 4, 'ESSAI ALERTE RTN'),
(41, 4, 'ESSAI ALERTE SR'),
(42, 2, 'COLONNE DE RENFORT'),
(43, 2, 'PERMANENCE CI'),
(44, 2, 'RENFORT CI'),
(45, 5, 'HABITATION COLLECTIVE'),
(46, 5, 'HABITATION INDIVIDUELLE'),
(47, 5, 'GARAGE / BOX'),
(48, 5, 'CABANE / BARAQUE'),
(50, 5, 'CHEMINEE'),
(51, 5, 'CHAUDIERE'),
(52, 6, 'STUCTURE / ACCUEIL / PERS AGEES / HANDICAPEES'),
(53, 6, 'HOTEL / AUTRES ETABLISSEMENT / HEBERGEMENT'),
(54, 6, 'AUTRES ETABLISSEMENTS HEBERGEMENT'),
(56, 7, 'SALLE CONFERENCE / REUNIONS / SPECTACLES'),
(57, 7, 'MAGASINS CENTRE COMMERCIAUX'),
(58, 7, 'RESTAURANT DEBIT DE BOISSONS'),
(59, 7, 'BIBLIOTHEQUES'),
(60, 7, 'SALLE D\'EXPOSITION'),
(61, 7, 'ETABLISSEMENT SANITAIRES / HOPITAL'),
(62, 7, 'ETABLISSEMENT DE CULTE'),
(63, 7, 'ADMINISTRATION / BANQUES / BUREAUX'),
(64, 7, 'MUSEE'),
(65, 8, 'ETABLISSEMENT DE PLEIN AIR'),
(66, 8, 'CHAPITAUX / TENTES / STRUCTURES GONFLABLES'),
(67, 8, 'GARES'),
(68, 8, 'ETABLISSEMENT FLOTTANTS'),
(69, 9, 'FEU DE VL'),
(70, 9, 'FEU DE PL NON TMD'),
(71, 9, 'FEU DE PL TMD'),
(72, 9, 'FEU DEUX ROUES'),
(73, 9, 'FEU DE BUS'),
(74, 9, 'FEU ENGIN / AGRICOLE - CHANTIER'),
(75, 9, 'FEU DE CAMPING CAR'),
(76, 9, 'FEU AERONEF'),
(77, 9, 'FEU DE BATEAU'),
(78, 9, 'FEU DE LOCOMOTIVE SEULE'),
(79, 9, 'FEU DE TRAIN DE MARCHANDISE'),
(80, 9, 'FEU DE TRAIN DE VOYAGEURS'),
(81, 10, 'FEU DE BATIMENT AGRICOLE'),
(82, 10, 'FEU DE FOURRAGE - EXTERIEUR'),
(83, 10, 'FEU DE SILO'),
(84, 10, 'AUTRE FEU AGRICOLE'),
(85, 11, 'FEU USINE / ENTREPOT'),
(86, 11, 'FEU DE DECHETTERIE'),
(87, 11, 'FEU DE STATION SERVICE'),
(88, 11, 'FEU DE SILO INDUSTRIEL'),
(89, 11, 'FEU DE CHAUFFERIE - INDUSTRIEL'),
(90, 12, 'FEU DE BROUSAILLE'),
(91, 12, 'FEU DE FORET'),
(92, 12, 'FEU DE HAIE'),
(93, 12, 'RECOLTE SUR PIED'),
(94, 12, 'FEU DE CHAUMES'),
(95, 13, 'FEU DE COMPTEUR'),
(96, 13, 'FEU DE TRANSFORMATEUR'),
(97, 13, 'FEU DE CENTRALE ELECTRIQUE A CIEL OUVERT'),
(98, 13, 'FEU DE CENTRALE PHOTOVOLTAIQUE'),
(99, 13, 'AUTRE FEU ELECTRIQUE'),
(100, 13, 'RISQUE ELECTRIQUE'),
(101, 14, 'FUMEE SUSPECT / ODEUR DE BRULE'),
(102, 14, 'DECHARGE / DETRITUS'),
(103, 14, 'FEU DE POUBELLE / CONTAINER'),
(104, 14, 'FEU DE MOBILIER URBAIN'),
(105, 14, 'FEU DE PILE DE BOIS'),
(106, 14, 'AUTRE FEU'),
(107, 15, 'DECTECTEUR INDIVIDUEL DAAF'),
(108, 15, 'DECTECTEUR INDIVIDUEL CO'),
(109, 15, 'DETECTEUR INDIVIDUEL SSI'),
(110, 16, 'FEU DE PARKING SOUTERRAIN'),
(111, 17, 'ACCIDENT VL - PIETON'),
(112, 17, 'ACCIDENT VL SEUL'),
(113, 17, 'ACCIDENT VL - VL'),
(114, 17, 'ACCIDENT VL - PL'),
(115, 17, 'ACCIDENT PL NON TMD'),
(116, 17, 'ACCIDENT PL TMD'),
(117, 17, 'ACCIDENT PL - PL '),
(118, 17, 'ACCIDENT PL / PAS D\'INFO SUR LE CHARGEMENT'),
(119, 17, 'ACCIDENT 2ROUES'),
(120, 17, 'ACCIDENT DE QUAD / BUGGY'),
(121, 17, 'ACCIDENT ENGIN AGRICOLE / CHANTIER'),
(122, 17, 'ACCIDENT DE BUS URBAIN'),
(123, 17, 'ACIDENT DE CAR'),
(124, 17, 'ACCIDENT DE CAMIONETTE / CAMPING CAR'),
(125, 17, 'ACCIDENT TRANSPORT SCOLAIRE'),
(126, 17, 'ACCIDENT TRANSPORT VOYAGEURS'),
(127, 17, 'PAS D\'INFO SUR LE TYPE DE BUS'),
(128, 17, 'ACCIDENT DE VELO'),
(129, 17, 'ACCIDENT DE MOTO'),
(130, 17, 'ACCIDENT DE SCOOTER'),
(131, 5, 'EXPLOSION');

DROP TABLE IF EXISTS `presence_externe`;
CREATE TABLE `presence_externe` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `libelle` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `presence_externe` (`id`, `libelle`) VALUES
(1, 'APRR'),
(2, 'GENDARMERIE'),
(3, 'POLICE'),
(4, 'CRS'),
(5, 'ERDF'),
(6, 'GRDF'),
(7, 'CHAUMONT HABITAT'),
(8, 'VEOLIA EAU'),
(9, 'CHAUMONT POIDS LOURD'),
(10, 'DIRR'),
(11, 'DDE'),
(12, 'LYONNAISE DES EAUX'),
(13, 'MAIRE');

DROP TABLE IF EXISTS `situation`;
CREATE TABLE `situation` (
  `id` int(11) NOT NULL,
  `creer` datetime NOT NULL DEFAULT current_timestamp(),
  `centre` int(11) NOT NULL,
  `data` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `situation` (`id`, `creer`, `centre`, `data`) VALUES
(1, '2017-11-11 21:37:00', 1, 0x4f3a31333a2244617461536974756174696f6e223a353a7b733a32313a220044617461536974756174696f6e00696e74657273223b613a343a7b693a303b733a313a2231223b693a313b733a313a2232223b693a323b733a313a2231223b693a333b733a313a2230223b7d733a32313a220044617461536974756174696f6e006d6f79656e73223b613a343a7b693a303b733a313a2232223b693a313b733a313a2234223b693a323b733a313a2231223b693a333b733a313a2232223b7d733a32343a220044617461536974756174696f6e00706572736f6e6e6573223b613a333a7b693a303b733a313a2231223b693a313b733a313a2231223b693a323b733a393a2244616d65206167c3a9223b7d733a32333a220044617461536974756174696f6e00616e6e696d617578223b613a333a7b693a303b733a313a2231223b693a313b733a313a2231223b693a323b733a373a22756e2063686174223b7d733a32303a220044617461536974756174696f6e007369746573223b613a333a7b693a303b613a323a7b693a303b733a383a22496d6d6575626c65223b693a313b733a31313a22417070617274656d656e74223b7d693a313b613a323a7b693a303b733a383a22496e6e6f6e64c3a9223b693a313b733a31393a22436f757075726520c3a96c6563747269717565223b7d693a323b613a323a7b693a303b733a303a22223b693a313b733a303a22223b7d7d7d);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `groupe_id` tinyint(4) NOT NULL DEFAULT 4,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `connecter` tinyint(1) NOT NULL DEFAULT 0,
  `info` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `users` (`id`, `nom`, `prenom`, `login`, `pass`, `groupe_id`, `active`, `connecter`, `info`) VALUES
(1, 'Administration', 'Opmd', 'opmd', '23164198916d506dae45b3ec1ce7b314', 1, 1, 1, 0);

DROP TABLE IF EXISTS `vehicule`;
CREATE TABLE `vehicule` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `libelle` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `intervention` int(10) UNSIGNED DEFAULT NULL,
  `etat` tinyint(3) UNSIGNED NOT NULL,
  `centre` tinyint(3) UNSIGNED NOT NULL,
  `groupe` tinyint(3) UNSIGNED NOT NULL,
  `visible` tinyint(2) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `affectation_poste`
  ADD PRIMARY KEY (`vehicule`,`poste`);

ALTER TABLE `centre`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `commune`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `etat_vehicule`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `grade`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `groupes`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `horaire`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `intervention`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `intervention_presence_externe`
  ADD PRIMARY KEY (`id_intervention`,`id_presence_externe`);

ALTER TABLE `materiel`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `nature_intervention`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `permanence`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `personnel`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `poste_vehicule`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `precision_intervention`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nature` (`nature`);

ALTER TABLE `presence_externe`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `situation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creer` (`creer`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

ALTER TABLE `vehicule`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `centre`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `commune`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `grade`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

ALTER TABLE `groupes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `groups`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `horaire`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `materiel`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `nature_intervention`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

ALTER TABLE `personnel`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `poste_vehicule`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

ALTER TABLE `precision_intervention`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=457;

ALTER TABLE `presence_externe`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

ALTER TABLE `situation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

ALTER TABLE `vehicule`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
