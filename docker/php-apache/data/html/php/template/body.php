<?php

$u = User::isLogin();
if( $u->getInfo() == 1 && $u->getGroupe() < 3){
	echo "<span id='info' data='1'></span>";
}

?>

<div class="container-fluid">

	<div id="cta_codis" style="text-align:center;">
		
		<div class="row">
			<div class="col-md-12"><h1 style="margin-top:5px"><a class="text-left" style="color:white;text-decoration:none;" href="<?php echo $_SERVER['PHP_SELF']; ?>">OPERATIONS MULTIPLES DIVERSES</a></h1></div>
		</div>

		
		<div class="row">
			<div class="col-md-12" id="navigation">
				
				<?php
					include_once(TEMPLATES.DS.'menu.php');
				?>
				
			</div>
		</div>
		
		

		<div class="row">

			<div class="col-md-12" id="synops">
				<h3 class="text-center bg-primary"><a href="<?= PROJ.DS; ?>synoptique.php" target="_blank" style='color:white; text-decoration:none;'><b>SYNOPTIQUE</b></a></h3>
			</div>
		</div>

		
		<div class="row">
			
			<div class="col-md-3">
				<div id="vehicules"></div>
				<div id="materiels"></div>
				<div id="autre-vehicules"></div>
			</div>
			
			<div class="col-md-6">
				<div id="cartouche"></div>
			</div>
			
			<div class="col-md-3">
				<div id="personnel"></div>
			</div>
		</div>

	</div>


</div>


<div id="bloc-modal"></div>

<div id="bloc-error"></div>

<div id="bloc-modal-depart"></div>

<div id="bloc-recherche"></div>