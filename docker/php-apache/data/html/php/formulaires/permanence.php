<?php
	require_once('../../init.php');

	function convertToTimestamp($s) {
		$dh = explode(' ', $s);
		$d = explode('/', $dh[0]);
		$h = explode(':', $dh[1]);
		return mktime($h[0], $h[1], 0, $d[1], $d[0], $d[2]);
	}

	if(isset($_POST['id'])) {
		//formulaire validé
		$p = new Permanence($_POST['id']);
		$fin = $p->getFin();
		if(empty($fin)) {
			$p->setNature($_POST['nature']);
			$p->setDebut(convertToTimestamp($_POST['debut']));
			if(!empty($_POST['fin'])) {
				$p->setFin(convertToTimestamp($_POST['fin']));
			}
			$p->commit();
		}
		exit;
	}

	$p = isset($_GET['id']) ? new Permanence($_GET['id']) : Permanence::getCurrentPermanence();
	$consult = isset($_GET['id']) && $p->exists();

	$debut_form = $p->exists() ? ($consult ? dateD($p->getDebut()) : dateDH2($p->getDebut())) : dateDH2(time());
	$fin_form = $p->exists() ? dateDH2($p->getFin()) : ''; 
	
	$print = isset($print);
	$disabled = $print ? 'disabled' : '';
?>

<?php if(isset($_GET['search'])) { ?>

<center>
	<form class="form-horizontal" role="form" id="form-permanence" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>?search=1" onsubmit="return false;">
		
		<div class="form-group">
			<div class="col-sm-12">
				<div id="error"></div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-sm-12">
				<h1 class="text-center">Sélectionner une permanence </h1>
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-sm-12">
		<?php
			$ps = Permanence::getAllPermanences();
			if($ps == null) {
				echo "Aucune permanence enregistrée";
			} else {
				echo "<select onchange='consult_permanence(this.value);'  class='form-control'><option></option>";
				foreach($ps as $p) {
					echo "<option value='{$p->getId()}'>{$p->getId()} : {$p->getNature()}</option>";
				}
				echo "</select>";
			}
		?>
			</div>
		</div>
		
	</form>
</center>

<?php } else { ?>


	<form class="form-horizontal" role="form" id="form-permanence" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" onsubmit="return false;">
		
		<div class="form-group">
			<div class="col-sm-12">
				<div id="error"></div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-sm-2">
				<img src="/assets/img/pucelle.png" style="width:62px;height:98px"/>
			</div>
			<div class="col-sm-10">
				<h1 class="text-center" style="margin-top:35px;">GESTION DES RENFORTS</h1>
			</div>
		</div>
		
	
		
		<div class="form-group">
			<label for="nature" class="col-sm-4 control-label">NATURE D'INTERVENTION : </label>
			<div class="col-sm-8">
			  <input type="text" name="nature" maxlength="50" class="form-control" value="Permanace CI" disabled>
			</div>
		</div>
		
		<div class="form-group">
			<label for="id" class="col-sm-4 control-label">N° INTERVENTION :</label>
				<div class="col-sm-8">
					<?php
						if($p->exists()) {
							if(!$consult) echo "<input type='hidden' name='id' value='{$p->getId()}' />";
							echo "<input type='text' class='form-control' value='{$p->getId()}' name='id' disabled style='text-align:center;' />";
						} else {
							echo "<input type='text' class='form-control' name='id' style='text-align:center;' />";
						}
					?>
				</div>
		</div>
			
		<div class="form-group">
		
			<label for="debut" class="col-sm-2 control-label">DÉBUT : </label>
			<div class="col-sm-4">
			  <input type="text" name="debut" maxlength="16" size="16" value="<?php echo $debut_form; ?>" <?php echo $disabled; ?> ondblclick="setDHnow(this);" />
			  <span class="glyphicon glyphicon-time" onclick="setDHnow(this);"></span>
			</div>

			<label for="fin" class="col-sm-2 control-label">FIN : </label>
			<div class="col-sm-4">
			  <input type="text" name="fin" maxlength="16" size="16" value="<?php echo $fin_form; ?>" <?php echo $disabled; ?> ondblclick="setDHnow(this);" />
			  <span class="glyphicon glyphicon-time" onclick="setDHnow($('input[name=fin]'));"></span>
			</div>

		</div>
			
		
		<div class="form-group" id="formTable">
		<div class="col-sm-12">
			
	<?php
		$hs = $p->getHorairesRenforts();

		if($hs != null) { ?>
		
			<table class='table table-bordered table-striped table-hover'>
						<thead>
							<tr style='font-weight:bold;' class="bg-danger">
								<th colspan='6'class='text-center'>RENFORTS AU CENTRE</th>
							</tr>
							<tr style='font-weight:bold;' class='bg-info'>
								<th class='text-center'>PERSONNEL</th>
								<th colspan='2' class='text-center'>DÉBUT</th>
								<th colspan='2' class='text-center'>FIN</th>
								<th class='text-center'>TOTAL</th>
							</tr>
							<tr style='font-weight:bold;' class='bg-info'>
								<th>GRADE NON Prénom (CENTRE)</th>
								<th>DATE</th>
								<th>HEURE</th>
								<th>DATE</th>
								<th>HEURE</th>
								<th>DUREE</th>
							</tr>
						</thead><tbody>
						
				<?php		
				}
      if(is_array($hs)){
      
				for($i=0 ; $i<count($hs) ; $i++) {
				$h = $hs[$i];
				$nom = $h->getPersonnel()->getNomFormate();
				// date-heure arrivée
				$arrivee = ($h->getArrivee() < $p->getDebut() ? $p->getDebut() : $h->getArrivee());
				$dateD = dateD2($arrivee);
				$heureD = dateH2($arrivee);
				// date-heure fin
				$depart = ($h->getDepart() > $p->getFin() ? ($p->getFin() == null ? $h->getDepart() : $p->getFin()) : $h->getDepart());
				$dateF = dateD2($depart);
				$heureF = dateH2($depart); 
				$total = "";
				if($h->getDepart() != null ) {
					$datetime1 = new DateTime(dateDH3($arrivee));
					$datetime2 = new DateTime(dateDH3($depart));
					$interval = $datetime1->diff($datetime2);
					$affH = $interval->format('%hh-%imin');
					$total = ( substr($affH,0,2) == "0h" ) ? substr($affH,3) : $affH;
				} else {
					$total = "En cour"; 
				} 
				
			?>
			<tr>
				<td><?= $nom; ?></td>
				<td><?= $dateD; ?></td>
				<td><?= $heureD; ?></td>
				<td><?= $dateF; ?></td>
				<td><?= $heureF; ?></td>
				<td><?= $total; ?></td>
			</tr>
		<?php } } ?>
				</tbody>
			</table>
	</div>
		</div>
		

	
	<div class="form-group" id="formBtn">
	<hr>
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-2">
	      <button type="submit" class="btn btn-purple" onclick="recherchePermanence();">Rechercher</button>
	    </div>
		<div class="col-sm-2">
	      <button type="submit" class="btn btn-rose" id="Imprimer" onclick="imprimePermanence();">Imprimer</button>
	    </div>
	    <div class="col-sm-4">
	      <button type="submit" class="btn btn-success" id="Valider" onclick="validPermanence();">Valider</button>
	    </div>
	 </div>


</form>

<script type="text/javascript" charset="utf-8">
	
	function recupDatas(){

		var data = {
			id : $("input[name=id]").val(),
			nature : $("input[name=nature]").val(),
			debut : $("input[name=debut]").val(),
			fin : $("input[name=fin]").val()
		}
		
		return data;
	}
	
	function recherchePermanence(){
		creerModal("search-permanences", "PERMANENCE", "rechercher",'/php/formulaires/permanence.php?search=1',"get", 'bg-info',"");
	}
	
	function consult_permanence(id) {
		creerModal("consult-permanences", "CONSULTATION DE PERMANENCE", "consulter",'/php/formulaires/permanence.php?id='+id,"get", 'bg-info',"");
	}
	
	function imprimePermanence(){
		notif("Fonctionnalité prochainnement disponible","info","","");
	}
	
	var presence = $("#formTable").has("#formBtn");
	if( presence.size() == 1 ){
		var copy = $("#formBtn").clone();
		$("#formBtn").remove();
		$("form").append(copy);
	}
	
	
	function validPermanence(){
		var data = recupDatas();
		msg(data);
		var retour = valid_permanence($("#form-permanence"),data);
		if(retour){
			$.post("/php/formulaires/permanence.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de l'enregistrement d'une permanence","danger","","");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre permanence à bien été enregistrée","success","","");
					}
					
				});
		}
	}
		
</script>

<?php  } ?>
	
