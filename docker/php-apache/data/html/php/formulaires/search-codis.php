<?php
require_once('../../init.php');

?>

<div id="rechercher-codis">
    <form class="form-horizontal" role="form" name="search_codis" method="POST" enctype="multipart/form-data" action="">

        <div class="form-group">
            <label class="col-md-3 col-md-offset-2 control-label">NUMÉRO CODIS : </label>
            <div class="col-md-5">
                <input class="form-control input-md" name="codis" type="number" min="0">
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-12 text-center"><button type="button" id="search-num-codis" class="btn btn-success">RECHERCHER</button></div>
            </div>
        </div>

    </form>
</div>

<script type="text/javascript">
		
    $(document).ready(function(e){
        $("#search-num-codis").on('click',function(){
            var codis = $("input[name=codis]").val();
            if(codis.length < 1 || codis == ""){
                notif("Veuillez saisir un numéro codis valide", "danger", "", "");
            } else {
                $.post("/php/requetes/recherche_codis.php",{num : codis}).always(function(data){
                    var html = "<h3 style='color:red' class='text-center'>Veuillez saisir un numéro codis</h3>";
                    if(data.msg == "ok"){
                        if(data.id < 1){
                            notif("Aucune Intervention trouvée avec ce numéro codis.", "danger", "", "");
                        } else {
                            splash_recap(data.id);
                        }
                    } else {
                        notif(data.msg, "danger", "", "");
                    }
                });
            }
        });
    });

</script>
