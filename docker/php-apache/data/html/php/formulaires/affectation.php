<?php
	require_once('../../init.php');

	$curr_c = isset($_POST['centre']) ? max($_POST['centre']) : -1;

	$nb_col = 2;
?>

<script>
	function submit2() {
		confirm = false;
		var centres = new Array();
		var tabCentres = $('select[name="centre[]"]').each(function(){ $(this).val()});
		var i
		for( i = 0; i < tabCentres.length; i++ ){
			centres[i] = $(tabCentres[i]).val();
		}
		var donne;
		$.post('/php/formulaires/affectation.php',{centre : centres, centre2 : 1, valid : "", raz : ""},function (data){
			$("#bloc-modal").empty();
		$('.modal-backdrop').remove();
		creerModal("affectations-1", "TABLEAU DE GARDE", "affectation-1",'',"", 'bg-info',data);
		
		});
	/*	*/
	}
	
	$(function() {
		// tous les select du tableau de garde ont la même largeur
		selects = $("#form-affectation select[name^=affs]");
		for(i=0 ; i<selects.length ; i++) {
			if(!selects.get(i).value.length) {
				w = $("#form-affectation select[name='"+selects.get(i).name+"']").css('width');
				$("#form-affectation select[name^=affs]").css('width', w);
				break;
			}
		}
	});
</script>

<center>
	<form class="form-horizontal" role="form" id="form-affectation" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>"  onsubmit="return false">
	
		<div class="form-group">
	  	<div class="col-sm-12">
	  		<div id="error" />
				<input type="hidden" name="centre2" value="<?php echo $curr_c; ?>" />
	  	</div>
	  </div>
		
	<div class="form-group">
	    <label for="centre" class="col-sm-1 control-label">CENTRE</label>
	    <div class="col-sm-11">
		<!-- listes déroulantes supérieures -->
	      <select name="centre[]" onchange="submit2();" class="form-control">
					<option value="0"></option>
					<?php
						$cs = Centre::getAllCentres();
						if($cs != null) {
							foreach($cs as $c) {
								echo "<option value='{$c->getId()}'>{$c->getLibelle()}</option>";
							}
						}
					?>
				</select>
	    </div>
	  </div>
		
	
	<div class="form-group">

		<!-- tableau de garde du centre sélectionné -->
		<?php
			$c = new Centre($curr_c);
			if($c->exists()) {
				$vs = $c->getAllVehiculesAffectables();

				if($vs == null) {
					echo "<div class='text-danger'>Ce centre ne dispose pas de véhicule ou ceux-ci ne peuvent accueillir du personnel.</div>";
				} else {
					$aff = array();
					foreach($vs as $v) {
						$aff[$v->getId()] = $v->getAffectations(true);
					}

					$posts = PosteVehicule::getAllPostesVehicule();
					$pp = Personnel::getAllPersonnelPresentDispo();
					$optionVide = "<option value=''></option>";
					$options = $optionVide;
					if($pp != null) foreach($pp as $p) {
						$options .= "<option value='{$p->getId()}'>{$p->getNomFormate()}</option>";
					}
					
					echo '<div class="container-fluid">';
					echo "<hr>";
					echo '<div class="row">';
					echo '<div class="col-sm-12">';
					echo "<div><b>{$c->getLibelle()}</b></div>";
					echo "</div>";
					echo "</div>";
					echo "<hr>";
					echo '<div class="row">';
					echo '<div class="col-sm-12">';
					echo '<div class="row">';
					
					for($c=0 ; $c<$nb_col ; $c++) {
						
						
						for($i=$c ; $i<count($vs) ; $i+=$nb_col) {
							$v = $vs[$i];
								echo '<div class="col-sm-6">';
								echo "<label class='control-label'>{$v->getLibelle()}</label>";
								
							foreach($aff[$v->getId()] as $cur_post => $cur_per) {
							
								$p = new Personnel($cur_per);
								$contenu = $p->isEnIntervention() ? $optionVide."<option value='{$p->getId()}'>{$p->getNomFormate()}</option>" : $options;
								$couleur = $p->exists() ? $p->getCouleur() : 'none';
								$name = "affs[{$v->getId()}][{$cur_post}]";
								echo '<div class="row">';
									echo '<div class="col-sm-3">';
										echo "<label class='control-label'>{$posts[$cur_post]->getLibelle()}</label>";
									echo '</div>';
									echo '<div class="col-sm-9">';
										echo "<select name='{$name}' style='background-color: {$couleur}' class='form-control' onchange=\"modif($(this).attr('name'),$(this).val());\">{$contenu}</select>";
									echo '</div>';
								echo '</div>';
							}
							//echo "</div>";
							echo "</div>";
						}
						
						
					}
					echo "</div>";
					echo "</div>";
					echo "</div>";
					echo "</div>";
				}
			}
		?>
		
	  </div>

	</form>
	
	<script>
	<?php
		if(isset($aff)) foreach($aff as $v_id => $af) foreach($af as $post_id => $per_id) if($per_id != 0) {
			echo "$(\"select[name='affs[{$v_id}][{$post_id}]']\").val({$per_id});";
		}
	?>
	</script>
	
	<script type="text/javascript" charset="utf-8">
		
		function modif(name,val){
			var suppr = name.substr(5);
			var modif = suppr.replace("][","-");
			var coupe = modif.replace("]","");
			var tab = coupe.split('-');
			var idveh = tab[0];
			var idposte =  tab[1];
			var idpers = val;
			//var texte = "Le vehicule "+idveh+" et le poste "+idposte+" avec comme personnel "+idpers;
			//msg(texte);
			$.post("/php/requetes/affectation.php",{poste:idposte,vehicule:idveh,personnel:idpers}).done(function(arg){
				if(arg.reponse == "ok"){
					notif("Votre affectation de poste à bien été ajouter à la liste","success","","");
				} else {
					notif("Erreur lors de l'affectation du poste","danger","","");
				}
			});
			updateIntelligent();
		}

	</script>
	
</center>

<style>
.form-control {
	color: #000000;
    font-weight: bold;
}
</style>