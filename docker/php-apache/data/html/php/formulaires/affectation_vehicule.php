<?php
	require_once('../../init.php');

?>



<center>
	<form class="form-horizontal" role="form" id="form-affectation-vehicule" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" onsubmit="return false;">
	
		<div class="form-group">
	  	<div class="col-sm-12">
	  		<input type="hidden" name="valid" />
			<div id="error" /><br />
	  	</div>
	  </div>
	  
	  
		
		<?php
			$v = new Vehicule(@$_GET['id']);
			$aff = $v->getAffectations(true);
			$postes = PosteVehicule::getAllPostesVehicule();
		
			echo '<div class="form-group">';
				echo '<div class="col-sm-12">';
					echo "<label class='control-label'>{$v->getLibelle()} {$v->getCentre()->getAbreviation()}</label>";
				echo '</div>';
			echo '</div>';
				
			//echo "<b>{$v->getLibelle()} {$v->getCentre()->getAbreviation()}</b><br /><br />";
			
			$optionVide = "<option value=''></option>";
			$pp = Personnel::getAllPersonnelPresentDispo();
			$optionsAutresPersonnes = '';
			if($pp != null) foreach($pp as $p) {
				$optionsAutresPersonnes .= "<option value='{$p->getId()}'>{$p->getNomFormate()}</option>";
			}
			
			if($aff != null){
			foreach($aff as $cur_post => $cur_pers) {
				$p = new Personnel($cur_pers);
				$options = $optionVide;
				if($p->exists()) $options .= "<option value='{$p->getId()}'>{$p->getNomFormate()}</option>";
				$options .= $optionsAutresPersonnes;
				$name = "affs[{$v->getId()}][{$cur_post}]";
				echo '<div class="container-fluid">';
				echo '<div class="form-group">';
					echo '<div class="col-sm-1 col-sm-offset-2">';
						echo "<label class='control-label'>{$postes[$cur_post]->getLibelle()}</label>";
					echo '</div>';
					echo '<div class="col-sm-7">';
						echo "<select name='{$name}' class='form-control' onchange=\"modif($(this).attr('name'),$(this).val());\">{$options}</select>";
					echo '</div>';
				echo '</div>';
			echo '</div>';
			}
			}

		?>
	<hr>	
	<div class="form-group">
	    <div class="col-sm-offset-4 col-sm-4">
	      <button type="button" class="btn btn-success" data-dismiss="modal">Valider</button>
	    </div>
	</div>
		
		
	</form>
	
	<script>
	<?php
		if(isset($aff)) foreach($aff as $post_id => $per_id) if($per_id != 0) {
			echo "$(\"select[name='affs[{$v->getId()}][{$post_id}]']\").val({$per_id});";
		}
	?>
	</script>
	
	<script type="text/javascript" charset="utf-8">
		
		function modif(name,val){
			var suppr = name.substr(5);
			var modif = suppr.replace("][","-");
			var coupe = modif.replace("]","");
			var tab = coupe.split('-');
			var idveh = tab[0];
			var idposte =  tab[1];
			var idpers = val;
			//var texte = "Le vehicule "+idveh+" et le poste "+idposte+" avec comme personnel "+idpers;
			//msg(texte);
			$.post("/php/requetes/affectation.php",{poste:idposte,vehicule:idveh,personnel:idpers}).always(function(arg){
				if(arg.reponse == "ok"){
					updateIntelligent();
					//viderModal();
					notif("Votre affectation de poste à bien été ajouter à la liste","success","","");
				} else {
					notif("Erreur lors de l'affectation du poste","danger","","");
				}
			});
		}

	</script>
	
</center>
