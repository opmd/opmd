<?php
	require_once('../../init.php');
	
	if(isset($_POST['id'])) {
		//formulaire validé
		$g = new Groupes($_POST['id']);
		if($_POST['del']) {
			$g->delete();
			print("ok");
		} else {
			$g->setLibelle(strtoupper($_POST['libelle']));
			$g->setPosition($_POST['position']);
			$g->setType($_POST['type']);
			$g->commit();
		}
		exit;
	}
	
	$g = new Groupes(@$_GET['id']);

?>



	<form class="form-horizontal" role="form" id="form-groupe" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" onsubmit="return false;">
		
	  <div class="form-group">
	  	<div class="col-sm-12">
	  		<div id="error" />

				<input type="hidden" name="id" value="<?php echo $g->getId(); ?>" />
				<input type="hidden" name="del" />
	  	</div>
	  </div>
		
	  <div class="form-group">
	    <label for="libelle" class="col-sm-2 control-label">LIBELLÉ</label>
	    <div class="col-sm-10">
	      <input type="text" name="libelle" maxlength="50" class="form-control" value="<?php echo $g->getLibelle(); ?>">
	    </div>
	  </div>
		
		<div class="form-group">
	    <label for="type" class="col-sm-2 control-label">TYPE</label>
	    <div class="col-sm-10">
	      <select name="type" class="form-control">
					<?php if( $g->getId() != -1 && $g->getType() == 1) : ?>
	      	<option value="1" selected="selected">Véhicules</option>
					<option value="2">Matériel</option>
					<?php elseif( $g->getId() != -1 && $g->getType() == 2) : ?>
					<option value="1">Véhicules</option>
					<option value="2" selected="selected">Matériel</option>
					<?php else : ?>
					<option value="1">Véhicules</option>
					<option value="2">Matériel</option>
					<?php endif; ?>
				</select>
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="position" class="col-sm-2 control-label">POSITION</label>
	    <div class="col-sm-10">
	    	<input type="number" name="position" maxlength="50" class="form-control" disabled="disabled" value="<?php echo Groupes::getLastPosition(); ?>">
	    </div>
	  </div>
	  	  
	  
<?php if(isset($_GET['mode']) && $_GET['mode'] == 'add') : ?>
	  
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-8">
	      <button type="submit" class="btn btn-success" id="addGroupe">Ajouter</button>
	    </div>
	  </div>
	  

	
<?php elseif(isset($_GET['mode']) && $_GET['mode'] == 'mod') : ?>
	
	<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-2">
	      <button type="submit" class="btn btn-success" id="saveGroupe">Sauvegarder</button>
	    </div>
	    <div class="col-sm-6">
	      <button type="submit" class="btn btn-danger" id="delGroupe">Supprimer</button>
	    </div>
	  </div>
	
<?php endif; ?>

	</form>
	
	
	<script type="text/javascript" charset="utf-8">
	
	function recupDatas(){

		var data = {
			id : $("input[name=id]").val(),
			libelle : $("input[name=libelle]").val(),
			position : $("input[name=position]").val(),
			type : $("select[name=type]").val(),
			del : $("input[name=del]").val()
		}
		
		return data;
	}
	
	$("#addGroupe").click(function(){
	
		var data = recupDatas();
		var retour = valid_groupe($("#form-groupe"),data);
		if(retour){
			$.post("/php/formulaires/groupe.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de l'ajout du groupe","danger","","");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre groupe à bien été ajouter à la liste","success","","");
					}
					
				});
		}
	});
	
	$("#saveGroupe").click(function(){
		var data = recupDatas();
		var retour = valid_groupe($("#form-groupe"),data);
		if(retour){
			$.post("/php/formulaires/groupe.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de la modification du groupe","danger","","");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre groupe à bien été modifié et mis à jour dans la liste","success","","");
					}
					
				});
		}
	});
	
	$("#delGroupe").click(function(){
		delGroupes($("input[name=id]").val(), "groupe");
	});
	
	
		
	</script>







