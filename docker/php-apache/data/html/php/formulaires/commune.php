<?php
	require_once('../../init.php');
	
	if(isset($_POST['id'])) {
		//formulaire validé
		$c = new Commune($_POST['id']);
		$pos = Commune::getLastPosition();
		if($_POST['del']) {
			$c->delete();
			print("ok");
		} else {
			
			$c->setLibelle(strtoupper($_POST['libelle']));
			$c->setCodePostal($_POST['cp']);
			$c->setPosition($pos);
			$c->setVisible($_POST['visible']);
			$c->commit();
		}
		exit;
	}
	
	$c = new Commune(@$_GET['id']);


?>



	<form class="form-horizontal" role="form" id="form-commune" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" onsubmit="return false;">
		
	  <div class="form-group">
	  	<div class="col-sm-12">
	  		<div id="error" />

				<input type="hidden" name="id" value="<?php echo $c->getId(); ?>" />
				<input type="hidden" name="del" />
	  	</div>
	  </div>
		
	  <div class="form-group">
	    <label for="libelle" class="col-sm-2 control-label">LIBELLÉ</label>
	    <div class="col-sm-10">
	      <input type="text" name="libelle" maxlength="50" class="form-control" onchange="verifExist($(this).val());" value="<?php echo $c->getLibelle(); ?>">
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="cp" class="col-sm-2 control-label">CODE POSTAL</label>
	    <div class="col-sm-10">
	      <input type="text" name="cp" maxlength="5" class="form-control" value="<?php echo $c->getCodePostal(); ?>">
	    </div>
	  </div>
	  
		<div class="form-group">
			<label for="position" class="col-sm-2 control-label">POSITION</label>
			<div class="col-sm-10">
				<input type="number" name="position" maxlength="50" class="form-control" disabled="disabled" value="<?= ($c->getPosition()) ? $c->getPosition() : Commune::getLastPosition(); ?>">
			</div>
		</div>
		
		<div class="form-group">
			<label for="visible" class="col-sm-2 control-label">VISIBLE</label>
	  		<div class="col-sm-10">
			    <label class="radio-inline">
					<input type="radio" name="visible" value="1" onclick="visibility(1);" <?= ($c->getVisible() == 1) ? "checked" : ""; ?>> OUI
				</label>
				<label class="radio-inline">
					<input type="radio" name="visible" value="0" onclick="visibility(0);" <?= ($c->getVisible() == 0) ? "checked" : ""; ?>> NON
				</label>
			</div>
		</div>
	  
	  
<?php if(isset($_GET['mode']) && $_GET['mode'] == 'add') : ?>
	  
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-8">
	      <button type="submit" class="btn btn-success" id="addCommune">Ajouter</button>
	    </div>
	  </div>
	  

	
<?php elseif(isset($_GET['mode']) && $_GET['mode'] == 'mod') : ?>
	
	<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-2">
	      <button type="submit" class="btn btn-success" id="saveCommune">Sauvegarder</button>
	    </div>
	    <div class="col-sm-6">
	      <button type="submit" class="btn btn-danger" id="delCommune">Supprimer</button>
	    </div>
	</div>
	
<?php endif; ?>

	</form>
	
	
	<script type="text/javascript" charset="utf-8">
	
	var voir = <?= ($c->getVisible()) ? $c->getVisible() : 1; ?>;
	
	function visibility(val){
		voir = val;
	}
	
	function recupDatas(){

		var data = {
			id : $("input[name=id]").val(),
			libelle : $("input[name=libelle]").val(),
			cp : $("input[name=cp]").val(),
			position : $("input[name=position]").val(),
			visible : voir,
			del : $("input[name=del]").val()
		}
		
		return data;
	}
	
	$("#addCommune").click(function(){
		
		var data = recupDatas();
		var retour = valid_commune($("#form-commune"),data);
		if(retour){
			$.post("/php/formulaires/commune.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de l'ajout d'une commune","danger","","");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre commune à bien été ajouter à la liste","success","","");
					}
					
				});
		}
	});
	
	$("#saveCommune").click(function(){
		var data = recupDatas();
		var retour = valid_commune($("#form-commune"),data);
		if(retour){
			$.post("/php/formulaires/commune.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de la modification d'une commune","danger","","");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre commune à bien été modifié et mis à jour dans la liste","success","","");
					}
					
				});
		}
	});
	
	$("#delCommune").click(function(){
		delCommunes($("input[name=id]").val(), "commune");
	});
	
	function verifExist(nom){
		$.post("/php/requetes/verif_unique.php",{'libelle' : nom, type : "commune"})
			.always(function(arg) {
				if( arg.trim() == "erreur"){
					notif("Attention ! la commune existe déjà, merci d'en saisir une autre","warning","","");
					$("input[name=libelle]").val("");
				} else {
					if( arg.trim() != "ok"){
						viderModal();
						$("#bloc-error").append(arg);
						$("input[name=libelle]").val("");
					}
				}
			});
	}
		
	</script>










