<?php
	$filename = "consignes.txt";

	if(isset($_POST['consignes'])) {
		//formulaire validé
		$f = fopen($filename, 'w');
		
		if($f !== false) {
			fwrite($f, stripcslashes($_POST['consignes']));
		}
		
	} else {
		$consignes = (file_exists($filename) ? file_get_contents($filename) : '');
	}
?>


<form class="form-horizontal" role="form" id="form-consignes">
	
	<div class="form-group">
	  	<div class="col-sm-12">
	  		<textarea name="consignes" style="width:100%; height:50%;border-radius:20px;"><?php echo @$consignes; ?></textarea>
	  	</div>
	  </div>
	
	<div class="form-group">
	    <div class="col-sm-offset-4 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-2">
	      <button type="button" class="btn btn-success" id="saveConsigne">Sauvegarder</button>
	    </div>
	</div>
	  
</form>

<script type="text/javascript" charset="utf-8">

$("#saveConsigne").click(function(){
	var data = { consignes : $("textarea[name=consignes]").val()};
	$.post("/php/formulaires/consignes.php",data)
		.always(function(arg) {
			var n = arg.search("Notice");
			if( n > 0 ) {
				arg = "";
			}

			if( trim(arg) != ""){
				viderModal();
				$("#bloc-error").append(arg);
				notif("Erreur lors de la modification de la consigne","danger","","");
				
			} else {
				viderModal();
				updateIntelligent();
				notif("Votre consigne à bien été modifié et mis à jour","success","","");
			}
			
		});
	
	});

</script>