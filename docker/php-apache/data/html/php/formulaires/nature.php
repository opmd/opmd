<?php
	require_once('../../init.php');

	if(isset($_POST['id'])) {
		//formulaire validé
		$n = new NatureIntervention($_POST['id']);
		if($_POST['del']) {
			$n->delete();
			print("ok");
		} else {
			$n->setLibelle(strtoupper($_POST['libelle']));
			$n->commit();
		}
		exit;
	}

	$n = new NatureIntervention(@$_GET['id']);

?>


	<form class="form-horizontal" role="form" id="form-nature" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" onsubmit="return false;">
		
	  <div class="form-group">
	  	<div class="col-sm-12">
	  		<div id="error" />

				<input type="hidden" name="id" value="<?php echo $n->getId(); ?>" />
				<input type="hidden" name="del" />
	  	</div>
	  </div>
		
	  <div class="form-group">
	    <label for="libelle" class="col-sm-2 control-label">LIBELLE</label>
	    <div class="col-sm-10">
	      <input type="text" name="libelle" class="form-control" value="<?php echo $n->getLibelle(); ?>">
	    </div>
	  </div>
	  
	  
	  
<?php if(isset($_GET['mode']) && $_GET['mode'] == 'add') : ?>
	  
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-8">
	      <button type="submit" class="btn btn-success" id="addNature">Ajouter</button>
	    </div>
	  </div>
	  

	
<?php elseif(isset($_GET['mode']) && $_GET['mode'] == 'mod') : ?>
	
	<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-2">
	      <button type="submit" class="btn btn-success" id="saveNature">Sauvegarder</button>
	    </div>
	    <div class="col-sm-6">
	      <button type="submit" class="btn btn-danger" id="delNature">Supprimer</button>
	    </div>
	  </div>
	
<?php endif; ?>

	</form>
	
	
	<script type="text/javascript" charset="utf-8">
	
	function recupDatas(){

		var data = {
			id : $("input[name=id]").val(),
			libelle : $("input[name=libelle]").val(),
			del : $("input[name=del]").val()
		}
		
		return data;
	}
	
	$("#addNature").click(function(){
		var data = recupDatas();
		var retour = valid_nature($("#form-nature"),data);
		if(retour){
			$.post("/php/formulaires/nature.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de l'ajout d'une nature","danger","","");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre nature a bien été ajouter à la liste","success","","");
					}
					
				});
		}
	});
	
	$("#saveNature").click(function(){
		var data = recupDatas();
		var retour = valid_nature($("#form-nature"),data);
		if(retour){
			$.post("/php/formulaires/nature.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de la modification d'une nature","danger","","");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre nature a bien été modifié et mis à jour dans la liste","success","","");
					}
					
				});
		}
	});
	
	$("#delNature").click(function(){
		delNatures($("input[name=id]").val(), "nature");
	});
		
	</script>