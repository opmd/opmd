<?php
require_once('../../init.php');


if(isset($_POST['date'])){
    $date = $_POST['date'];
    if(!empty($date)){
        $tmp = explode("/",$date);
        if(count($tmp) == 3){
            $jour = intval($tmp[0]);
            $mois = intval($tmp[1]);
            $an = intval($tmp[2]);

            if($jour > 0 && $jour < 31){
                if($mois > 0 && $mois < 13){
                    if($an >= 2017){
                        $jour = ($jour < 9) ? "0".$jour : $jour;
                        $mois = ($mois < 9) ? "0".$mois : $mois;
                        $date = $an . "-" . $mois . "-" . $jour ." 00:00:00";
                        $all = Situation::getAllSituationsByDate($date);
                        if($all != null){
                            $nb = count($all);
                            if($nb > 1){
                                $html = "<h3 style='color:green' class='text-center'>$nb résultat trouvé pour la date demandée</h3>";
                            } else {
                                $html = "<h3 style='color:green' class='text-center'>$nb résultats trouvé pour la date demandée</h3>";
                            }
                            $html .= "<ul class='list-group row text-center'>";
                            foreach($all as $s){
                                $html .= "<li class='points list-group-item col-md-6 list-group-item-info' data-id='".$s->getId()."'>";
                                $html .= "Point de situation N° <b>".$s->getId()."</b> fait le <b>".$s->getCreer()."</b></li>";
                            }
                            $html .= "</ul>";
                            $html .= "<script type='text/javascript'>";
                            $html .= "$(document).ready(function(){ ";
                            $html .= "$('.points').on('click',function(){";
                            $html .= "var pid = $(this).attr('data-id');";
                            $html .= "var html = '';";
                            $html .= " $.post('/pdf/template/point_de_situation.php',{id:pid}).always(function(data){";
                            $html .= "html = data;";
                            $html .= "getPdf(html);";
                            $html .= "});";
                            $html .= "});";
                            $html .= "function getPdf(data){";
                            $html .= " $.post('/php/requetes/pdf_situation.php',{code:data}).always(function(file){";
                            $html .= "window.open(file);";
                            $html .= "});";
                            $html .= "}";
                            $html .= "});";
                            $html .= "</script>";
                            $html .= "<style>.points{cursor:pointer;} .points:hover{background-color:#dff0d8;}</style>";
                        } else {
                            $html = "<h3 style='color:green' class='text-center'>Aucun résultat trouvé pour la date demandée</h3>";
                        }
                    } else {
                        $html = "<h3 style='color:red' class='text-center'>L'année ne peut pas être plus petit que 2017</h3>";
                    }
                } else {
                    $html = "<h3 style='color:red' class='text-center'>Le mois commence à 1 et ne peut pas être plus grand que 13</h3>";
                }
            } else {
                $html = "<h3 style='color:red' class='text-center'>Le jour commence à 1 et ne peut pas être plus grand que 31</h3>";
            }
        } else {
            $html = "<h3 style='color:red' class='text-center'>Veuillez saisir une date au format JOUR(1-31) / MOIS(1-12) / ANNEE(2017 et +)</h3>";
        }
    } else {
        $html = "<h3 style='color:red' class='text-center'>Veuillez saisir une date au format JOUR(1-31) / MOIS(1-12) / ANNEE(2017 et +)</h3>";
    }
} else {
    $html = "<h3 style='color:red' class='text-center'>Erreure sur la date demandée</h3>";
}

echo $html;

?>
