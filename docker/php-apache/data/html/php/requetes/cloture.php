<?php
require_once('../../init.php');
if(isset($_POST['id'])) {
	$inter = new Intervention($_POST['id']);
	if($inter->exists()) {
		$inter->setVehicules(array());
		$inter->setMateriels(array());
    Materiel::updateInter(null, $inter->getId());
		$inter->setFin(time());
		$inter->setStatus(10);
		$inter->setDifferee(0);
		$inter->setEtat(4);
		$inter->commit();
	}
}

if(isset($_GET['id'])) {
	$inter = new Intervention($_GET['id']);
}

?>
<!--
<script type="text/javascript" src="js/jquery-2.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="functions.js"></script>
<script type="text/javascript" src="scripts/functions.js"></script>
<script type="text/javascript" src="js/unixvers.js"></script>
	-->
<h2 class="text-danger">Attention ! vous aller cloturer une Intervention</h2>

<p>Pour valider la clôture et afin d'éviter toute erreur, merci d'indiquer le numéro CODIS de cette intervention que vous aller clôturer.</p>
	
<form class="form-horizontal" role="form" name="cloture-inter" method="POST" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']; ?>">

<div class="row">
	<div class="col-md-12">
		  
		  <div class="form-group">
		    <label for="id" class="col-sm-2 control-label">NUMERO CODIS</label>
		    <div class="col-sm-10">
		      <input type="number" class="form-control input-sm" name="id" data="<?= $inter->getId(); ?>" onchange="validCodis($(this).val());">
		      <div id="error-codis"> </div>
			  <div id="error-codis-time"> </div>
		    </div>
		  </div>
		  
	</div>
 </div>

 <div class="form-group">
 	<div class="row">
		    <div class="col-sm-offset-3 col-sm-3">
		      <button type="button" class="btn btn-primary" id="cancel">Annuler</button>
		    </div>
		    <div class="col-sm-3">
				<button type="button" class="btn btn-danger" id="termine">Cloturer</button>
		    </div>
		</div>
	</div>
</form>
		

<script>

		
		$("#termine").on('click',function(e){
			e.preventDefault();
			var Inter = new Object();
			Inter.id = $('input[name=id]').val();
			Inter.verifId = $('input[name=id]').attr('data');
			if(Inter.id != Inter.verifId){
				$('input[name=id]').parent().addClass('has-error');
				$('input[name=id]').attr('disabled','disabled');
				$('#error-codis').text("Ce numéro CODIS ne correspond pas à l'intervention que vous voulez clôturer !").addClass("text-danger");
				$('#error-codis-time').text("Cette fenêtre va se fermer dans 3 seconde").addClass("text-danger");
				setTimeout(function(){erreurCodis(Inter.verifId)},3000);
			} else {
				validFormTerm(Inter);
			}
			
		})
		
		$("#cancel").on('click',function(e){
			e.preventDefault();
			viderModal();
		})
		
		function validFormTerm(form){
			viderModal();
			$.post("/php/requetes/cloture.php",form,function(arg){
				updateIntelligent();
			})
		}
		
		function validCodis(id){
			$.get("/php/requetes/codis.php?id="+id+'&type=valid', function(arg){ 
				if(arg.reponse == "ok") {
					$('input[name=id]').parent().removeClass('has-error');
					$('#error-codis').text("").removeClass("text-danger");
				} else {
					$('input[name=id]').parent().addClass('has-error');
					$('input[name=id]').attr('disabled','disabled');
					$('#error-codis').text("Ce numéro CODIS est invalide car il n'existe pas !").addClass("text-danger");
					$('#error-codis-time').text("Cette fenêtre va se fermer dans 3 seconde").addClass("text-danger");
					setTimeout(function(){erreurCodis(id)},3000);
				}
			});
		}
		
		function erreurCodis(id){
			viderModal();
			viderModal();
			creerModal("finir", "CLOTURER UNE INTERVENTION", "valid-finir",'/php/requetes/cloture.php?id='+id,"get", 'bg-danger',"");
		}
	
</script>
