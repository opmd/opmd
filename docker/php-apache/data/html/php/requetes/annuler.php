<?php
require_once('../../init.php');
if(isset($_POST['id'])) {
	$inter = new Intervention($_POST['id']);
	if($inter->exists()) {
		$inter->setVehicules(array());
		$inter->setMateriels(array());
    Materiel::updateInter(null, $inter->getId());
		$inter->setDifferee(0);
		$inter->setFin(time());
		$inter->setStatus(10);
		$inter->setEtat(5);
		$inter->commit();
	}
}

if(isset($_GET['id'])) {
	$inter = new Intervention($_GET['id']);
}

?>

<h2 class="text-danger">Attention ! vous aller annuler une Intervention</h2>

<p>Pour valider l'annulation et afin d'éviter toute erreur, merci d'indiquer le numéro CODIS de cette intervention que vous aller annuler.</p>
	
<form class="form-horizontal" role="form" name="annuler-inter" method="POST" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']; ?>">

<div class="row">
	<div class="col-md-12">
		  
		  <div class="form-group">
		    <label for="id" class="col-sm-2 control-label">NUMERO CODIS</label>
		    <div class="col-sm-10">
		      <input type="number" class="form-control input-sm" name="id" data="<?= $inter->getId(); ?>" onchange="validCodis($(this).val());">
		      <div id="error-codis"> </div>
			  <div id="error-codis-time"> </div>
		    </div>
		  </div>
		  
	</div>
 </div>

 <div class="form-group">
 	<div class="row">
		    <div class="col-sm-offset-3 col-sm-3">
		      <button type="button" class="btn btn-primary" id="cancel">Retour</button>
		    </div>
		    <div class="col-sm-3">
				<button type="button" class="btn btn-danger" id="stop">Annuler</button>
		    </div>
		</div>
	</div>
</form>
		

<script>

		
		$("#stop").on('click',function(e){
			e.preventDefault();
			var Inter = new Object();
			Inter.id = $('input[name=id]').val();
			Inter.verifId = $('input[name=id]').attr('data');
			if(Inter.id != Inter.verifId){
				$('input[name=id]').parent().addClass('has-error');
				$('input[name=id]').attr('disabled','disabled');
				$('#error-codis').text("Ce numéro CODIS ne correspond pas à l'intervention que vous voulez annuler !").addClass("text-danger");
				$('#error-codis-time').text("Cette fenêtre va se fermer dans 3 seconde").addClass("text-danger");
				setTimeout(function(){erreurCodis(Inter.verifId)},3000);
			} else {
				validFormAnnul(Inter);
			}
			
		})
		
		$("#cancel").on('click',function(e){
			e.preventDefault();
			viderModal();
		})
		
		function validFormAnnul(form){
			viderModal();
			$.post("/php/requetes/annuler.php",form,function(arg){
				updateIntelligent();
			})
		}
		
		function validCodis(id){
			$.get("/php/requetes/codis.php?id="+id+'&type=valid', function(arg){ 
				if(arg.reponse == "ok") {
					$('input[name=id]').parent().removeClass('has-error');
					$('#error-codis').text("").removeClass("text-danger");
				} else {
					$('input[name=id]').parent().addClass('has-error');
					$('input[name=id]').attr('disabled','disabled');
					$('#error-codis').text("Ce numéro CODIS est invalide car il n'existe pas !").addClass("text-danger");
					$('#error-codis-time').text("Cette fenêtre va se fermer dans 3 seconde").addClass("text-danger");
					setTimeout(function(){erreurCodis(id)},3000);
				}
			});
		}
		
		function erreurCodis(id){
			viderModal();
			creerModal("arreter", "ANNULER UNE INTERVENTION", "valid-arreter",'/php/requetes/annuler.php?id='+id,"get", 'bg-danger',"");
		}

</script>
