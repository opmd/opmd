<?php
require_once('../../init.php');
header('Content-type: application/json');
$tab = null;
$id_inter = $_POST['id_inter'];
$id_pe = $_POST['id_pe'];
$nb = $_POST['quantite'];
$ope = $_POST['operation'];
$quantite = PresenceExterne::getQuantite($id_inter,$id_pe);
$total  = 0;

if( $ope == "plus" ){
	if( $nb > 0 ){
		$total = $quantite + $nb;
		$tab = array("rep" => "ok", "quantite" => $total);
		PresenceExterne::setQuantite($id_inter,$id_pe,$total);
	} else {
		$total = $quantite;
		$tab = array("rep" => "no", "quantite" => $total);
	}
} else {
	if( $quantite == 0 ){
		$tab = array("rep" => "no", "quantite" => $total);
	} else {
		$total = $quantite - $nb;
		$total = ( $total <= 0 ) ? 0 : $total;
		$tab = array("rep" => "ok", "quantite" => $total);
		PresenceExterne::setQuantite($id_inter,$id_pe,$total);
	}
}

$ret = $tab;
echo json_encode($ret);

?>