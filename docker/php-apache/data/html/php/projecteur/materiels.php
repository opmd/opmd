<?php
require_once('../../init.php');


$ms = Materiel::getListMateriels(false);
$etatDispo = 0;
$etatInter = 0;
$etatReco = 0;
$etatIndispo = 0;

if($ms != null) {
	for($i=0 ; $i<count($ms) ; $i++) {
		$m = $ms[$i];
		if($m->getVisible() == 1 )
		{
			$etat_m = $m->getEtat();
			if( $etat_m->getId() == 1 ){ ++$etatInter; }
			if( $etat_m->getId() == 3 ){ ++$etatReco; }
			if( $etat_m->getId() == 5 ){ ++$etatIndispo; }
			if( $etat_m->getId() == 10 ){ ++$etatDispo; }
		}
	}
}


echo "<div class='panel-heading bg-apple text-center'><b>LISTE DU MATERIEL AU CENTRE DE CHAUMONT ( <span class='badge-success'>{$etatDispo}</span> / <span class='badge-warning'>{$etatInter}</span> / <span class='badge-info'>{$etatReco}</span> / <span class='badge-black'>{$etatIndispo}</span> )</b></div>";
if($ms != null) {
	for($i=0 ; $i<count($ms) ; $i++) {
		$m = $ms[$i];
		if($m->getVisible() == 1 )
		{
			$etat_m = $m->getEtat();
			$style = "background-color:{$etat_m->getCouleurFond()}; color:{$etat_m->getCouleurTexte()}";
			echo "<button class='btn space' style='{$style}'><b>{$m->getLibelle()} {$m->getCentre()->getAbreviation()} <b class='badge-white'>{$m->getDispo()}</b></button></b>";
		}
	}
}




?>