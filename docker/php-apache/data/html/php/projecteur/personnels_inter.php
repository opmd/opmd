<?php
require_once('../../init.php');


$ps_i = Personnel::getAllPersonnelPresentEnInter();
$off_i = Personnel::getNbOfficiersInter();
$ssoff_i = Personnel::getNbSousOfficiersInter();
$hdr_i = Personnel::getNbHommesDuRangInter();

echo "<div class='panel-heading bg-apple text-center'><b>PERSONNEL EN INTERVENTION ( <span class='badge-off'>{$off_i}</span> / <span class='badge-soff'>{$ssoff_i}</span> / <span class='badge-hdr'>{$hdr_i}</span> )</b></div>";

if($ps_i != null) {		
	foreach($ps_i as $p_i) {
		$nom = $p_i->getGrade()->getAbreviation() ." " .$p_i->getNom(). "." . strtoupper(substr($p_i->getPrenom(),0,1));
		$color = ( $p_i->getCouleur() == "red" ) ? "red;color:white;" : $p_i->getCouleur();
		echo "<button class='btn space' style='background:{$color}' onclick='afficher_inter({$p_i->getId()})'><b>{$nom}</button></b>";			
	}	
}


?>
