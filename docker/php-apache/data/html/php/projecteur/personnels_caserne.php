<?php
require_once('../../init.php');


$ps_c = Personnel::getAllPersonnelPresentDispo();
$off_c = Personnel::getNbOfficiersCaserne();
$ssoff_c = Personnel::getNbSousOfficiersCaserne();
$hdr_c = Personnel::getNbHommesDuRangCaserne();

echo "<div class='panel-heading bg-apple text-center'><b>PERSONNEL EN CASERNE ( <span class='badge-off'>{$off_c}</span> / <span class='badge-soff'>{$ssoff_c}</span> / <span class='badge-hdr'>{$hdr_c}</span> )</b></div>";

if($ps_c != null) {		
	foreach($ps_c as $p_c) {
		$nom = $p_c->getGrade()->getAbreviation() ." " .$p_c->getNom(). "." . strtoupper(substr($p_c->getPrenom(),0,1));
		$color = ( $p_c->getCouleur() == "red" ) ? "red;color:white;" : $p_c->getCouleur();
		echo "<button class='btn space' style='background:{$color}'><b>{$nom}</button></b>";			
	}	
}


?>
