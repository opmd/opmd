
<?php
require_once('../../init.php');

if ( !isset($_SESSION['users']) ){
	$serveur = "Location: http://".$_SERVER['SERVER_NAME'];
	header($serveur);
} else {

?>
<html lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>SYNOPTIQUE</title>
		<?php require '../template/styles.php'; ?>
		<link rel="shortcut icon" href="/favicon.ico">
	<script type="text/javascript" src="/<?= JQUERY; ?>/jquery-2.1.0.js"></script>
	</head>
	
	<body style="width:95%;margin:20 auto;" id="fondSite">
	

	<div class='row'>
		<div class='col-sm-8'>
		
			<div class='row'>
			<div class='col-sm-12'>
			
				<div class='panel panel-info'>
					<div class='panel-body pane-veh'>
						
					</div>
				</div>
				
			</div>
			</div>
			
			<div class='row'>
			<div class='col-sm-12'>
			
				<div class='panel panel-info'>
					<div class='panel-body pane-mat'>
						
					</div>
				</div>
			
			</div>
			</div>
			
			<div class='row'>
			<div class='col-sm-12'>
			
				<div class='panel panel-info'>
					<div class='panel-body pane-pers-caserne'>
						
					</div>
				</div>
				
			</div>
			</div>
			
			<div class='row'>
			<div class='col-sm-12'>
			
				<div class='panel panel-info'>
					<div class='panel-body pane-pers-inter'>
						
					</div>
				</div>
				
			</div>
			</div>
			
			<div class='row'>
			<div class='col-sm-12'>
			
				<div class='panel panel-info'>
					<div class='panel-body pane-autre-veh'>
						
					</div>
				</div>
				
			</div>
			</div>

		</div>	
	<div class='col-sm-4'>
		
		<div class='row'>
			<div class='col-sm-12'>
			
			<div class='panel panel-info'>
				<div class='panel-body pane-inter'>
				
				
				</div>
			</div>
			
		</div>
		</div>
		
		<div class='row'>
			<div class='col-sm-12'>
			
			<div class='panel panel-info'>
				<div class='panel-body pane-recap-matos'>
				
				
				</div>
			</div>
			
		</div>
		</div>
		
			<div class='row'>
				<div class='col-sm-12'>
					
					<div class='panel panel-info'>
						<div class='panel-body'>
							<div class='panel-heading bg-apple text-center'><b>SECTION RECHERCHE </b></div>
							
							<button class="btn btn-warning space" id="inter-cour" onclick="affiche('cour')" style="color:black">INTERVENTIONS EN COURS</button>
							<button class="btn btn-danger space" id="perm-ci" onclick="affiche('perm')">PERMANENCE CI</button>
							<button class="btn btn-success space" id="perso" onclick="affiche('perso')">PERSONNEL</button>
							<button class="btn btn-yellow space" id="inter-diff" onclick="affiche('diff')" style="color:black">INTERVENTIONS DIFFEREES</button>
							<button class="btn btn-info space" id="vl-reco" onclick="affiche('reco')" style="color:black">VEHICULE EN RECONNAISSANCE</button>
							
						</div>
					</div>
					
				</div>
			</div>
			
	</div>
	
	</div>

	<?php  require_once('../template/scripts.php'); 
	/* 
Rappel etat 
0 : aucun
1 : En cours
2 : Reconnaissance
3 : Differee
4 : Terminer
5 : Annuler
*/
	
	?>
	
	<div id="bloc-modal"></div>
	<div id="bloc-error"></div>
	<div id="bloc-modal-depart"></div>
	
	<script>
	var perso = new Object() ;
	$(document).ready(function(e){
	
		// charger les élément (véhicules/matériels/personnels/interventions
		updateSynoptique();
	
	});
	
	function updateSynoptique(){
		$('.pane-veh').load('/php/projecteur/vehicules.php');
		$('.pane-mat').load('/php/projecteur/materiels.php');
		$('.pane-autre-veh').load('/php/projecteur/autre_vehicules.php');
		$('.pane-pers-caserne').load('/php/projecteur/personnels_caserne.php');
		$('.pane-pers-inter').load('/php/projecteur/personnels_inter.php');
		$('.pane-inter').load('/php/projecteur/interventions.php');
		$('.pane-recap-matos').load('/php/projecteur/recap_materiel.php');
	}
	
	setInterval(function(){ updateSynoptique(); }, 5000);
	
	function modVehicules2(v_id) {
		creerModal("mod-vehicule", "MODIFIER UN VEHICULE", "mod-v",'/php/formulaires/vehicule.php?id='+v_id+'&mode=mod',"get", 'bg-warning',"");
	}
	
	function consult_permanence(id) {
		creerModal("consult-permanences", "CONSULTATION DE PERMANENCE", "consulter",'/php/formulaires/permanence.php?id='+id,"get", 'bg-info',"");
	}
	
	function affiche(data){
		$.post("/php/pages/modal_inter.php",{ type : data }).always( function(arg) {
			if(fullDebug) { msg(arg) };
			if( arg.id != "aucun" ){
				chargerFenetre(arg,data);
			} else {
				notif("Votre recheche ne peut aboutir car il y a aucun résultat","info","","");
			}
		});
	}

	function chargerFenetre(data,type){
		$.post('/php/requetes/listing.php',data).always(function(arg){
			afficherRecherche(arg,type);
		});
	}
	
	function afficherRecherche(html,type){
	
		var texte;
	
		if ( type == "cour" ) {
			texte = "Cliquer sur le numéro de l'intervention pour en afficher sont récapitulatif";
			modalInter("searching", "RECHERCHER UNE INTERVENTION EN COURS", "bg-warning",html,texte,'idInter');
		}
		
		if ( type == "diff" ) {
			texte = "Cliquer sur le numéro de l'intervention pour en afficher sont récapitulatif";
			modalInter("searching", "RECHERCHER UNE INTERVENTION DIFFEREE", "bg-yellow",html,texte,'idInter');
		}	
		
		if ( type == "reco" ) {
			texte = "Cliquer sur le numéro de l'intervention pour en afficher sont récapitulatif";
			modalInter("searching", "RECHERCHER LES VEHICULES EN RECONNAISSANCE", "bg-info",html,texte,'idInter');
		}
		
		if ( type == "perm" ) {
			texte = "Cliquer sur la permanence en cours ou sur son numéro pour en afficher sont récapitulatif";
			modalInter("searching", "RECHERCHER UNE PERMANENCE CI", "bg-danger",html,texte,'idInter');
		}
		
		if ( type == "perso" ) {
			texte = "Entrer le Nom et prénom d'une personne puis la sélectionner pour en afficher sont récapitulatif";
			modalInter("searching", "RECHERCHER DU PERSONNEL", "bg-success",html,texte,'idPerso');
		}
	}
	
	function initPerso(p){ 
		 perso = p;
		if(fullDebug) { msg(perso) };
		$("#recherche").on("keyup", function(){
			var search = $(this).val();
			if(fullDebug) { msg(search) };
			deletePerso(search);	
		}); 
		eventPerso();
	}
	
	function deletePerso(search){
		var i;
		var id;
		var patt = new RegExp(search,'gi');
		for ( i = 0; i < perso.length; i++ ){
			if( patt.test(perso[i].name) ) {
				msg(perso[i].id);
				id = "#id-"+perso[i].id;
				$(id).show();
			} else {
				id = "#id-"+perso[i].id;
				$(id).hide();
			}
		}
	}
	
	function eventPerso(){
		$('.personnel').on('mouseenter',function(){ $(this).css({backgroundColor : '#C1FAC6'}) });
		$('.personnel').on('mouseout',function(){ $(this).css({backgroundColor : '#FFFFFF'}) });
		$('.personnel').on('click',function(){ 
				var elem = $(this).attr('id');
				//modalLoader();
				$.post('/php/requetes/recherche_personnel.php', { id : elem.substr(3) }).always( function(arg){
					if ( arg.perso == "aucun" ) {
						viderModal();
						notif("Votre recheche ne peut aboutir car il y a aucun résultat","info","","");
					} else if ( arg.perso == "caserne" ) {
						viderModal();
						notif("Le personnel est bien présent en caserne","success","","");
					} else {
						viderModal();
						splash_recap(arg.perso);
					}
					if(fullDebug) { msg(arg) };
				});
				//alert(elem.substr(3));
		});
	}
	
	function afficher_inter(id_pers){
		$.post('/php/requetes/recherche_personnel.php', { id : id_pers }).always( function(arg){
			splash_recap(arg.perso);
		});
		if(fullDebug) { msg(arg) };
	}
	
	</script>
	
	

	</body>
</body>

<?php
}
?>