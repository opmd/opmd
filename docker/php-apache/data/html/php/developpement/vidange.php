<?php

require_once('../../init.php');

if ( isset($_POST['vider']) && !empty($_POST['vider']) ) {

	if ( $_POST['vider'] == "vidange" ) {

		Mysql::connect();

		// Vider toutes Inter présence externe
		$sql = "TRUNCATE TABLE intervention_presence_externe";
		Mysql::query($sql);

		// Vider toutes Inter
		$sql = "TRUNCATE TABLE intervention";
		Mysql::query($sql);

		// Faire rentrée tous le personnel
		$sql = "TRUNCATE TABLE affectation_poste";
		Mysql::query($sql);

		// Vider les horaires
		$sql = "TRUNCATE TABLE horaire";
		Mysql::query($sql);

		// Remmettre tous les véhicules dispo
		$sql = "UPDATE vehicule SET intervention = NULL , etat = 10;";
		Mysql::query($sql);

		// Remmettre tous les matériels dispo
		$sql = "UPDATE materiel SET intervention = NULL , etat = 10;";
		Mysql::query($sql);

		// Vider toutes les permanence
		$sql = "TRUNCATE TABLE permanence";
		Mysql::query($sql);
		
		
		// Remettre toutes les quantitées pour le matériel
		$sql = "SELECT * FROM materiel";
		$result = Mysql::query($sql);
		while($row = $result->fetch()) {
			$m = new Materiel($row['id']);
			$mat = $m->getQuantite();
			$m->setDispo($mat);
			$m->setUtiliser(0);
			$m->commit();
		}
		
		
		// Remettre toutes les fonctions pour les véhicule
		$sql = "SELECT * FROM vehicule WHERE visible = 1";
		$result = Mysql::query($sql);
		while($row = $result->fetch()) {
			for($i=1; $i < 3; $i++ ) {
				$id = $row['id'];
				$req = "INSERT INTO affectation_poste (vehicule,poste,personnel) VALUES (?, ?, NULL)";
				Mysql::query($req,$id,$i);
			}
		}

		echo "ok";

	}

} else {

?>


<div class="row">
	<div class="col-sm-12">
		<h1 class="text-center text-danger">Attention ! Vous allez vidanger la base de donnée sur les élément suivants : <h1>
	</div>
</div>


<div class="row">
	<div class="col-sm-offset-2 col-sm-8">
		<ul style="font-size:18px;">
			<li class="text-primary">Vider toutes Interventions liées</li>
			<li class="text-primary">Vider toutes présence externe sur les Interventions</li>
			<li class="text-primary">Vider toutes Interventions</li>
			<li class="text-primary">Faire rentrée tous le personnels et vider les horaires</li>
			<li class="text-primary">Faire rentrée tous les véhicules et les remettre en disponible</li>
			<li class="text-primary">Faire rentrée tous les matériels et les remettre en disponible</li>
			<li class="text-primary">Vider toutes les permanences CI</li>
		<ul>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<h4 class="text-center text-warning">Pour valider votre action et faire appliquer tous ceux qui est indiquées ci-dessus, alors cliquer sur VIDANGER, aucune confirmation sera demandé <br><b class="text-danger">( ACTION IRREVERSIBLE !!! )</b></h4>
	</div>
</div>

<br>
<hr>
<br>

<div class="row">
	<div class="col-sm-offset-5 col-sm-2">
		<button class="btn btn-danger" id="vidertable" type="button">VIDANGER</button>
	</div>
</div>


<script type="text/javascript">
	
	$("#vidertable").on('click', function(){
		$.post('/php/developpement/vidange.php', { vider : "vidange" }).always( function(arg){
			if ( arg.trim() == "ok" ) {
				update();
				notif("La base de donnée a bien été vidangé","success","","");
				viderModal();
				
			}
		});
	});

</script>

<?php } ?>

