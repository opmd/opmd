<?php
	require_once('../../init.php');
	$off = Personnel::getNbOfficiersCaserne();
	$ssoff = Personnel::getNbSousOfficiersCaserne();
	$hdr = Personnel::getNbHommesDuRangCaserne();
?>


<div class="panel panel-info">
   <div class="panel-heading">
      <h3 class="panel-title">PERSONNEL <?php echo "({$off}/{$ssoff}/{$hdr})"; ?> <span class="badge-trans pull-right"><a href="#" onclick="personnel();"><span class="glyphicon glyphicon-plus"></span></a></span><img src="<?= IMG; ?>/return.png" onclick="vider_personnel();" style="float:right;margin-right:5px;"/></h3>
    </div>
    <div class="panel-body" id="perso-panel">
		<div class="table-responsive">
			<table cellspacing="4" class="table table-bordered" >

	
	<tbody>
<?php
	$ps = Personnel::getAllPersonnelPresent();
	$tab = null;
	if($ps != null) foreach($ps as $p) {
		$color = $p->getCouleur();
		$color = ( $p->getCouleur() == "red" ) ? "red;color:white;" : $p->getCouleur();
		$name = substr($p->getNomFormate(),3) . " " . $p->getCentre()->getAbreviation();
		if($color == 'black') $color .= ';color:white';
		echo "<tr><td class='td_delete_personnel' style='background:{$color}'>&nbsp;<div style='display:inline;' onclick='edit_personnel({$p->getId()});'><img src='/assets/img/grades/{$p->getGrade()->getAbreviation()}.png' style='width:20px;height:20px;'/><b>  {$name}</b></div>&nbsp;&nbsp;</td>";
		echo "<td style='background:{$color}' class='td_delete_personnel'><span class='badge badge-danger' onclick='modalConfirmPerso({$p->getId()},\"{$p->getNomFormate()}\")'><span style='padding-top:3px;padding-bottom:3px;' class='glyphicon glyphicon-remove'></span></span></td></tr>\n";
	}
?>
	</tbody>
</table>
</div>
	</div>
</div>

<script>
	$(".td_delete_personnel").mouseenter(function() {
		$(this).children("img").css("opacity", 1);
	});
	$(".td_delete_personnel").mouseleave(function() {
		$(this).children("img").css("opacity", 0.1);
	});
	function personnel(){
		creerModal("personnels", "GERER LE PERSONNEL", "open-personnel","/php/formulaires/personnel.php","get", 'bg-info',"");
	}
	
	function vider_personnel(){
		modalConfirm("modal-vider-perso", "FAIRE RENTRER TOUT LE PERSONNEL", "vider-perso", "bg-danger", "Voulez-vous vraiment faire rentré tout le personnel ? ");
		$("#del-vider-perso").on("click", function() { viderPerso(); });
	}

	function viderPerso(){
		$.get('/php/requetes/fin_permanence.php').always(function (arg) {
			if( $.trim(arg.reponse) == "ok"){
				notif("Tous le personnel est rentré","success","","");
				updateIntelligent();
				viderModal();
			} else {

				if( $.trim(arg) != ""){
					viderModal();
					$("#bloc-error").append(arg);
					notif("Erreur lors de la rentré du personnel","danger","","");
				} 
			}
		});
	}
</script>