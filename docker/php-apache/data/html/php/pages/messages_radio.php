<?php
require_once('../../init.php');
	$inter = new Intervention(@$_GET['id']);
	echo "<input type='hidden' name='id' value='{$inter->getId()}' />";
?>
<input type="button" value="GROUPE HORAIRE" class="btn btn-purple" onclick="addTextInside('message_radio2', formatDate(new Date(), 'hhnn '));" />

<hr />

<?php
	$vs = $inter->getVehicules();
	if($vs != null) foreach($vs as $v) {
		$etat_v = $v->getEtat();
		$fond = array("orange"=>"warning","cyan"=>"info","black"=>"black","lime"=>"success","red"=>"danger");
		$class = $fond[$etat_v->getCouleurFond()];
		echo "<input type='button' class='btn btn-{$class}' value='{$v->getLibelle()} {$v->getCentre()->getAbreviation()}' onclick=\"addTextInside('message_radio2', '{$v->getLibelle()} {$v->getCentre()->getAbreviation() }');\" /> ";
	}
?>

<hr />

<?php
	$ps = Personnel::getAllPersonnelPresentDispo();
	if($ps != null) foreach($ps as $p) {
		echo "<input type='button' value='{$p->getNom()} {$p->getPrenom()}' class='btn btn-success space' onclick=\"addTextInside('message_radio2', '{$p->getNom()} {$p->getPrenom()} ');\" /> ";
	}
?>

<textarea id="message_radio2" name="message_radio2" style="width:100%; height:45%;" ><?php echo $inter->getMessageRadio(); ?></textarea><br /><br/>

<div class="row">
	<div class="col-md-12 text-center">
		<input type="button" value="Fermer" onclick="viderModal();" class="btn btn-danger"/>
		<input type="button" value="Valider" onclick="send_radio();" class="btn btn-success"/>
	</div>
</div>
