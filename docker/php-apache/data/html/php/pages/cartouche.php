<?php
	require_once('../../init.php');
	$c_encour = "active";
	$c_differee = "";
	$c_permci = "";
	$c_reco = "";
	if( isset($_GET['onglet']) ){
		$c_encour = ($_GET['onglet'] == "en-cour") ? "active" : "";
		$c_differee = ($_GET['onglet'] == "differee") ? "active" : "";
		$c_permci = ($_GET['onglet'] == "permci") ? "active" : "";
		$c_reco = ($_GET['onglet'] == "reco") ? "active" : "";
	}
	$nb_col = 2;
	$msg = 0;
	$iec = Intervention::getInterventionsEnCours();
	$compteur = Intervention::compteurInter();
	$nb_iec = $compteur['enCour'];
	$nb_ier = $compteur['vlreco'];
	$nb_id = $compteur['diff'];
	$total_inter = $compteur['total'];;
	$nb_pe = (Permanence::getCurrentPermanence()->getId() == -1) ? 0 : 1;
	
	//debug( Intervention::compteurInter() );
	
?>


	<div class='bg-danger'><b style="font-size:25px;color:red;">INTERVENTIONS <span class="badge-danger"><b style="font-size:25px"> <?= $total_inter; ?> </b></span> / PERMANENCE CI <span class="badge-danger"><b style="font-size:25px"> <?= $nb_pe; ?> </b></span></b></div>
	
	
	<div class="panel panel-danger">
     <div class="panel-heading">
		 
	   	<ul class="nav nav-tabs nav-justified" id="myTabs">
	      	<li class="<?= $c_encour ?>" style="font-size:18px"><a href="#en-cour" data-toggle="tab">EN COURS <span class="badge-info pull-right"><b style="font-size:20px"> <?= $nb_iec; ?> </b></span></a></li>
	      	<li class="<?= $c_differee ?>" style="font-size:18px"><a href="#differee" data-toggle="tab">DIFFEREE <span class="badge-info pull-right"><b style="font-size:20px">  <?= $nb_id; ?> </b></span></a></li>
			<li class="<?= $c_permci ?>" style="font-size:18px"><a href="#permci" data-toggle="tab">PERM CI <span class="badge-info pull-right"><b style="font-size:20px">  <?= $nb_pe; ?> </b></span></a></li>
			<li class="<?= $c_reco ?>" style="font-size:18px"><a href="#reco" data-toggle="tab">VL RECO <span class="badge-info pull-right"><b style="font-size:20px">  <?= $nb_ier; ?> </b></span></a></li>
    	</ul>
	   
     </div>
     
		 <div class="panel-body" id='intervention-panel'>



<div class="tab-content">
 <div class="tab-pane <?= $c_encour ?>" id="en-cour">
<?php
		if($iec != null) {
	?>

<div class="table-responsive">
<table class="table table-bordered">
	<thead>
		<tr class="bg-info">
			<th class="redtitle text-middle">N° CODIS</th>
			<th class="redtitle text-middle">Date - heure</th>
			<th class="redtitle text-middle">Lieu / Précision</th>
			<th class="redtitle text-middle">Véhicules / Personnel</th>
			<th class="redtitle text-middle"<?php echo " colspan='{$nb_col}'"; ?>>Actions</th>
		</tr>
	</thead>
	
	<tbody>
	<?php
		foreach($iec as $inter) {
		
			$com = $inter->getCommune();
			$pes = $inter->getPresencesExternes();
			$statusTexte = $inter->getStatus()->getCouleurTexte();
			$statusFond = $inter->getStatus()->getCouleurFond();
			$styleInter = "color:{$statusTexte};background-color:{$statusFond};";
			$codisLier = explode(" ",$inter->getCodisLier());
	?>
			<tr class="bg-default">
				<td class="text-center text-middle">
				<input type="button" value="<?php echo $inter->getId(); ?>" onclick="splash_recap(<?php echo $inter->getId(); ?>);" style="font-weight:bold;<?= $styleInter; ?>" class="btn espace" />
				<?php if(!empty($codisLier[0])) : ?> 
				<br>
					<?php foreach($codisLier as $num) : ?> 
					<input type="button" value="<?php echo $num; ?>" onclick="" style="font-weight:bold;background-color:black;color:white;" class="btn espace" />
					<?php endforeach; ?>
				<?php endif; ?> 
				</td>
				<td class="text-center text-middle"><?php echo date('d/m/y à Hi', $inter->getDateHeure()); ?></td>
				<td class="text-center text-middle"><?php echo $com->getLibelle().(strlen($inter->getAdresse()) ? " – {$inter->getNumeroRue()} {$inter->getAdresse()}" : '').'<br />&nbsp;&nbsp;'.$inter->getPrecision()->getLibelle(); ?></td>
				<td class="text-center text-middle"><?php
						$vs = $inter->getVehiculesVisibles();
						for($i=0 ; $i<count($vs) ; $i++) {
							$v = $vs[$i];
							//if($i%4 == 0 && $i != 0) echo '<br /><br style="line-height:2px;" />&nbsp;&nbsp;';
							$etat_v = $v->getEtat();
							$style = "background-color:{$etat_v->getCouleurFond()}; color:{$etat_v->getCouleurTexte()};";
							echo "<div style='{$style}'";
							echo " onclick='affectationVehicule({$v->getId()});' onmouseup='document.oncontextmenu = rightClick;' data='veh-{$v->getId()}'";
							echo " class='arrondi spaceT sizeT'> {$v->getLibelle()} {$v->getCentre()->getAbreviation()}</div>";
						}
			
						$ps = $inter->getPersonnel();
						if($ps != null){
							for($i=0 ; $i<count($ps) ; $i++) {
								if($i == 0) echo "<br style='clear:both'><hr/>";
								$p = $ps[$i];
								//if($i%4 == 0 && $i != 0) echo '<br /><br style="line-height:2px;" />&nbsp;&nbsp;';
								$style = "background-color:#f0ad4e;color:black";
								echo "<div style='{$style}'";
								echo "class='arrondi spaceT sizeT'>". $p->getGrade()->getAbreviation()." ".$p->getNom().".". strtoupper(substr($p->getPrenom(),0,1))."</div>";
							}
						}
						
					?>
				</td>
				<td class="text-center text-middle" onclick="radio(<?= $inter->getId(); ?>);">RELÈVE <br> RADIO</td>
        <td class="text-center text-middle"><img onclick="print_recap(<?php echo $inter->getId(); ?>);" src="<?= IMG; ?>/print16.png" /><br><br><img data="<?php if( !empty($com->getCodePostal()) ) { echo  $com->getCodePostal(); } else { echo 52000; } ?>" class="mapi" src="<?= IMG; ?>/map24.png" /></td>
			</tr>
<?php
		
		}
	
?>
	</tbody>
</table>
</div>



<?php
	}
?>
</div>

 
  <div class="tab-pane <?= $c_permci ?>" id="permci">
	
	<?php include_once("permci.php"); ?>
 	
 </div>
 
 <div class="tab-pane <?= $c_reco ?>" id="reco">
 	
 	<?php include_once("vl_reco.php"); ?>
	
 </div>
 
  <div class="tab-pane <?= $c_differee ?>" id="differee">
 	
	<?php include_once("differees.php"); ?>
 	
 </div>
 
 
</div>




	</div>
</div>

<div id="pers" style="display:none; position:absolute; background-color:lemonchiffon;"></div>



<script type="text/javascript" charset="utf-8">
	$('#myTabs a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	});
  
  $('.mapi').on('click', function(){
    var code = $(this).attr('data');
    window.open("https://www.google.fr/maps/place/"+code);
  });
	
	function affectationVehicule(id){
		creerModal("aff-veh", "MODIFIER L'AFFECTATION A UN VEHICULE", "mod-aff-v",'/php/formulaires/affectation_vehicule.php?id='+id,"get", 'bg-warning',"");
	}
</script>


