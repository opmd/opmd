<?php
require_once('../../init.php');
$it = null;

if(!empty($_POST['debut']) && !empty($_POST['fin'])) {
	$debut = str_replace('/', '-', $_POST['debut']);
	$fin = str_replace('/', '-', $_POST['fin']);

	$debut_ts = strtotime($debut);
	$fin_ts = strtotime($fin)+86399;
	
	if($debut_ts && $fin_ts) {
		$it = Intervention::getPlageInterventionsTerminees($debut_ts, $fin_ts);
	}
}

if(!empty($_POST['id'])) {
	$it_tmp = new Intervention($_POST['id']);
	if($it_tmp->exists()) {
		$it[] = $it_tmp;
	}
}

?>


		<center><font style="font-weight:bold; font-size:46; color:red;">OPERATIONS MULTIPLES DIVERSES</font></center>
		<br /><br />

	

		<center><font style="font-weight:bold; font-size:36; color:blue;">CIG CHAUMONT</font><center>
		<br /><br>
		
		<form method="post" style="display: inline-block; border: 1px solid black; background-color: red; padding: 5px;">
			RAPPORT D'INTERVENTION<br />
			du <input type="text" name="debut" size="10" value="<?php echo @$_POST['debut']; ?>" />
			au <input type="text" name="fin" size="10" value="<?php echo @$_POST['fin']; ?>" /><br />
			<input type="submit" value="Rechercher" />
		</form>
		<br />
		
		<form method="post" style="display: inline-block; border: 1px solid black; background-color: red; padding: 5px;">
			RAPPORT D'INTERVENTION<br />
			N° <input type="text" name="id" size="10" value="<?php echo @$_POST['id']; ?>" />
			<input type="submit" value="Rechercher" />
		</form>
		<br /><br />
		
		<?php
			if($it != null) {
				include_once('menu.php');
		?>
			<div id="recap" style="position:absolute; visibility:hidden; background-color:salmon; padding: 5px; border: 1px solid grey; text-align:left;"></div>
			
			<table border="1" cellspacing="0" cellpadding="2" style="empty-cells:show;" align="center">
				<thead>
					<tr>
						<th class="redtitle">Date - heure</th>
						<th class="redtitle">Précision</th>
						<th class="redtitle">Commune</th>
					</tr>
				</thead>
				
				<tbody>
				<?php
					foreach($it as $i) {
				?>
						<tr onclick="hide_recap(); compte_rendu(<?php echo $i->getId(); ?>);" onmouseover="display_recap(<?php echo $i->getId(); ?>); recap_displayed=true;" onmousemove="display_recap(<?php echo $i->getId(); ?>);" onmouseout="hide_recap();" style="background-color:<?php echo $i->getCompteRendu()->isValide() ? 'beige' : 'aqua'; ?>">
							<td>&nbsp;&nbsp;<?php echo date('d/m/y à Hi', $i->getDateHeure()); ?>&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;<?php echo $i->getPrecision()->getLibelle(); ?>&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;<?php echo $i->getCommune()->getLibelle(); ?>&nbsp;&nbsp;</td>
						</tr>
				</tbody>
				<?php } ?>
			</table>
		<?php
			}
		?>
	