/*
 * Fichier d'application principal partie administration
 * Fichier crée le 01/05/2014
 * Auteur : Denis Sanson alias Multixvers
 */

var bug = "non";
var fullDebug = true;

$(document).ready(function(evt) {

    // PARTIE ADMIN STRICT

    $("#admin-user").click(function() { viewUsers(); });

    /* OPERATION */
    $("#modal-vidange").click(function() { vidanger(); });
    $("#gestion-export").click(function() { exportDB(); });
    $("#gestion-import").click(function() { importDB(); });


    /* FONCTION DU MENU PUBLIC */

    $("#groupe-add").click(function() { addGroupes(); });

    $("#centre-add").click(function() { addCentres(); });

    $("#gestion-groupes").click(function() { groupes(); });

    $("#gestion-centres").click(function() { centres(); });

    $("#gestion-materiels").click(function() { materiels(); });

    $("#gestion-vehicules").click(function() { vehicules(); });

    $("#gestion-communes").click(function() { communes(); });

    $("#gestion-natures").click(function() { natures(); });

    $("#gestion-precisions").click(function() { precisions(); });

    $("#gestion-consulter").click(function() { consulter(); });

    $("#gestion-rapports").click(function() { rapports(); });

    $("#gestion-secteur").click(function() { secteurs(); });

    $("#gestion-consignes").click(function() { consignes(); });

    $("#gestion-affectation").click(function() { affectation(); });

    $("#gestion-permanence").click(function() { permanence(); });

    $("#deco").click(function() { logout($(this).attr('data')); });

    $("#modal-search-codis").click(function() { modalCodis(); });

    $("#gestion-site").click(function() {
        var url = window.location.origin;
        location.assign(url);
    });

    $("#gestion-depart").click(function() {
        //notif("Les Intervention sont bloqué temporairement tant que les adresses ne seront pas crées afin d'éviter les bug","info","",true);

        var titre = "FEUILLE DE DEPART";
        creerModalDiff("depart", titre, "feuille_depart", "/php/pages/feuille_depart.php", "", 'bg-danger', true);

    });


    /* HORLOGE MULTIXVERS */
    setInterval(function() { affiche_heure() }, 1000);
    affiche_heure();

    /* Mise à jour intelligente */
    update();

    $('#admin-bdd').on('click', function() { window.open('https://pma.multixvers.ovh/'); });
    $('#up-info').on('click', function() { upinfo(); });
});