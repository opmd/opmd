
<?php
require_once('../init.php');

?>
<html lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>SYNOPTIQUE</title>
		<?php require '../php/template/styles.php'; ?>
		<link rel="shortcut icon" href="/favicon.ico">
	<script type="text/javascript" src="/<?= JQUERY; ?>/jquery-2.1.0.js"></script>
	</head>
	
	<body style="width:95%;margin:20 auto;" id="fondSite">
	

	<div class='row'>
		<div class='col-sm-8'>
		
			<div class='row'>
			<div class='col-sm-12'>
			
				<div class='panel panel-info'>
					<div class='panel-body pane-veh'>
						
					</div>
				</div>
				
			</div>
			</div>
			
			<div class='row'>
			<div class='col-sm-12'>
			
				<div class='panel panel-info'>
					<div class='panel-body pane-mat'>
						
					</div>
				</div>
			
			</div>
			</div>
			
			<div class='row'>
			<div class='col-sm-12'>
			
				<div class='panel panel-info'>
					<div class='panel-body pane-pers-caserne'>
						
					</div>
				</div>
				
			</div>
			</div>
			
			<div class='row'>
			<div class='col-sm-12'>
			
				<div class='panel panel-info'>
					<div class='panel-body pane-pers-inter'>
						
					</div>
				</div>
				
			</div>
			</div>
			
			<div class='row'>
			<div class='col-sm-12'>
			
				<div class='panel panel-info'>
					<div class='panel-body pane-autre-veh'>
						
					</div>
				</div>
				
			</div>
			</div>

		</div>	
	<div class='col-sm-4'>
		
		<div class='row'>
			<div class='col-sm-12'>
			
			<div class='panel panel-info'>
				<div class='panel-body pane-inter'>
				
				
				</div>
			</div>
			
		</div>
		</div>
		
		<div class='row'>
			<div class='col-sm-12'>
			
			<div class='panel panel-info'>
				<div class='panel-body pane-recap-matos'>
				
				
				</div>
			</div>
			
		</div>
		</div>
		

	</div>
	
	</div>

	<?php  //require_once('../template/scripts.php'); ?>
	
	
    <script>
    $(document).ready(function(e){
	
		// charger les élément (véhicules/matériels/personnels/interventions
		updateSynoptique();
	
	});
	
	function updateSynoptique(){
		$('.pane-veh').load('/php/projecteur/vehicules.php');
		$('.pane-mat').load('/php/projecteur/materiels.php');
		$('.pane-autre-veh').load('/php/projecteur/autre_vehicules.php');
		$('.pane-pers-caserne').load('/php/projecteur/personnels_caserne.php');
		$('.pane-pers-inter').load('/php/projecteur/personnels_inter.php');
		$('.pane-inter').load('/php/projecteur/interventions.php');
		$('.pane-recap-matos').load('/php/projecteur/recap_materiel.php');
	}
	
	setInterval(function(){ updateSynoptique(); }, 5000);
    </script>
	

	</body>
</body>

