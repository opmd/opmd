/*
 * Fichier d'application principal
 * Fichier crée le 06/04/2014
 * Auteur : Denis Sanson alias Multixvers
 */

var debug = "";
var fullDebug = true;
var delay = 20000;

$(document).ready(function(evt) {

    /* FONCTION DU MENU GERER */

    $("#groupe-add").click(function() { addGroupes(); });

    $("#centre-add").click(function() { addCentres(); });

    $("#gestion-groupes").click(function() { groupes(); });

    $("#gestion-centres").click(function() { centres(); });

    $("#gestion-materiels").click(function() { materiels(); });

    $("#gestion-vehicules").click(function() { vehicules(); });

    $("#gestion-communes").click(function() { communes(); });

    $("#gestion-natures").click(function() { natures(); });

    $("#gestion-precisions").click(function() { precisions(); });

    $("#gestion-consulter").click(function() { consulter(); });

    $("#gestion-rapports").click(function() { rapports(); });

    $("#gestion-secteur").click(function() { secteurs(); });

    $("#gestion-consignes").click(function() { consignes(); });

    $("#gestion-affectation").click(function() { affectation(); });

    $("#gestion-permanence").click(function() { permanence(); });

    $("#modal-search-codis").click(function() { modalCodis(); });

    $("#gestion-admin").click(function() {
        var url = window.location.origin + "/admin";
        location.assign(url);
    });

    $("#modal-depart").click(function() {
        var titre = "FEUILLE DE DEPART";
        creerModalDiff("depart", titre, "feuille_depart", "/php/pages/feuille_depart.php", "", 'bg-danger', true);

    });

    $("#deco").click(function() { logout($(this).attr('data')); });

    if ($("#info").attr('data') == 1) {
        notif("L'APPLICATION A EU DE NOUVELLE MISE A JOUR <br><br> Vous allez être redirigé automatiquement vers celle-ci dans 5 secondes", "info", "", "");
        setTimeout(function() { visuInfo(); }, 5000);
    }


    /* HORLOGE */
    setInterval(function() { affiche_heure() }, 1000);
    affiche_heure();

    /* MISE A JOUR AUTO */
    function recharger() {
        window.setTimeout(function() {
            if (window.requestAnimationFrame) {
                window.requestAnimationFrame(function() {
                    update();
                    recharger();
                });
            } else {
                update();
                recharger();
            }
        }, delay);
    }

    /* Mise à jour intelligente */
    recharger();
    update();

    $('.collapse').collapse();

    // collapse
    $('#cta_codis').on('click', '.glyphicon-chevron-up', function() {
        var $me = $(this);
        var $parent = $me.parent().parent().parent().parent();
        $me.removeClass('glyphicon-chevron-up');
        $me.addClass('glyphicon-chevron-down');
        $parent.find('.panel-body').collapse('hide');
    });

    $('#cta_codis').on('click', '.glyphicon-chevron-down', function() {
        var $me = $(this);
        var $parent = $me.parent().parent().parent().parent();
        $me.removeClass('glyphicon-chevron-down');
        $me.addClass('glyphicon-chevron-up');
        $parent.find('.panel-body').collapse('show');
    });


});