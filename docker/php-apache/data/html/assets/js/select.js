/*
 * Fonction de gestion des éléments de liste déroulante
 * pour les véhicules/matériels/personnels/postes
 * Fichier crée le 06/04/2014
 * Auteur : Denis Sanson alias Multixvers
 */

/*-------------------- AJOUT ET SUPPRESSION DES ELEMENTS DE LISTE --------------------*/

function addSelect(select, divId) {
    try {
        var div = document.getElementById(divId);

        var br = document.createElement('br');
        div.appendChild(br);

        var newSelect = document.createElement("select");
        for (i = 0; i < select.length; i++) {
            var opt = new Option(select.options[i].text, select.options[i].value);
            opt.disabled = select.options[i].disabled;
            newSelect.appendChild(opt);
        }
        newSelect.name = select.name;
        newSelect.onchange = select.onchange;
        select.onchange = "";
        $(newSelect).addClass('form-control');
        div.appendChild(newSelect);
    } catch (e) {
        if (debug) alert(e);
    }
}

function optionValueIndexOf(options, value) {
    for (var i = 0; i < options.length; i++) {
        if (options[i].value == value) {
            return i;
        }
    }
    return -1;
}

function addText(textarea, txt) {
    t = document.getElementById(textarea);
    if (t) {
        t.value = t.value + txt;
        t.scrollTop = t.scrollHeight;
        t.focus();
    }
}

function addTextInside(textarea, txt) {
    t = document.getElementById(textarea);
    if (t && t.selectionStart == t.selectionEnd) {
        pos = t.selectionStart;
        t.value = t.value.substr(0, pos) + txt + t.value.substr(pos);
        t.selectionStart = pos + txt.length;
        t.selectionEnd = t.selectionStart;
    }
}

function selectInSelect(divName, value) {
    selects = document.getElementById(divName).getElementsByTagName("select");
    select = selects[selects.length - 1];
    select.value = value;
    select.onchange();
}