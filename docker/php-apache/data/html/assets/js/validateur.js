/*
 * Fonction de validation des formulaires
 * Fichier crée le 14/04/2014
 * Auteur : Denis Sanson alias Multixvers
 */


/*-------------------- VALIDATION DES FORM --------------------*/

function errorForms(context, msg, type) {

    // nettoyer les class en cour
    $(context).parent().parent().removeClass('has-success has-error has-warning');
    $(context).next().removeClass('glyphicon-ok glyphicon-remove glyphicon-warning-sign');



    // nettoyer le message
    $(context).parent().find('.error').text('');

    // ajouter la class en fonction du type
    if (type == "success") {
        $(context).parent().parent().addClass('has-success');
        $(context).next().addClass('glyphicon-ok');
    } else if (type == "error") {
        $(context).parent().parent().addClass('has-error');
        $(context).next().addClass('glyphicon-remove');
    } else {
        $(context).parent().parent().addClass('has-warning');
        $(context).next().addClass('glyphicon-warning-sign');
    }

    // ajouter le message
    $(context).parent().find('.error').text(msg).addClass('text-danger');

}



// validation d'un champ input
function validInput(texte, type, context, min, max, regexp) {

    var minimum = (min != "" || min != 0) ? min : false;
    var maximum = (max != "" || max != 0) ? max : false;
    var msgErreur;

    data = texte.trim();

    if (type == "text") {

        if (minimum) {
            if (data.length < minimum) {
                msgErreur = "La valeur attendu est trop court => " + minimum + " caratères attendu minimum";
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            }
        }

        if (maximum) {
            if (data.length > maximum) {
                msgErreur = "La valeur attendu est trop long => " + maximum + " caratères attendu maximun";
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            }
        }

        if (regexp == "alpha") {
            // autorise les caractère alphabétique en majuscule et minuscule (avec l'espace, le tiret et l'appostrophe)
            var patt = regexAlpha;
            var res = patt.test(data);
            if (res) {
                msgErreur = "Les caractères autorisées sont les minuscules, les majuscules, l'espace, le tiret et l'appostrophe";
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            }
        }

        if (regexp == "alphaMaj") {
            // autorise les caractère alphabétique en majuscule (avec l'espace, le tiret et l'appostrophe)
            var patt = regexAlphaMaj;
            var res = patt.test(data);
            if (res) {
                msgErreur = "Les caractères autorisées sont les majuscules, l'espace, le tiret et l'appostrophe";
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            }
        }

        if (regexp == "alphaMin") {
            // autorise les caractère alphabétique en minuscule (avec l'espace, le tiret et l'appostrophe)
            var patt = regexAlphaMin;
            var res = patt.test(data);
            if (res) {
                msgErreur = "Les caractères autorisées sont les minuscules, l'espace, le tiret et l'appostrophe";
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            }
        }

        if (regexp == "num") {
            // autorise les caractère alphabétique en majuscule et minuscule (avec l'espace et le tiret)
            var patt = regexNum;
            var res = patt.test(data);
            if (res) {
                msgErreur = "Les caractères autorisées sont les chiffres";
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            }
        }

        if (regexp == "alphaNum") {
            // autorise les caractère alphabétique en majuscule et minuscule (avec l'espace et le tiret)
            var patt = regexNum;
            var res = patt.test(data);
            if (res) {
                msgErreur = "Les caractères autorisées sont les minuscules, les majuscules, les chiffres, l'espace, le tiret et l'appostrophe";
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            }
        }

        if (regexp == "alphaNumMin") {
            // autorise les caractère alphabétique en majuscule et minuscule (avec l'espace et le tiret)
            var patt = regexNum;
            var res = patt.test(data);
            if (res) {
                msgErreur = "Les caractères autorisées sont les minuscules, les chiffres, l'espace, le tiret et l'appostrophe";
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            }
        }

        if (regexp == "alphaNumMaj") {
            // autorise les caractère alphabétique en majuscule et minuscule (avec l'espace et le tiret)
            var patt = regexNum;
            var res = patt.test(data);
            if (res) {
                msgErreur = "Les caractères autorisées sont les majuscules, les chiffres, l'espace, le tiret et l'appostrophe";
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            }
        }


        errorForms(context, '', 'success');
        return true;

    }



    if (type == "number") {

        if (minimum) {
            if (data.length < minimum) {
                msgErreur = "La valeur maximum possiblee est : " + minimum;
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            }
        }

        if (maximum) {
            if (data.length > maximum) {
                msgErreur = "La valeur maximum possiblee est : " + maximum;
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            }
        }

        errorForms(context, '', 'success');
        return true;

    }

    if (type == "password") {

        if (minimum) {
            if (data.length < minimum) {
                msgErreur = "La valeur attendu est trop court => " + minimum + " caratères attendu minimum";
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            }
        }

        if (maximum) {
            if (data.length > maximum) {
                msgErreur = "La valeur attendu est trop long => " + maximum + " caratères attendu maximun";
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            }
        }

        if (regexp == "password") {
            // autorise les caractères alphabétique, numérique et quelques caractères spéciaux ( & ~ # ^ @ = $ * ! : ; , ? . / § - + ° )
            var patt = regexPassword;
            var res = patt.test(data);
            if (res) {
                msgErreur = "Les caractères autorisées sont les minuscules, les majuscules, l'espace, le tiret et l'appostrophe";
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            }
        }

        errorForms(context, '', 'success');
        return true;

    }

}

// Validation d'un mail
function validMail(texte, context, min, max) {

    var minimum = (min != "" || min != 0) ? min : false;
    var maximum = (max != "" || max != 0) ? max : false;
    var msgErreur;

    // data contient les données brute du champ mail
    var data = texte.trim();
    // user contient une copie de data pour le test de l'arobase
    var user = data;
    var mail = data;

    var domaine;
    var extension;

    var testArobase = false;
    var testPoint = false;

    // test de longueur minimum	
    if (minimum) {
        if (data.length < minimum) {
            msgErreur = "La valeur attendu est trop court => " + minimum + " caratères attendu minimum";
            type = "error";
            errorForms(context, msgErreur, type);
            return false;
        }
    }

    // test de longueur maximum
    if (maximum) {
        if (data.length > maximum) {
            msgErreur = "La valeur attendu est trop long => " + maximum + " caratères attendu maximun";
            type = "error";
            errorForms(context, msgErreur, type);
            return false;
        }
    }

    // Vérification si 2 caractères spéciaux ou plus se suivre
    var allPoint = (data.match(/[.]{2,}/g)) ? true : false;
    var allTiret = (data.match(/[-]{2,}/g)) ? true : false;
    var allUnderscore = (data.match(/[_]{2,}/g)) ? true : false;
    var ultime = (data.match(/(\-\.\_|\-\_\.|\.\-\_|\.\_\-|\_\.\-|\_\-\.|\-\.|\-\_|\_\-|\_\.|\.\-|\.\_)/g)) ? true : false;

    // si plus de 2 point successif
    if (allPoint) {
        msgErreur = "Il y a trop de point \".\" successif pour valider votre adresse mail";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // si plus de 2 tiret successif
    if (allTiret) {
        msgErreur = "Il y a trop de tiret \"-\" successif pour valider votre adresse mail";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // si plus de 2 underscore successif
    if (allUnderscore) {
        msgErreur = "Il y a trop de underscore \"_\" successif pour valider votre adresse mail";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // si plusieurs caractères spéciaux successif
    if (ultime) {
        msgErreur = "Il y a trop de caractères spéciaux successif pour valider votre adresse mail";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // Vérification de l'arobase
    var res = (data.match(/@/g) != null) ? data.match(/@/g).length : 0;
    if (res < 1) {
        msgErreur = "Il manque l'arobase @ pour avoir une adresse mail valide";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    } else if (res > 1) {
        msgErreur = "Un seul caractère arobase @ est autorisé pour avoir une adresse mail valide";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    } else { // ici on sait qu'il y a qu'un seul signe arobase


        // on coupe en 2 pour tester la partie user
        var username = user.split("\@");

        // on créee la regex qui valide cette partie 
        // la partie username doit commencer par un caractère alphabétique et en minuscule
        // puis il peut y avoir des caractères alphanumérique, le tiret, le point et l'underscore
        var pattUsername = /^[a-z]+[a-z0-9-_.]+[a-z0-9]{1}$/;
        var reponse = pattUsername.test(username[0]);
        if (!reponse) {
            msgErreur = "Le nom d'utilisateur comporte des caractères invalide";
            type = "error";
            errorForms(context, msgErreur, type);
            return false;
        }

        // si le test réussit on va tester la deuxième partie soit le domaine et l'extension
        if (username[1] == "") {
            msgErreur = "Il manque le nom de domaine et son extension";
            type = "error";
            errorForms(context, msgErreur, type);
            return false;

        } else {

            // premièrement on test si il y a des caractère après l'arobase
            if (username[1].length < 2) {
                msgErreur = "Le nom de domaine ou son extension est invalide";
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            }

            // puis on vérifie le nombre de point présent
            // domext contien le domaine et l'extension
            var domext = username[1];
            // compteurPoint contient le nombre de points
            var compteurPoint = (domext.match(/[.]/g) != null) ? domext.match(/[.]/g).length : "erreur";

            if (compteurPoint == "erreur") {
                msgErreur = "Il manque l'extension du domaine";
                type = "error";
                errorForms(context, msgErreur, type);
                return false;
            } else {

                /* ici on a le domaine et l'extension que l'on doit vérifier
                Mais avant de vérifier on doit s'assurer de bien récupérer l'extension
                car il peut y avaoir plusieurs points dans le nom de domaine
                Ayant compter le nombre de points précedemment on peut en déduire 
                que le dernier sera celui de l'extension mais on pourra jamais en être certain !!! */

                // Ici on découpe tout la partie droite de l'arobase sur tous les points
                var explose = domext.split("\.");

                // on en récupère l'extension via compteurPoint
                extension = explose[compteurPoint];

                // msg(explose);  // tableau domanie avec extension
                // msg(compteurPoint); // nombre de point
                // msg(extension); // extension en fonction du dernier point

                // on compte le nombre de caractères pour l'extension
                var nb_ext = extension.length;

                // on compte le nombre de caractère pour le domanie et l'extension
                var nb_dom = domext.length;

                // on compte le nombre de caractère pour le domaine
                var nb_char = nb_dom - nb_ext - 1;

                // on coupe le domaine ( longeur du domaine + extension - 1 pour le point de l'ext)
                domaine = domext.substring(0, nb_char);

                // msg(domaine) // ici on a le domaine
                // msg(extension) // ici on a l'extension

                // on verifie que le nom de domaine est suppérrieur à 2 caractères
                if (domaine.length < 2) {
                    msgErreur = "Le nom de domaine est trop court minimum 3 caractères attendu";
                    type = "error";
                    errorForms(context, msgErreur, type);
                    return false;
                }

                // on verifie que le nom de domaine est  inférieur ou égale à 50 caratères
                if (domaine.length >= 50) {
                    msgErreur = "Le nom de domaine est trop long maximun 50 caratères attendu";
                    type = "error";
                    errorForms(context, msgErreur, type);
                    return false;
                }

                // on verifie que l'extension est suppérrieur à 2 caractère
                if (extension.length < 2) {
                    msgErreur = "L'extension du domaine est trop courte minumum 2 caractèes attendu";
                    type = "error";
                    errorForms(context, msgErreur, type);
                    return false;
                }

                // on verifie que l'extension est inférieur ou égale à 6 caractères
                if (extension.length >= 6) {
                    msgErreur = "L'extension du domaine est trop longue maximum 6 caractèes attendu";
                    type = "error";
                    errorForms(context, msgErreur, type);
                    return false;
                }

                // on verifie que l'extension ne contient aucun chiffres ou caractères spéciaux
                var pattMail = /[0-9-_.]/g;
                var reponse2 = pattMail.test(extension);
                if (reponse2) {
                    msgErreur = "Les chiffres ou caractère spéciaux ne sont pas autorisé dans l'extension'";
                    type = "error";
                    errorForms(context, msgErreur, type);
                    return false;
                }

            }
        }
    }



    // ici on peut en déduire d'avoir un email valide
    errorForms(context, '', 'success');
    return true;

}


//validation d'un select
function validSelect(valeur, type, context, min, max) {

    var minimum = (min != "" || min != 0) ? min : false;
    var maximum = (max != "" || max != 0) ? max : false;
    var msgErreur;

    if (valeur > maximum) {
        msgErreur = "Erreur ce choix ne semble pas disponible dans la liste";
        type = "error";
        errorForms(context, msgErreur, type);
        $(context).next().removeClass('glyphicon-ok glyphicon-remove glyphicon-warning-sign');
        return false;
    }

    if (valeur < minimum) {
        msgErreur = "Vous devez faire une selection dans la liste";
        type = "error";
        errorForms(context, msgErreur, type);
        $(context).next().removeClass('glyphicon-ok glyphicon-remove glyphicon-warning-sign');
        return false;
    }

    errorForms(context, '', 'success');
    $(context).next().removeClass('glyphicon-ok glyphicon-remove glyphicon-warning-sign');
    return true;

}

//validation d'un checkox
function validCheckbox(valeur, type, context, min, max) {

    var minimum = (min != "" || min != 0) ? min : false;
    var maximum = (max != "" || max != 0) ? max : false;
    var msgErreur;

    if (valeur > maximum) {
        msgErreur = "Le maximum de choix est " + maximum;
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    if (valeur < minimum) {
        msgErreur = "Le minimum de choix est " + minimum;
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    errorForms(context, '', 'success');
    return true;

}

//validation d'un radio button
function validRadio(valeur, type, context, min, max) {

    var minimum = (min != "" || min != 0) ? min : false;
    var maximum = (max != "" || max != 0) ? max : false;
    var msgErreur;

    if (valeur > maximum) {
        msgErreur = "Le maximum de choix est " + maximum;
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    if (valeur < minimum) {
        msgErreur = "Le minimum de choix est " + minimum;
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    errorForms(context, '', 'success');
    return true;

}

//validation d'une date
function validDate(valeur, type, context, amin, amax) {

    var minimum = (amin != "" || amin != 0) ? amin : false;
    var maximum = (amax != "" || amax != 0) ? amax : false;
    var msgErreur;


    // vérification si date vide
    if (valeur == "" || valeur == 0 || valeur == null || valeur == undefined) {
        msgErreur = "La date doit être renseigné pour valider le formulaire";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // verification si date est au bon format
    var compteurSlash = (valeur.match(/[/]/g) != null) ? valeur.match(/[/]/g).length : "erreur";
    if (compteurSlash != 2) {
        msgErreur = "La date n'est pas au bon format jour/mois/année ex : 31/12/2014";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    var date = (valeur != "" || valeur != 0) ? valeur.split("/") : false;
    var jour = date[0];
    var mois = date[1];
    var annee = date[2];

    // Vérification si c'est bien des nombres
    var patt = regexNum;

    // le jour
    var resJ = patt.test(jour);
    if (resJ) {
        msgErreur = "Seul les caractères numériques sont autorisé pour les jours";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // le mois
    var resM = patt.test(mois);
    if (resM) {
        msgErreur = "Seul les caractères numériques sont autorisé pour les mois";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // l'Année
    var resA = patt.test(annee);
    if (resA) {
        msgErreur = "Seul les caractères numériques sont autorisé pour l'année";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }


    // vérification du jour valide
    if (jour < 0 || jour > 31 || jour.length > 2) {
        msgErreur = "Le jour doit être comprit entre 1 est 31";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // vérification du mois valide
    if (mois < 0 || mois > 12 || mois.length > 2) {
        msgErreur = "Le mois doit être comprit entre 1 est 12";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // vérification de l'année valide
    if (annee.length != 4) {
        msgErreur = "L'année doit être sur 4 chiffres Exemple : 2014 ";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // Vérification si l'année est bien suppérieur au minimum fixé
    if (amin != false) {
        if (annee < amin) {
            msgErreur = "L'année doit être suppérieur à : " + amin;
            type = "error";
            errorForms(context, msgErreur, type);
            return false;
        }
    }

    // Vérification si l'année est bien inférieur au maximum fixé
    if (amax != false) {
        if (annee >= amax) {
            msgErreur = "L'année doit être inférieur à : " + amax;
            type = "error";
            errorForms(context, msgErreur, type);
            return false;
        }
    }

    // Ici la date est validé
    errorForms(context, '', 'success');
    return true;

}

//validation d'une heure
function validHeure(valeur, type, context) {

    var msgErreur;

    // vérification si date vide
    if (valeur == "" || valeur == 0 || valeur == null || valeur == undefined) {
        msgErreur = "L'heure doit être renseigné pour valider le formulaire";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // verification si l'heure est au bon format
    var compteurPoint = (valeur.match(/[:]/g) != null) ? valeur.match(/[:]/g).length : "erreur";
    if (compteurPoint != 1) {
        msgErreur = "La l'heure n'est pas au bon format heure:minute ex : 23:59";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    var temps = (valeur != "" || valeur != 0) ? valeur.split(":") : false;
    var heure = temps[0];
    var minute = temps[1];

    // Vérification si c'est bien des nombres
    var patt = regexNum;

    // les heures
    var resH = patt.test(heure);
    if (resH) {
        msgErreur = "Seul les caractères numériques sont autorisé pour les heures";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // les minutes
    var resM = patt.test(minute);
    if (resM) {
        msgErreur = "Seul les caractères numériques sont autorisé pour les minutes";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }


    // vérification d'une heure valide
    if (heure < 0 || heure > 23 || heure.length > 2) {
        msgErreur = "L'heure doit être comprit entre 0 est 23";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // vérification des minutes valide
    if (minute < 0 || minute > 59 || minute.length > 2) {
        msgErreur = "Les minutes doivent être comprises entre 0 est 59";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    msg(heure)
    msg(heure.trim())
        // Ici on a une heure et minutes validé
    errorForms(context, '', 'success');
    return true;

}


//validation d'une heure
function validTel(valeur, type, context) {

    var tel = valeur;
    var msgErreur;

    // vérification si date vide
    if (valeur == "" || valeur == 0 || valeur == null || valeur == undefined) {
        msgErreur = "Le téléphone doit être renseigné pour valider le formulaire";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // verification si le téléphone est au bon format ( autorise espace, le tiret, et sans espace )

    // on compte le nombre de caractère ( minimum 10, maximum 14)
    if (valeur.length < 10 || valeur.length > 14) {
        msgErreur = "Le téléphone doit être au complet minimum 10 chiffres et maximun 14 avec les espaces ou tiret";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // Téléphone sans espace
    if (valeur.length == 10) {
        // vérifie qu'il contient bien que des chiffres
        var patt = regexNum;
        var res = patt.test(tel);
        if (res) {
            msgErreur = "Seul les caractères numériques sont autorisé pour un numéro de téléphone";
            type = "error";
            errorForms(context, msgErreur, type);
            return false;
        }
    }

    // Téléphone avec espace ou tiret
    if (valeur.length == 10) {
        // vérifie qu'il contient bien que des chiffres
        var patt = regexNum;
        var res = patt.test(tel);
        if (res) {
            msgErreur = "Seul les caractères numériques sont autorisé pour un numéro de téléphone";
            type = "error";
            errorForms(context, msgErreur, type);
            return false;
        }
    }




    var compteurPoint = (valeur.match(/[:]/g) != null) ? valeur.match(/[:]/g).length : "erreur";
    if (compteurPoint != 1) {
        msgErreur = "La l'heure n'est pas au bon format heure:minute ex : 23:59";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    var temps = (valeur != "" || valeur != 0) ? valeur.split(":") : false;
    var heure = temps[0];
    var minute = temps[1];

    // Vérification si c'est bien des nombres
    var patt = regexNum;

    // les heures
    var resH = patt.test(heure);
    if (resH) {
        msgErreur = "Seul les caractères numériques sont autorisé pour les heures";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // les minutes
    var resM = patt.test(minute);
    if (resM) {
        msgErreur = "Seul les caractères numériques sont autorisé pour les minutes";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }


    // vérification d'une heure valide
    if (heure < 0 || heure > 23 || heure.length > 2) {
        msgErreur = "L'heure doit être comprit entre 0 est 23";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    // vérification des minutes valide
    if (minute < 0 || minute > 59 || minute.length > 2) {
        msgErreur = "Les minutes doivent être comprises entre 0 est 59";
        type = "error";
        errorForms(context, msgErreur, type);
        return false;
    }

    msg(heure)
    msg(heure.trim())
        // Ici on a une heure et minutes validé
    errorForms(context, '', 'success');
    return true;

}