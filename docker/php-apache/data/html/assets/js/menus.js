/*
 * Fonction des boîtes modal du menu d'accueil via Bootstrap
 * Fichier crée le 06/04/2014
 * Auteur : Denis Sanson alias Multixvers
 */


/*-------------------- FONCTIONS DES BOITES MODAL BOOTSTRAP DU MENU --------------------*/

// Recherche Codis
function modalCodis() {
    creerModal("codis-number", "RECHERCHER UNE INTERVENTION", "search-c", '/php/formulaires/search-codis.php', "get", 'bg-success', "");
}

// VEHICULES

function vehicules() {
    $.post('/php/pages/modal_choix.php', { type: "vehicule" }, function(arg) { $("#bloc-modal").append(arg) });
}

function addVehicules() {
    creerModal("add-vehicule", "AJOUTER UN VEHICULE", "add-v", '/php/formulaires/vehicule.php?id=-1&mode=add', "get", 'bg-success', "");
}

function modVehicules(v_id) {
    creerModal("mod-vehicule", "MODIFIER UN VEHICULE", "mod-v", '/php/formulaires/vehicule.php?id=' + v_id + '&mode=mod', "get", 'bg-warning', "");
}

function delVehicules(v_id, type) {
    modalSuppr("del-vehicule", "SUPPRIMER UN VEHICULE", "del-v", 'bg-danger', "du véhicule", type, v_id);
}

// MATERIELS

function materiels() {
    $.post('/php/pages/modal_choix.php', { type: "materiel" }, function(arg) { $("#bloc-modal").append(arg) });
}

function addMateriels() {
    creerModal("add-materiel", "AJOUTER UN MATERIEL", "add-m", '/php/formulaires/materiel.php?id=-1&mode=add', "get", 'bg-success', "");
}

function modMateriels(m_id) {
    creerModal("mod-materiel", "MODIFIER UN MATERIEL", "mod-m", '/php/formulaires/materiel.php?id=' + m_id + '&mode=mod', "get", 'bg-warning', "");
}

function delMateriels(v_id, type) {
    modalSuppr("del-materiel", "SUPPRIMER UN MATERIEL", "del-m", 'bg-danger', "du matériel", type, v_id);
}

// GROUPES

function groupes() {
    $.post('/php/pages/modal_choix.php', { type: "groupe" }, function(arg) { $("#bloc-modal").append(arg) });
}

function addGroupes() {
    creerModal("add-groupe", "AJOUTER UN GROUPE", "add-g", '/php/formulaires/groupe.php?id=-1&mode=add', "get", 'bg-success', "");
}

function modGroupes(g_id) {
    creerModal("mod-groupe", "MODIFIER UN GROUPE", "mod-g", '/php/formulaires/groupe.php?id=' + g_id + '&mode=mod', "get", 'bg-warning', "");
}

function delGroupes(v_id, type) {
    modalSuppr("del-groupe", "SUPPRIMER UN GROUPE", "del-g", 'bg-danger', "du groupe", type, v_id);
}

// CENTRES

function centres() {
    $.post('/php/pages/modal_choix.php', { type: "centre" }, function(arg) { $("#bloc-modal").append(arg) });
}

function addCentres() {
    creerModal("add-centre", "AJOUTER UN CENTRE", "add-c", '/php/formulaires/centre.php?id=-1&mode=add', "get", 'bg-success', "");
}

function modCentres(c_id) {
    creerModal("mod-centre", "MODIFIER UN CENTRE", "mod-c", '/php/formulaires/centre.php?id=' + c_id + '&mode=mod', "get", 'bg-warning', "");
}

function delCentres(v_id, type) {
    modalSuppr("del-centre", "SUPPRIMER UN CENTRE", "del-c", 'bg-danger', "du centre", type, v_id);
}

// COMMUNES

function communes() {
    $.post('/php/pages/modal_choix.php', { type: "commune" }, function(arg) { $("#bloc-modal").append(arg) });
}

function addCommunes() {
    creerModal("add-commune", "AJOUTER UNE COMMUNE", "add-c", '/php/formulaires/commune.php?id=-1&mode=add', "get", 'bg-success', "");
}

function modCommunes(c_id) {
    creerModal("mod-commune", "MODIFIER UNE COMMUNE", "mod-c", '/php/formulaires/commune.php?id=' + c_id + '&mode=mod', "get", 'bg-warning', "");
}

function delCommunes(v_id, type) {
    modalSuppr("del-commune", "SUPPRIMER UNE COMMUNE", "del-c", 'bg-danger', "de la commune", type, v_id);
}

// NATURES

function natures() {
    $.post('/php/pages/modal_choix.php', { type: "nature" }, function(arg) { $("#bloc-modal").append(arg) });
}

function addNatures() {
    creerModal("add-nature", "AJOUTER UNE NATURE", "add-c", '/php/formulaires/nature.php?id=-1&mode=add', "get", 'bg-success', "");
}

function modNatures(n_id) {
    creerModal("mod-nature", "MODIFIER UNE NATURE", "mod-c", '/php/formulaires/nature.php?id=' + n_id + '&mode=mod', "get", 'bg-warning', "");
}

function delNatures(n_id, type) {
    modalSuppr("del-nature", "SUPPRIMER UNE NATURE", "del-c", 'bg-danger', "de la nature", type, n_id);
}

// PRECISION

function precisions() {
    $.post('/php/pages/modal_choix.php', { type: "precision" }, function(arg) { $("#bloc-modal").append(arg) });
}

function addPrecisions() {
    creerModal("add-precision", "AJOUTER UNE PRECISION", "add-c", '/php/formulaires/precision.php?id=-1&mode=add', "get", 'bg-success', "");
}

function modPrecisions(p_id) {
    creerModal("mod-precision", "MODIFIER UNE PRECISION", "mod-c", '/php/formulaires/precision.php?id=' + p_id + '&mode=mod', "get", 'bg-warning', "");
}

function delPrecisions(p_id, type) {
    modalSuppr("del-precision", "SUPPRIMER UNE PRECISION", "del-c", 'bg-danger', "de la precision", type, p_id);
}

// CONSULTER UNE INTERVENTION

function consulter() {
    $.post('/php/pages/modal_choix.php', { type: "intervention" }, function(arg) { $("#bloc-modal").append(arg) });
}

// RAPPORT D'INTERVENTION

function rapports() {
    creerModal("rapports", "RAPPORT D'INTERVENTION", "rapport", '/php/pages/search_rapport.php', "get", 'bg-success', "");
}

// TABLEAU DE GARDE

function affectation() {
    creerModal("affectations", "TABLEAU DE GARDE", "affectation", '/php/formulaires/affectation.php', "get", 'bg-info', "");
}

// MESSAGE RADIO RELEVE

function radio(id) {
    creerModal("radio", "RELEVE", "radios", '/php/pages/messages_radio.php?id=' + id, "get", 'bg-info', "");
}

function send_radio() {
    var iid = $("input[name=id]").val();
    var message = $("textarea[name=message_radio2]").val();
    var obj = { id: iid, msg: message };
    $.post('/php/requetes/send_radio.php', { radio: obj }).always(function() {
        viderModal();
    });
}

// CONSIGNE

function consignes() {
    creerModal("consignes", "CONSIGNE", "consigne", '/php/formulaires/consignes.php', "get", 'bg-success', "");
}

// PERMANENCE

function permanence() {
    creerModal("permanences", "PERMANENCE", "permanence", '/php/formulaires/permanence.php', "get", 'bg-info', "");
}


// PERSONNEL

function edit_personnel(p_id) {
    creerModal("personnels-edit", "MODIFIER LE PERSONNEL", "edit-personnel", "/php/formulaires/modif-personnel.php?id=" + p_id, "get", 'bg-warning', "");
}

function depart_personnel(id, nom) {
    if (id != undefined) {
        do {
            var quit = formatDate(new Date(), "dd/mm/yyyy hh:nn");
            var textes = 'Confirmer que ' + nom + ' va quitter la permanence CI à : ';
            var titres = "QUITTER LA PERMANENCE CI";
            var rep_dt = null;
            $.post("/php/requetes/confirm.php", { titre: titres, texte: textes, date: quit }).always(function(arg) {

                viderModal();
                $('#bloc-modal').append(arg);
                $('#confirm').modal("show");
                $("#del-confirm").on("click", function() {
                    rep_dt = "ok";
                    if (dt = isDatetimeValid(quit)) {
                        $.getScript("/php/requetes/depart_personnel.php?id=" + id + "&dt=" + dt);
                        notif("Le personnel " + nom + " à bien quitté la caserne", "success", "", "");
                        updateIntelligent();
                        $("#bloc-modal").empty();
                        $('.modal-backdrop').remove();
                    } else {
                        notif("La date et/ou l'heure sont invalide.", "danger", "", "");
                    }
                });

                $("#del-annuler").on("click", function() {
                    notif("L'opération à été annuler par l'utilisateur", "warning", "", "");
                });

            });

        } while (rep_dt != null && dt == 0);
    }
}

function undo_arrive_personnel(id, nom) {
    if (id != undefined) {
        var textes = "Confirmer la suppression du personnel : " + nom;
        var titres = "CONFIRMATION DE SUPPRESSION";

        $.post("/php/requetes/confirm.php", { titre: titres, texte: textes }).always(function(arg) {

            viderModal();
            $('#bloc-modal').append(arg);
            $('#confirm').modal("show");
            $("#del-confirm").on("click", function() {
                $.getScript("/php/requetes/undo_arrive_personnel.php?id=" + id);
                notif("Le personnel " + nom + " à bien été supprimer", "success", "", "");
                updateIntelligent();
                $("#bloc-modal").empty();
                $('.modal-backdrop').remove();
            });

            $("#del-annuler").on("click", function() {
                notif("L'opération à été annuler par l'utilisateur", "warning", "", "");
            });

        });

    }
}

// MENU CONTEXUEL

function rightClick(e) {
    e.preventDefault();
    if (e.target.attributes.data != undefined) {
        var temp = e.target.attributes.data.value;
        var id = temp.substring(4);
        var type = temp.substring(0, 3);
        if (type == "mat") {
            modMateriels(id);
        }
        if (type == "veh") {
            modVehicules(id);
        }
        updateIntelligent();
    }
}


/*-------------------- OUVERTURE DE FENÊTRES --------------------*/
function popup(page, width, height, center, nom) {
    if (nom == undefined) nom = '';
    w = window.open(page, nom, 'width=' + width + ',height=' + height + ',directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,toolbar=no');
    if (center != false) {
        w.moveTo(screen.availWidth / 2 - 0.56 * width, screen.availHeight / 2 - 0.56 * height);
    }
}

function print_permanence(id) {
    param = (id == undefined) ? '' : '?id=' + id;
    window.open('/php/pages/print_permanence.php' + param);
    return false;
}

function splash_recap(id) {
    param = (id == undefined) ? '' : '?id=' + id;
    creerModal("recap-depart", "Récapitulatif de l'intervention N° " + id, "inter-" + id, '/php/pages/recap_feuille_depart.php' + param, "get", 'bg-danger', "");
}

function print_recap(id) {
    param = (id == undefined) ? '' : '?id=' + id;
    popup('/php/pages/recap_feuille_depart.php' + param + '&print', 600, 500);
}

/* CONNEXION UTILISATEUR */

function login() {
    creerModal("login-user", "CONNEXION", "login-u", '/admin/membres/users/login.php', "get", 'bg-success', "");
}

function logout(data) {
    $.post('/admin/membres/users/logout.php', { id: data }).always(function(arg) {
        if (arg.retour == "ok") {
            notif("Vous être bien déconnecter, au-revoir est à bientôt", "success", "", "");
            setTimeout(function() {
                var url = window.location.origin;
                location.assign(url);
            }, 2000);

        } else {
            notif("Une erreur est survenue lors de la déconnexion : " + arg.retour, "danger", "", "");
        }
    });
}

// Voir les mises à jours
function visuInfo() {
    var url = window.location.origin + "/admin";
    location.assign(url);
}