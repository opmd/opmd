/*
 * Fonction des boîte de dialogue dites Modal via Bootstrap
 * Fichier crée le 06/04/2014
 * Auteur : Denis Sanson alias Multixvers
 */

// Variable global contenant ID de la dernière Moadal ouverte
var idBlocModal;
var loader;

/*-------------------- FONCTIONS BOITE MODAL JQUERY ET BOOTSTRAP --------------------*/

// Permet de vider une boite modal
function viderModal() {
    $('body').removeClass('modal-open');
    $("#bloc-modal").empty();
    $('.modal-backdrop').remove();
}

// Boite modal standart
function creerModal(idModal, titre, idBody, fichier, mode, classM, body) {

    viderModal();
    var code = "";
    code += "<div class='modal fade' id='" + idModal + "' tabindex='-1' role='dialog' aria-labelledby='dial-" + idModal + "' aria-hidden='true'>";
    code += "<div class='modal-dialog modal-lg'>";
    code += "<div class='modal-content' style='border-radius:20px;'>";
    code += "<div class='modal-header " + classM + "' style='border-radius:20px;'>";
    code += "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
    code += "<h1 class='modal-title text-center' id='dial-" + idModal + "'>" + titre + "</h1>";
    code += "</div>";
    code += "<div class='modal-body'>";

    if (fichier == false) {
        code += body;
    } else {
        code += "<div id='" + idBody + "'></div>";
    }

    code += "</div>";
    code += "</div>";
    code += "</div>";
    code += "</div>";

    $("#bloc-modal").append(code);
    if (mode == 'post') {

    } else if (mode == "get") {
        $.get(fichier, function(arg) {
            $("#" + idBody).append(arg);
        });
    } else {
        if (fichier != "") {
            $("#" + idBody).load(fichier);
        }
    }

    $('#' + idModal).modal("show");

    $('#' + idModal).on('hidden.bs.modal', function(e) {
        viderModal();
    });

    code = "";
}


// Boîte Modal différée
function creerModalDiff(idModal, titre, idBody, fichier, mode, classM, show) {

    viderModal();
    var code = "";
    code += "<div class='modal fade' id='" + idModal + "' tabindex='-1' role='dialog' aria-labelledby='dial-" + idModal + "' aria-hidden='true'>";
    code += "<div class='modal-dialog modal-lg'>";
    code += "<div class='modal-content' style='border-radius:20px;'>";
    code += "<div class='modal-header " + classM + "' style='border-radius:20px;'>";
    code += "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
    code += "<h1 class='modal-title text-center' id='dial-" + idModal + "'>" + titre + "</h1>";
    code += "</div>";
    code += "<div class='modal-body'>";

    if (fichier == false) {
        code += body;
    } else {
        code += "<div id='" + idBody + "'></div>";
    }

    code += "</div>";
    code += "</div>";
    code += "</div>";
    code += "</div>";

    $("#bloc-modal").append(code);
    if (mode == 'post') {

    } else if (mode == "get") {
        $.get(fichier, function(arg) {
            $("#" + idBody).append(arg);
        });
    } else {
        if (fichier != "") {
            $("#" + idBody).load(fichier);
        }
    }

    if (show == true) {
        $('#' + idModal).modal("show");
    }

    $('#' + idModal).on('hidden.bs.modal', function(e) {
        viderModal();
    });

    code = "";
}

// Boite modal de suppression
function modalSuppr(idModal, titre, idBody, classM, texte, type, id) {

    viderModal();
    var code = "";
    code += "<div class='modal fade' id='" + idModal + "' tabindex='-1' role='dialog' aria-labelledby='dial-" + idModal + "' aria-hidden='true'>";
    code += "<div class='modal-dialog modal-lg'>";
    code += "<div class='modal-content' style='border-radius:20px;'>";
    code += "<div class='modal-header " + classM + "' style='border-radius:20px;'>";
    code += "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
    code += "<h1 class='modal-title text-center' id='dial-" + idModal + "'>" + titre + "</h1>";
    code += "</div>";
    code += "<div class='modal-body'>";

    code += "<div id='" + idBody + "'>";

    code += "<form class='form-horizontal' role='form' id='form-" + type + "' method='POST' onsubmit='return false'>";

    code += "<div class='form-group'>";
    code += "<div class='col-sm-12'>";
    code += "<h2>Merci de bien vouloir confirmer la suppression : <b>" + texte + "</b></h2>";
    code += "</div>";
    code += "</div>";

    code += "<div class='form-group'>";
    code += "<div class='col-sm-offset-4 col-sm-2'>";
    code += "<button type='submit' class='btn btn-primary' data-dismiss='modal'>Annuler</button>";
    code += "</div>";
    code += "<div class='col-sm-2'>";
    code += "<button type='submit' class='btn btn-danger' id='del-" + type + "'>Confirmer</button>";
    code += "</div>";
    code += "</div>";

    code += "</form>";

    code += "</div>";


    code += "</div>";
    code += "</div>";
    code += "</div>";
    code += "</div>";

    $("#bloc-modal").append(code);
    $('#' + idModal).modal("show");

    $("#del-" + type).on("click", function() {
        var data = { id: id, del: true };
        $.post("/php/formulaires/" + type + ".php", data, function(arg) {

            if (trim(arg) == "ok") {
                $('#' + idModal).modal("hide");
                viderModal();
                notif("Votre " + type + " à bien été supprimer avec succès", "success", "", "");
                if (type == "utilisateur") {
                    viewUsers();
                } else {
                    updateIntelligent();
                }
            } else {
                $('#' + idModal).modal("hide");
                viderModal();
                $("#bloc-error").append(arg);
                notif("Votre " + type + " a rencontrer une ERREUR lors de la suppression !", "danger", "", "");

            }

        });
    });

    $('#' + idModal).on('hidden.bs.modal', function(e) {
        viderModal();
    });

    code = "";

}

// Boite modal de confirmation de suppression générique
function modalConfirm(idModal, titre, idBody, classM, texte) {

    viderModal();
    var code = "";
    code += "<div class='modal fade' id='" + idModal + "' tabindex='-1' role='dialog' aria-labelledby='dial-" + idModal + "' aria-hidden='true'>";
    code += "<div class='modal-dialog modal-lg'>";
    code += "<div class='modal-content' style='border-radius:20px;'>";
    code += "<div class='modal-header " + classM + "' style='border-radius:20px;'>";
    code += "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
    code += "<h1 class='modal-title text-center' id='dial-" + idModal + "'>" + titre + "</h1>";
    code += "</div>";
    code += "<div class='modal-body'>";

    code += "<div id='" + idBody + "'>";

    code += "<form class='form-horizontal' role='form' id='form-" + idBody + "' method='POST' onsubmit='return false'>";

    code += "<div class='form-group'>";
    code += "<div class='col-sm-12'>";
    code += "<h3 class='text-center'>" + texte + "</h3>";
    code += "</div>";
    code += "</div>";

    code += "<div class='form-group'>";
    code += "<div class='col-sm-offset-4 col-sm-2'>";
    code += "<button type='submit' class='btn btn-primary' data-dismiss='modal'>Annuler</button>";
    code += "</div>";
    code += "<div class='col-sm-2'>";
    code += "<button type='submit' class='btn btn-danger' id='del-" + idBody + "'>Confirmer</button>";
    code += "</div>";
    code += "</div>";

    code += "</form>";

    code += "</div>";


    code += "</div>";
    code += "</div>";
    code += "</div>";
    code += "</div>";

    $("#bloc-modal").append(code);
    $('#' + idModal).modal("show");

    $("#del-" + idBody).on("click", function() {
        return "ok";
    });

    $('#' + idModal).on('hidden.bs.modal', function(e) {
        viderModal();
    });

    code = "";
}

// Boite modal de confirmation de suppression de personnel
function modalConfirmPerso(id, nom) {

    viderModal();
    var code = "";
    code += "<div class='modal fade' id='suppr-p' tabindex='-1' role='dialog' aria-labelledby='dial-suppr-p' aria-hidden='true'>";
    code += "<div class='modal-dialog modal-lg'>";
    code += "<div class='modal-content' style='border-radius:20px;'>";
    code += "<div class='modal-header bg-danger' style='border-radius:20px;'>";
    code += "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
    code += "<h1 class='modal-title text-center' id='dial-suppr-p'>Supprimer ou faire quitter du personnel de la permanence CI</h1>";
    code += "</div>";
    code += "<div class='modal-body'>";

    code += "<div id='choix-p'>";

    code += "<form class='form-horizontal' role='form' id='form-supp' method='POST' onsubmit='return false'>";

    code += "<div class='form-group'>";
    code += "<div class='col-sm-12'>";
    code += "<h3 class='text-center'>Faites votre choix pour : " + nom + "</h3>";
    code += "</div>";
    code += "</div>";

    code += "<div class='form-group'>";
    code += "<div class='col-sm-offset-3 col-sm-2'>";
    code += "<button type='submit' class='btn btn-primary' data-dismiss='modal'>Annuler</button>";
    code += "</div>";
    code += "<div class='col-sm-2'>";
    code += "<button type='submit' class='btn btn-danger' onclick='undo_arrive_personnel(" + id + ",\"" + nom + "\")'>Supprimer</button>";
    code += "</div>";
    code += "<div class='col-sm-2'>";
    code += "<button type='submit' class='btn btn-warning' onclick='depart_personnel(" + id + ",\"" + nom + "\");'>Quitter la permanence</button>";
    code += "</div>";
    code += "</div>";

    code += "</form>";

    code += "</div>";


    code += "</div>";
    code += "</div>";
    code += "</div>";
    code += "</div>";

    $("#bloc-modal").append(code);
    $('#suppr-p').modal("show");

    /*$("#del-"+type).on("click",function(){
    	return "ok";
    });*/

    $('#suppr-p').on('hidden.bs.modal', function(e) {
        viderModal();
    });

    code = "";
}

// Boite modal de suppression
function modalError(idModal, titre, idBody, fichier, mode, classM, body, icone, couleur) {

    viderModal();
    var code = "";
    code += "<div class='modal fade' id='" + idModal + "' tabindex='-1' role='dialog' aria-labelledby='dial-" + idModal + "' aria-hidden='true'>";
    code += "<div class='modal-dialog modal-lg' style='border-radius:20px;width:80%;'>";
    code += "<div class='modal-content' style='border-radius:20px;width:100%;'>";
    code += "<div class='modal-header " + classM + "' style='border-radius:20px;width:100%;'>";
    code += "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
    code += "<h1 class='modal-title text-center' id='dial-" + idModal + "'><span class='glyphicon glyphicon-" + icone + "'    style='color:" + couleur + "'></span> " + titre + "</h1>";
    code += "</div>";
    code += "<div class='modal-body'>";

    if (fichier == false) {
        code += body;
    } else {
        code += "<div id='" + idBody + "'></div>";
    }

    code += "</div>";
    code += "</div>";
    code += "</div>";
    code += "</div>";

    $("#bloc-modal").append(code);
    if (mode == 'post') {

    } else if (mode == "get") {
        $.get(fichier, function(arg) {
            $("#" + idBody).append(arg);
        });
    } else {
        if (fichier != "") {
            $("#" + idBody).load(fichier);
        }
    }

    $('#' + idModal).modal("show");

    $('#' + idModal).on('hidden.bs.modal', function(e) {
        viderModal();
    });

    code = "";
}

// Boite modal standart pour le developpement
function creerModalDev(idModal, titre, idBody, fichier, mode, classM, body, icone, couleur) {

    viderModal();
    var code = "";
    code += "<div class='modal fade' id='" + idModal + "' tabindex='-1' role='dialog' aria-labelledby='dial-" + idModal + "' aria-hidden='true'>";
    code += "<div class='modal-dialog modal-lg'>";
    code += "<div class='modal-content' style='border-radius:20px;'>";
    code += "<div class='modal-header " + classM + "' style='border-radius:20px;'>";
    code += "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
    code += "<h1 class='modal-title text-center' id='dial-" + idModal + "'><span class='glyphicon glyphicon-" + icone + "'    style='color:" + couleur + "'></span> " + titre + "</h1>";
    code += "</div>";
    code += "<div class='modal-body'>";

    if (fichier == false) {
        code += body;
    } else {
        code += "<div id='" + idBody + "'></div>";
    }

    code += "</div>";
    code += "</div>";
    code += "</div>";
    code += "</div>";

    $("#bloc-modal").append(code);
    if (mode == 'post') {

    } else if (mode == "get") {
        $.get(fichier, function(arg) {
            $("#" + idBody).append(arg);
        });
    } else {
        if (fichier != "") {
            $("#" + idBody).load(fichier);
        }
    }

    $('#' + idModal).modal("show");

    $('#' + idModal).on('hidden.bs.modal', function(e) {
        viderModal();
    });

    code = "";

}

// Boite modal spécial départ
function modalDepart(fichier, mode, modalRefId, show) {

    if (modalRefId != "") {
        $("#" + modalRefId).modal("hide");
        $('.modal-backdrop').remove();
    }


    $("#feuille_depart").empty();

    if (mode == 'post') {

    } else if (mode == "get") {
        $.get(fichier, function(arg) {
            $("#feuille_depart").append(arg);
        });
    } else {
        if (fichier != "") {
            $("#feuille_depart").load(fichier);
        }
    }

    if (show == true) {
        $('#depart').modal("show");
    }

    $('#feuille_depart').on('hidden.bs.modal', function(e) {
        viderModal();
    });
}

// Boite modal spécial projecteur synoptique
function modalInter(idModal, titre, classM, data, texte, classe) {

    viderModal();
    var code = "";
    code += "<div class='modal fade' id='" + idModal + "' tabindex='-1' role='dialog' aria-labelledby='dial-" + idModal + "' aria-hidden='true'>";
    code += "<div class='modal-dialog modal-lg'>";
    code += "<div class='modal-content' style='border-radius:20px;'>";
    code += "<div class='modal-header " + classM + "' style='border-radius:20px;'>";
    code += "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
    code += "<h2 class='modal-title text-center' id='dial-" + idModal + "'>" + titre + "</h2>";
    code += "</div>";
    code += "<div class='modal-body'>";
    code += "<div id='body-" + idModal + "'>";
    code += "<h4 class='text-center'>" + texte + "</h4>";
    code += "<div class='" + classe + "'><span class='text-center'>" + data + "</span></div>";
    code += "</div>";
    code += "</div>";
    code += "</div>";
    code += "</div>";
    code += "</div>";

    $("#bloc-modal").append(code);

    $('#' + idModal).modal("show");
    $('#' + idModal).on('hidden.bs.modal', function(e) {
        viderModal();
    });

    code = "";
}

// Boite modal spécial projecteur synoptique
function modalLoader() {

    viderModal();
    var code = "";
    code += "<div class='modal fade' id='loading' tabindex='-1' role='dialog' aria-labelledby='dial-loading' aria-hidden='true'>";
    code += "<div class='modal-dialog modal-lg'>";
    code += "<div class='modal-content bg-black' style='border-radius:20px;'>";
    code += "<div class='modal-header bg-black' style='border-radius:20px;'>";
    code += "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
    code += "<h1 class='modal-title text-center text-white' id='dial-loading'>CHARGEMENT DES DONNEES EN COURS...</h1>";
    code += "</div>";
    code += "<div class='modal-body bg-black'>";
    code += "<div id='loader' style='width:350px;height:400px'>";
    code += "</div>";
    code += "<h2 class='modal-title text-center text-apple'>FONCTIONNALITE PROCHAINNEMENT DISPONIBLE</h2>";
    code += "</div>";
    code += "</div>";
    code += "</div>";
    code += "</div>";

    $("#bloc-modal").append(code);

    $('#loading').modal("show");
    startLoad();

    $('#loading').on('hidden.bs.modal', function(e) {
        viderModal();
        stopLoad();
    });

    code = "";
}


// Loader chargeur pendant requeste d'interrogation en BDD
function startLoad() {

    var opts = {
        lines: 11, // The number of lines to draw
        length: 0, // The length of each line
        width: 20, // The line thickness
        radius: 60, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 68, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#FFF', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 46, // Afterglow percentage
        shadow: true, // Whether to render a shadow
        hwaccel: true, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: '50%', // Top position relative to parent
        left: '50%' // Left position relative to parent
    };

    var cible = document.getElementById('loader');
    loader = new Spinner(opts).spin(cible);

}

function stopLoad() {
    loader.stop();
}