/*
 * Fonction de gestion des éléments du menu feuille de départ
 * pour les véhicules/matériels/personnels/
 * Fichier crée le 04/07/2014
 * Auteur : Denis Sanson alias Multixvers
 */

/*-------------------- PARTIE VEHICULES --------------------*/

// Elements du menu

function addVehicule(div_target, id, text) {
  var nom = $('#veh-' + id).text();
  var data = "";
  addItemV(div_target, id, nom, "vehicules[]", data);
  $('#veh-' + id + ' a').removeAttr('onclick').attr('disabled', 'disabled').css({ backgroundColor: 'red', color: 'white' });
}


function removeVehicule(parent, id) {
  var nom = $('#veh-' + id).text();
  $('#veh-' + id + ' a', '#cssmenu').removeAttr('disabled').attr('onclick', "addVehicule('liste_vehicules', " + id + ", '" + nom + "')").css({ backgroundColor: 'lime', color: 'black' });
  //removeItems(parent, "v_", id);
}

// Elements de liste
function addItemV(div_target, id, text, data) {
  var element = "v_" + id
  var target = document.getElementById(div_target);
  var elem = document.createElement("span");
  var post = document.createElement("input");
  elem.id = element;
  elem.innerHTML = text;
  elem.onclick = function() {
    removeVehicule(element, id);
    removeItemsV(element);
  };
  elem.className = "label label-danger space size";
  elem.name = "vehicules[]";
  elem.value = id;
  post.type = "hidden";
  post.name = "vehicules[]";
  post.value = id;
  elem.appendChild(post);
  target.appendChild(elem);
  updateIntelligent();
}

function removeItemsV(parent) {
  $("#" + parent).remove();
  updateIntelligent();
}


/*-------------------- PARTIE MATERIELS --------------------*/

// Elements du menu
function addMateriel(div_target, id, text) {
  var txt = $('#mat-' + id).text();
  var data = ($('#mat-' + id).attr('data') != undefined) ? $('#mat-' + id).attr('data') : $('#mat-' + id).find('.dispo').text();
  var nom = txt.replace(data, "");
  var reste = new Number(data);
  if ($('#mat-' + id).find('.dispo').text() == "1") {
    $('#mat-' + id + ' a').removeAttr('onclick').attr('disabled', 'disabled').css({ backgroundColor: 'red', color: 'white' });
  }
  var datas = 0;
  var tp = ($('#m_' + id).attr('id') == 'm_' + id) ? true : false;
  if (tp) {
    var perso = $('#m_' + id).find('input').attr('data');
    var val = new Number(perso);
    datas = val + 1;
    var valeur = $('#mat-' + id).find('.dispo').text();
    var nb = new Number(valeur);
    var restant = nb - 1;
    $('#mat-' + id).find('.dispo').text(restant);
    updateItemM(div_target, id, nom, datas, datas);
  } else {
    var valeur = $('#mat-' + id).find('.dispo').text();
    var nb = new Number(valeur);
    var restant = nb - 1;
    $('#mat-' + id).find('.dispo').text(restant);
    addItemM(div_target, id, nom + " " + 1, 1);
  }
}

function removeMateriel(parent, id) {
  var valeur = $('#mat-' + id).find('.dispo').text();
  var nb = new Number(valeur);
  var restant = nb + 1;
  $('#mat-' + id).find('.dispo').text(restant);
  removeItemsM("m_" + id);
}


// Elements de liste
function addItemM(div_target, id, text, data) {
  var target = document.getElementById(div_target);
  var element = "m_" + id
  var elem = document.createElement("span");
  var post = document.createElement("input");
  elem.id = element;
  elem.innerHTML = text;
  elem.onclick = function() { removeMateriel(element, id); };
  var attr = document.createAttribute("data");
  attr.value = data;
  post.attributes.setNamedItem(attr);
  elem.className = "label label-warning space size";
  elem.name = "materiels[]";
  elem.value = id;
  post.type = "hidden";
  post.name = "materiels[]";
  post.value = id;
  elem.appendChild(post);
  target.appendChild(elem);
  updateIntelligent();
  colorM(id);
}

function updateItemM(div_target, id, text, data, mat) {
  var txt = text + " " + mat;
  if (data >= 1) {
    $("#m_" + id).find('input').attr('data', data).text(txt);
  } else {
    $("#m_" + id).find('input').attr('data', 0).text(txt);
  }
  var inp = $('input', "#m_" + id);
  $("#m_" + id).text(txt).append(inp);
  colorM(id);
}

function removeItemsM(parent) {
  var data = 0;
  var id = parent.substr(2, parent.length);
  var mat = $('#m_' + id).find('input').attr('data');
  var mate = new Number(mat);
  data = mate - 1;
  var text = $('#mat-' + id).find('.texte').text();
  text = text.replace(" " + mat, "");
  if (data >= 1) {
    updateItemM("liste_materiels", id, text, data, data);
    $('#mat-' + id + ' a').removeAttr('disabled').attr('onclick', "addMateriel('liste_materiels', " + id + ", '" + text + "')");
  } else {
    $("#" + parent).remove();
    updateItemM("liste_materiels", id, text, data, data);
    $('#mat-' + id + ' a').removeAttr('disabled').attr('onclick', "addMateriel('liste_materiels', " + id + ", '" + text + "')");
    updateIntelligent();
  }
  colorM(id);
}

function colorM(id){
  var reste = $('#mat-' + id).find('.dispo').text();
  var total = $('#mat-' + id).attr('data-total');
  if(reste != total){
    $('#mat-' + id + ' a').css({ backgroundColor: 'red', color: 'white' });
  } else {
    $('#mat-' + id + ' a').css({ backgroundColor: 'lime', color: 'black' });
  }
}


/*-------------------- PARTIE PRESENCE --------------------*/

// Elements du menu
function addPresenceExterne(div_target, id, text) {
  var data = 0;
  var tp = ($('#pe_' + id).attr('id') == 'pe_' + id) ? true : false;
  if (tp) {
    var perso = $('#pe_' + id).find('input').attr('data');
    var val = new Number(perso);
    data = val + 1;
    updateItemP(div_target, id, text, data, data);
  } else {
    addItemP(div_target, id, text + " " + 1, 1);
  }
}

function removePresenceExterne(parent, id) {
  removeItemsP(parent, "pe_" + id);
}


// Elements de liste
function addItemP(div_target, id, text, data) {
  var target = document.getElementById(div_target);
  var element = "pe_" + id
  var elem = document.createElement("span");
  var post = document.createElement("input");
  elem.id = element;
  elem.innerHTML = text;
  elem.onclick = function() { removeItemsP(div_target, element); };
  var attr = document.createAttribute("data");
  attr.value = data;
  post.attributes.setNamedItem(attr);
  elem.className = "label label-info space size prext";
  elem.name = "presencesexternes[]";
  elem.value = id;
  post.type = "hidden";
  post.name = "presencesexternes[]";
  post.value = id;
  elem.appendChild(post);
  target.appendChild(elem);
  updateIntelligent();
}

function updateItemP(div_target, id, text, data, perso) {
  var txt = text + " " + perso;
  if (data > 1) {
    $("#pe_" + id).find('input').attr('data', data).text(txt);
  } else {
    $("#pe_" + id).find('input').attr('data', 1).text(txt);
  }
  var inp = $('input', "#pe_" + id);
  $("#pe_" + id).text(txt).append(inp);
  updateIntelligent();
}

function removeItemsP(div_target, parent) {
  var data = 0;
  var id = parent.substr(3, parent.length);
  var perso = $('#pe_' + id).find('input').attr('data');
  data = perso - 1;
  var text = $('#pe_' + id).find('input').text();
  text = text.replace(" " + perso, "");
  if (data >= 1) {
    updateItemP(div_target, id, text, data, data)
  } else {
    var p = document.getElementById(div_target);
    var old = document.getElementById(parent);
    p.removeChild(old);
  }
  updateIntelligent();
}