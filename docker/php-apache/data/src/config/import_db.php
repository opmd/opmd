<?php

header('Content-type: application/json');

// verif user et droit retour ( nom du groupe, nom et prenom utilisateur, et si autorisé ou non )
if( isset($_POST['auth']) && $_POST['auth'] == true){

	$tab = array("rep" => "no");
	$user = User::isLogin();
	$name = $user->getNomFormat();
	$id = $user->getGroupe();
	$groups = new Group($id);
	$groupe = $groups->getNom();
	
	if( $id < 3 && $id > 0 ){
		$tab = array("rep" => "ok", "name" => $name, "groupe" => $groupe);
	}
	
	echo json_encode($tab);

}


// deplacement du fichier pour importation (date heure user )
if( isset($_POST['move']) && $_POST['move'] == true && !empty($_POST['file']) ){

	$tab = array("rep" => "no");
	$user = User::isLogin();
	$name = $user->getNomDB();
	$fichier = $_POST['file'];
	
	$tp = explode("_", $fichier);
	$tp_jour = $tp[1];
	$tp_heure = $tp[2];
	$jour = str_replace("-","/",$tp_jour);
	$heure = substr($tp_heure,0, -4);
	
	$dos = WEB."/".ADMIN_BAKCUP."php/files/".$fichier;

	$dir = BACKUP.$name;
	
	if(!is_dir($dir)) {
		mkdir($dir);
	}
	
	$save = $dos;
	$copy = $dir."/".$fichier;

	copy($save,$copy);
	$tab = array("rep" => "ok", "jour" => $jour, "heure" => $heure);
	
	echo json_encode($tab);

}

// effacement de la base de donnée
if( isset($_POST['vider']) && $_POST['vider'] == true ){

	$tab = array("rep" => "no");
	$user = User::isLogin();
	$name = $user->getNomDB();
	Mysql::connect();
	$sql = "DROP TABLE ".TABLES;
	Mysql::query($sql);
	$tab = array("rep" => "ok", "name" => $name);
	
	echo json_encode($tab);

}

// importation des données depuis le fichier (date heure user )
if( isset($_POST['insert']) && $_POST['insert'] == true && !empty($_POST['file']) && !empty($_POST['name'])){

	$tab = array("rep" => "no");
	$name = $_POST['name'];
	$fichier = $_POST['file'];
	$dir = BACKUP.$name;
	$up = $dir."/".$fichier;
	$cmd = "mysql --host=".HOST." --user=".USER." --password=".PASS." ".BASE." < ".$up;
	system($cmd);
	$tab = array("rep" => "ok");
	
	echo json_encode($tab);

}

// suppression du dossier temporaire
if( isset($_POST['delete']) && $_POST['delete'] == true && !empty($_POST['file']) ){

	$tab = array("rep" => "no");
	$user = User::isLogin();
	$name = $user->getNomDB();
	$fichier = $_POST['file'];
	$file_temp = WEB."/".ADMIN_BAKCUP."php/files/".$fichier;
	$dos = BACKUP.$name;
	unlink($file_temp);
	unlink($dos."/".$fichier);
	rmdir($dos);
	$url = "http://".$_SERVER['SERVER_NAME']."/";

	$tab = array("rep" => "ok", "url" => $url );
	
	echo json_encode($tab);

}
?>

