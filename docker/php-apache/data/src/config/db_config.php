<?php
/*
* Fichier de configuration pour la base de donnée Mysql  
* Fichier crée le 06/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

/* INFORMATION DE CONNECTION */
$host = 'mariadb';
$user = 'opmd';
$password = 'opmd';
$db = 'opmd';
$table_prefix = '';
$port = '3306';
$dsn = 'mysql:host='.$host.';dbname='.$db;

/* VARIABLE GLOBALE DE CONNECTION */
define('HOST',$host);
define('USER',$user);
define('PASS',$password);
define('BASE',$db);
define('PORT',$port);
define('DSN',$dsn);
define('PREFIX',$table_prefix);

/* VARIABLE GLOBALE DES TABLES */
define('T_USER', PREFIX.'users');
define('T_GROUPS', PREFIX.'groups');
define('T_AFFECTATION_POSTE', PREFIX.'affectation_poste');
define('T_CENTRE', PREFIX.'centre');
define('T_COMMUNE', PREFIX.'commune');
define('T_ETAT_VEHICULE', PREFIX.'etat_vehicule');
define('T_VEHICULE', PREFIX.'vehicule');
define('T_GRADE', PREFIX.'grade');
define('T_GROUPES', PREFIX.'groupes');
define('T_HORAIRE', PREFIX.'horaire');
define('T_INTERVENTION', PREFIX.'intervention');
define('T_INTERVENTION_PRESENCE_EXTERNE', PREFIX.'intervention_presence_externe');
define('T_NATURE_INTERVENTION', PREFIX.'nature_intervention');
define('T_MATERIEL', PREFIX.'materiel');
define('T_PERMANENCE', PREFIX.'permanence');
define('T_PERSONNEL', PREFIX.'personnel');
define('T_POSTE_VEHICULE', PREFIX.'poste_vehicule');
define('T_PRECISION_INTERVENTION', PREFIX.'precision_intervention');
define('T_PRESENCE_EXTERNE', PREFIX.'presence_externe');
define('T_SITUATION', PREFIX.'situation');

$tables = T_AFFECTATION_POSTE.",".T_CENTRE.",".T_COMMUNE.",".T_ETAT_VEHICULE.",".T_VEHICULE.",".T_GRADE.",".T_GROUPES.",".T_HORAIRE.",".T_INTERVENTION.",".T_INTERVENTION_PRESENCE_EXTERNE.",".T_NATURE_INTERVENTION.",".T_MATERIEL.",".T_PERMANENCE.",".T_PERSONNEL.",".T_POSTE_VEHICULE.",".T_PRECISION_INTERVENTION.",".T_PRESENCE_EXTERNE.",".T_SITUATION.",".T_USER.",".T_GROUPS;

define('TABLES',$tables);

?>