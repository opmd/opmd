<?php

/*
* Chargement des class PHP 
* Fichier crée le 06/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

function chargerClasse($classe)
{
    if(!defined('CLASSES_DIR')) {
        define('CLASSES_DIR', CLASSES);
    }
    
    set_include_path(
        CLASSES_DIR . PATH_SEPARATOR . get_include_path()
    );
    require $classe . '.php'; // On inclut la classe correspondante au paramètre passé.
}

spl_autoload_register('chargerClasse');


?>