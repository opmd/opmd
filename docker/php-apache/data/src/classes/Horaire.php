<?php

/*
* Fichier de class Horaire
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class Horaire extends Enregistrement {
	//attributs
	private $personnel;
	private $arrivee;
	private $depart;
	
	//constructeur
	public function __construct($id) {
		parent::__construct(T_HORAIRE, $id);
		if($this->row) {
			$this->personnel = $this->row['personnel'];
			$this->arrivee = $this->row['arrivee'];
			$this->depart = $this->row['depart'];
		}
	}
	
	//accesseurs get
	public function getPersonnel($raw = false) { return $raw ? $this->personnel : new Personnel($this->personnel); }
	public function getArrivee() { return $this->arrivee; }
	public function getDepart() { return $this->depart; }
	
	//accesseurs set
	public function setPersonnel($v = null) { $this->personnel = $v; }
	public function setArrivee($v = null) { $this->arrivee = $v; }
	public function setDepart($v = null) { $this->depart = $v; }
	
	public static function finiPermanenceCI(){
		Mysql::Connect();
		$depart = time();
		$query = "update ".T_HORAIRE." set depart = ? WHERE depart is null";
		$result = Mysql::query($query,$depart);	
		return "ok";
	}
	//commit
	public function commit() {
		if($this->notExists()) {
			$query = "insert into ".T_HORAIRE."(personnel, arrivee, depart) values(?,?,?)";
			$result = Mysql::query($query,$this->personnel,$this->arrivee,$this->depart);	
		} else {
			$query = "update ".T_HORAIRE."set personnel = ?, arrivee = ?, depart = ? where id = ?";
			$result = Mysql::query($query,$this->personnel,$this->arrivee,$this->depart,$this->id);
		}
	}
}

?>