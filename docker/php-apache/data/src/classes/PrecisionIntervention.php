<?php

/*
* Fichier de class PrecisionIntervention
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class PrecisionIntervention extends Enregistrement {
	//attributs
	private $libelle;
	private $nature;
	
	//constructeur
	public function __construct($id) {
		parent::__construct(T_PRECISION_INTERVENTION, $id);
		if($this->row) {
			$this->libelle = $this->row['libelle'];
			$this->nature = $this->row['nature'];
		}
	}
	
	//accesseurs get
	public function getLibelle() { return $this->libelle; }
	public function getNature($raw = false) { return $raw ? $this->nature : new NatureIntervention($this->nature); }
	
	//accesseurs set
	public function setLibelle($libelle = null) { $this->libelle = $libelle; }
	public function setNature($nature = null) { $this->nature = $nature; }
	
	//accesseurs bonus
	public static function getAllPrecisions() {
		$all = null;
		$query = 'select id from '.T_PRECISION_INTERVENTION.' order by libelle asc';
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = new PrecisionIntervention($row['id']);
		}
		return $all;
	}

	//commit
	public function commit() {
		if($this->notExists()) {
			$query = "insert into ".T_PRECISION_INTERVENTION."(libelle, nature) values(?, ?)";
			$result = Mysql::query($query,$this->libelle,$this->nature);
		} else {
			$query = "update ".T_PRECISION_INTERVENTION." set libelle = ?, nature = ? where id = ?";
			$result = Mysql::query($query,$this->libelle,$this->nature,$this->id);
		}
	}

	//delete
    public function delete() {
		if($this->exists()) {
		  $query = "delete from ".T_PRECISION_INTERVENTION." where id=?";
		  $result = Mysql::query($query,$this->id);
		}
	  }
}

?>