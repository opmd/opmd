<?php

/*
* Fichier de class Mysql et ErreurSQL
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class Mysql  
{  

	private static $bdd;
	
	private function __construct($dsn, $user, $pswd, $options = null)  
	{  
		$dsn = DSN;
		$user = USER;
		$pswd = PASS;
		try  
		{  
			self::$bdd = new PDO($dsn, $user, $pswd, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
			self::$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
		}  
		catch(PDOException $e)  
		{  
			echo "<pre style='background-color:yellow'>";
			print_r($e);
			echo "</pre>";
		}  
	}  

	public static function Connect($options = null)  
	{  
		if(!self::$bdd)  
		{  
			$dsn = DSN;
			$user = USER;
			$pswd = PASS;
			try{
				new self($dsn, $user, $pswd, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
			} catch (PDOException $e) {
				echo "<pre style='background-color:yellow'>";
				print_r($e);
				echo "</pre>";
			}
			
		}  
		return self::$bdd;  
	}  
	
	public static function query($query){ //secured query with prepare and execute
		$args = func_get_args();
		array_shift($args); //first element is not an argument but the query itself, should removed
		try {
			$reponse = self::$bdd->prepare($query);
			$reponse->execute($args);
		} catch (PDOException  $e) {
			echo "<pre style='background-color:yellow'>";
			print_r($e);
			echo "</pre>";
		}
        return $reponse;
    }
		
} 


?>
	