<?php

/*
* Fichier de class PresenceExterne
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class PresenceExterne extends Enregistrement {
	//attributs
	private $libelle;
	
	//constructeur
	public function __construct($id) {
		parent::__construct(T_PRESENCE_EXTERNE, $id);
		if($this->row) {
			$this->libelle = $this->row['libelle'];
		}
	}
	
	//accesseurs get
	public function getLibelle() { return $this->libelle; }
	
	//accesseurs set
	public function setLibelle($libelle = null) { $this->libelle = $libelle; }
	
	//accesseurs bonus
	public function getPrecisions() {
		$all = null;
		$query = "select id from ".T_PRECISION_INTERVENTION." where nature=? order by libelle asc";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = new PrecisionIntervention($row['id']);
		}
		return $all;
	}
	
	public static function getAllPresencesExternes() {
		$all = null;
		Mysql::Connect();
		$query = 'select id from '.T_PRESENCE_EXTERNE.' order by libelle asc';
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = new PresenceExterne($row['id']);
		}
		return $all;
	}
	
	//commit
	public function commit() {
		if($this->notExists()) {
			$query = "insert into ".T_PRESENCE_EXTERNE."(libelle) values(?)";
			$result = Mysql::query($query,$this->libelle);
		} else {
			$query = "update ".T_PRESENCE_EXTERNE."set libelle = ? where id = ?";
			$result = Mysql::query($query,$this->libelle,$this->id);
		}
	}
	
	public static function getQuantite($id_inter,$id_pe) {
		$quantite = 0;
		$query = "select * from ".T_INTERVENTION_PRESENCE_EXTERNE." where id_intervention = ? and id_presence_externe = ?";
		$result = Mysql::query($query,$id_inter,$id_pe);
		while($row = $result->fetch()) {
			$quantite = $row['quantite'];
		}
		return $quantite;
	}
	
	public static function setQuantite($id_inter,$id_pe,$nb) {
		$query = "update ".T_INTERVENTION_PRESENCE_EXTERNE." set quantite = ? where id_intervention = ? and id_presence_externe = ?";
		$result = Mysql::query($query,$nb,$id_inter,$id_pe);
	}
	
	public static function delQuantite($id) {
		$query = "delete from ".T_INTERVENTION_PRESENCE_EXTERNE." where id_intervention=?";
		$result = Mysql::query($query,$id);
	}
}

?>