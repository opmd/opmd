<?php
/**
 * \brief Core/Debugs : Système de gestion pour le débogage
 * \author Denis Sanson
 * \copyright Multixvers
 * \version PHP 7.0+
 */

final class Debug {
	
	/*
		S = degug la session
		C = debug les Cookies
		D = degug les data
		P = debug les Post
		G = debug les Get
		F = debug les Files
		SERV = debug le Server
		I = phpinfo
    OS = version du système d'exploitation
	*/

  /**
   * Sert à afficher le débogage
   * @param mixed $data Donnée a déboger
   */
  private static function affiche($data){
    if(DEBUG){
			echo "<pre>";
			print_r($data);
			echo "</pre>";
		}
  }
  
  /**
   * Debug la variable super globale $_SESSION
   **/
	public static function S(){
			self::affiche($_SESSION);
	}
	
  /**
   * Debug la variable super globale $_COOKIE
   **/
	public static function C(){
    self::affiche($_COOKIE);
	}
	
  /**
   * Fonction de Débogage
   * @param mixed $data donnée a afficher 
   **/
	public static function D($data){
    self::affiche($data);
	}
	
  /**
   * Debug la variable super globale $_POST
   **/
	public static function P(){
    self::affiche($_POST);
	}
	
  /**
   * Debug la variable super globale $_GET
   **/
	public static function G(){
    self::affiche($_GET);
	}
	
  /**
   * Debug la variable super globale $_SERVER
   **/
	public static function SERV(){
    self::affiche($_SERVER);
	}
	
  /**
   * Debug la variable super globale $_FILES
   **/
	public static function F(){
    self::affiche($_FILES);
	}
  
  /**
   * @return string le système d'exploitation windows ou linux
   **/
	public static function OS(){
		$os = "";
		if(DEBUG){
			$os = ( preg_match("/^[A-Z\:]{1}/",$_SERVER['DOCUMENT_ROOT']) == 1 ) ? "windows" : "linux";
		}
		return $os;
	}
	
  /**
   * Affiche le phpinfo si DEBUG à true
   **/
	public static function I(){
		if(DEBUG){
			phpinfo();
		}
	}

}
