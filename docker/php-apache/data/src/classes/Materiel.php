<?php

/*
* Fichier de class Materiel
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class Materiel extends Enregistrement {
	//attributs
	private $libelle;
	private $intervention;
	private $etat;
	private $centre;
	private $_postes;
	private $groupe;
	private $visible;
	private $quantite;
	private $utiliser;
	private $dispo;
	
	//constructeur
	public function __construct($id) {
		parent::__construct(T_MATERIEL, $id);
		$this->etat = 10;
		if($this->row) {
			$this->libelle = $this->row['libelle'];
			$this->intervention = $this->row['intervention'];
			$this->etat = $this->row['etat'];
			$this->centre = $this->row['centre'];
			$this->groupe = $this->row['groupe'];
			$this->visible = $this->row['visible'];
			$this->quantite = $this->row['quantite'];
			$this->utiliser = $this->row['utiliser'];
			$this->dispo = $this->row['dispo'];
		} else {
			$this->quantite = 0;
			$this->utiliser = 0;
			$this->dispo = 0;
      $this->intervention = null;
		}
	}
	
	//accesseurs get
	public function getLibelle() { return $this->libelle; }
	public function getIntervention() { return $this->intervention; }
	public function getEtat() { return new EtatVehicule($this->etat); }
	public function getCentre($raw = false) { return $raw ? $this->centre : new Centre($this->centre); }
	public function getGroupe($raw = false) { return $raw ? $this->groupe : new Groupes($this->groupe); }
	public function getVisible() { return $this->visible; }
	public function getQuantite() { return $this->quantite; }
	public function getUtiliser() { return $this->utiliser; }
	public function getDispo() { return $this->dispo; }

	
	//accesseurs set
	public function setLibelle($libelle = null) { $this->libelle = $libelle; }
	public function setIntervention($intervention = null) { $this->intervention = $intervention; }
	public function setEtat($etat = null) { $this->etat = $etat; }
	public function setCentre($centre = null) { $this->centre = $centre; }
	public function setPostes($postes = null) { $this->_postes = $postes; }
	public function setGroupe($groupe = null) { $this->groupe = $groupe; }
	public function setVisible($visible = null) { $this->visible = $visible; }
	public function setQuantite($quantite = null) { $this->quantite = $quantite; }
	public function setUtiliser($utiliser = null) { $this->utiliser = $utiliser; }
	public function setDispo($dispo = null) { $this->dispo = $dispo; }

	
	//accesseurs bonus
	/*public function getPersonnel($raw = false) {
		$all = null;
		$query = "select personnel from ".T_AFFECTATION_POSTE." where vehicule = ? and personnel <> 0";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = $raw ? $row['personnel'] : new Personnel($row['personnel']);
		}
		return $all;
	}
	
	public function getPostes($raw = false) {
		$all = null;
		$query = "select poste from ".T_AFFECTATION_POSTE." where vehicule=?";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = $raw ? $row['poste'] : new PosteVehicule($row['poste']);
		}
		return $all;
	}
	
	public function getAffectations($raw = false) {
		$all = null;
		$query = "select poste, personnel from ".T_AFFECTATION_POSTE." where vehicule=?";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[$row['poste']] = $row['personnel'];
		}
		return $all;
	}*/
  
  public function getNbByInter($codis) {
		$nb = 0;
    $inters = $this->getIntervention();
    if($inters != null){
      $tmp = explode(";",$inters);
      foreach($tmp as $t){
        $parts = explode(":",$t);
        $c1 = $parts[0];
        $n1 = $parts[1];
        if($codis == $c1){
          $nb =  $n1;
        }
      }
    }
		return $nb;
	}
	
	public function contient($id_personnel = -1) {
		$b = false;
		$query = "select 1 from ".T_AFFECTATION_POSTE." where vehicule=? and personnel=?";
		$result = Mysql::query($query,$this->id,$id_personnel);
		if($result->fetch()) {
			$b = true;
		}
		return $b;
	}
  
  public function extractInter(){
    $all = null;
    if(!empty($this->getIntervention())){
      $ls = explode(";",$this->getIntervention());
      if(!empty($ls[0])){
        for($i = 0; $i < count($ls); $i++){
          $t = explode(":",$ls[$i]);
          $all[] = $t[0];
        }
      }
    }
    return $all;
  }
  
  public function extractNbInter(){
    $all = null;
    $nb = 0;
    if(!empty($this->getIntervention())){
      $ls = explode(";",$this->getIntervention());
      if(!empty($ls[0])){
        for($i = 0; $i < count($ls); $i++){
          $t = explode(":",$ls[$i]);
          $all[] = intval($t[1]);
        }
      }
    }
    if($all != null){
      for($i=0; $i < count($all); $i++){
        $nb += $all[$i];
      }
    }
    return $nb;
  }
	
	public function setAffectation($id_poste = -1, $id_personnel = -1) {
		$query = "update ".T_AFFECTATION_POSTE." set personnel = ? where vehicule = ? and poste = ?";
		$result = Mysql::query($query,$id_personnel,$this->id,$id_poste);

		$query = "update ".T_AFFECTATION_POSTE." set personnel = 0 where personnel = ? and vehicule <> ? and poste <> ?";
		$result = Mysql::query($query,$id_personnel,$this->id,$id_poste);
	}
	
	public static function viderAllMateriel($centre = '-1') {
		Mysql::Connect();
		$query = "update ".T_AFFECTATION_POSTE." a, ".T_MATERIEL." v set a.personnel = null where a.vehicule = v.id and v.centre = ?";
		$result = Mysql::query($query,$centre);
	}
	
	//commit
	public function commit() {
		if($this->notExists()) {
			$query = "insert into ".T_MATERIEL."(libelle, intervention, etat, centre, groupe, visible, quantite, utiliser, dispo) values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
			$result = Mysql::query($query,$this->libelle,$this->intervention,$this->etat,$this->centre,$this->groupe,$this->visible,$this->quantite,$this->utiliser,$this->dispo);
			$this->id = $this->bdd->lastInsertId(); 
		} else {
			$query = "update ".T_MATERIEL." set libelle = ?, intervention = ?, etat = ?, centre = ?, groupe = ?, visible = ?, quantite = ?, utiliser = ?, dispo = ? where id = ?";
			$result = Mysql::query($query,$this->libelle,$this->intervention,$this->etat,$this->centre,$this->groupe,$this->visible,$this->quantite,$this->utiliser,$this->dispo,$this->id);
		}
		
		if($this->exists()) {
			//postes
			if(is_array($this->_postes)) {
				//get personnel before changes
				$personnel = array();
				$query = "select poste, personnel from ".T_AFFECTATION_POSTE." where vehicule=?";
				$result = Mysql::query($query,$this->id);
				while($row = $result->fetch()) {
					$personnel[$row['poste']] = $row['personnel'];
				}
				
				//delete all postes	
				$query = "delete from ".T_AFFECTATION_POSTE." where vehicule=?";
				$result = Mysql::query($query,$this->id);
			
				//add new ones with eventual old value
				foreach($this->_postes as $poste) {
					if($poste > 0) {
						$query = "insert into ".T_AFFECTATION_POSTE."(vehicule, poste, personnel) values(?, ?, ?)";
						$result = Mysql::query($query,$this->id,$poste,$personnel[$poste]);
					}
				}
			}
		}
		
		
	}
	
	//delete
	public function delete() {
		if($this->exists()) {
			//affectations
			$query = "delete from ".T_AFFECTATION_POSTE." where vehicule=?";
			$result = Mysql::query($query,$this->id);
		
			$query = "delete from ".T_MATERIEL." where id=?";
			$result = Mysql::query($query,$this->id);	
		}
	}
  
  public function updateQuantiter(){
    $total = $this->getQuantite();
    $inters = $this->getIntervention();
    if($inters == null){
      $this->setDispo($total);
      $this->setUtiliser(0);
      $this->setEtat(10);
    } else {
      $x = $this->extractNbInter();
      $y = $total - $x;
      if($x == $total){
        $this->setEtat(1);
        $this->setDispo(0);
        $this->setUtiliser($total);
      } else if($y == $total) {
        $this->setEtat(10);
        $this->setDispo($total);
        $this->setUtiliser(0);
      } else {
        if($x + $y == $total){
          $this->setEtat(1);
          $this->setDispo($y);
          $this->setUtiliser($x);
        }
      }
    }
    $this->commit();
  }
  
  public function addInter($codis,$nb){
    $inters = $this->getIntervention();
    $id = $this->getId();
    if($inters != null){
      if($this->getNbByInter($codis) == 0){
        $rep = $inters .";". $codis .":". $nb;
      } else {
        $x = $codis . ":" . $this->getNbByInter($codis);
        $y = $codis . ":" . $nb;
        $rep = str_replace($x, $y, $inters);
      }
    } else {
      $rep = $codis .":". $nb;
    }
    $this->setIntervention($rep);
    $this->commit();
    $this->updateQuantiter();
  }
  
  public function removeInter($codis){
    $inters = $this->getIntervention();
    $id = $this->getId();
    $x = null;
    // si le materiel a une ou plusieurs intervention
    if($inters != null){
      $tmp = explode(";",$inters);
      foreach($tmp as $t){
        $parts = explode(":",$t);
        $c1 = intval($parts[0]);
        $n1 = intval($parts[1]);
        // cas ou l'inter n'est pas sur le matériel
        if($codis != $c1){
          $x[] = $c1 . ":" . $n1;
        }
      }
      // si il reste des inter pour le matériel
      if($x != null){
        $rep = implode(";",$x);
        // on mets a jours les inter restant sans l'inter retiré
        $this->setIntervention($rep);
      } else {
        // cas ou il y a plus d'inter dans le matériel
        $this->setIntervention(null);
      }
      // on mets a jours le matériel avec l'inter retiré
      $this->commit();
      $this->updateQuantiter();
    }
  }
  
  public static function updateInter($mat, $id){
    if($mat == null){
      $matId = null;
      $matNb = null;
      $total = 0;
    } else {
      $matId = $mat['id'];
      $matNb = $mat['utiliser'];
      $total = (is_array($matId)) ? count($matId) : 0;
    }
    
    $materiels = Materiel::getAllMaterielsByInter($id);
    $total = (is_array($matId)) ? count($matId) : 0;
    $rep = null;
    // delete
    if($materiels != null){
      // cas ou l'intervention a du matériel
      foreach($materiels as $m){
        // id du matériel possédant l'intervention
        $mai = $m->getId();
        $mi = $m->extractInter();
        // si l'inter n'as plus le matériel reçu
        if($matId != null){
          if(!in_array($mai,$matId)){
            $m->removeInter($id);
          }
        } else {
          $m->removeInter($id);
        }
        
      }
    }
    
    // add
    if($matId != null){
      // cas ou l'intervention n'as plus de matériel
      $a = 0;
      foreach($matId as $mi){
        // on ajoute l'inter au matériel
        $m = new Materiel($mi);
        $nb = intval($matNb[$a]);
        $inter_tmp = $m->extractInter();
        if($inter_tmp != null){
          if(!in_array($id,$inter_tmp)){
            $m->addInter($id,$nb);
          } 
        } else {
          $m->addInter($id,$nb);
        }
        $a++;
      }
    }
    
    // update
    if($materiels != null){
      // cas ou l'intervention a du matériel
      foreach($materiels as $m){
        // id du matériel possédant l'intervention
        $mai = $m->getId();
        // si l'inter déjà du matériel
        if($matId != null){
          if(in_array($mai,$matId)){
            // on parcourt le matériel reçu
            for( $i=0 ; $i < $total; $i++ ) {
              // on modifie le materiel possédant l'intervention
              if($matId[$i] == $mai){
                $nb1 = $m->getNbByInter($id);
                $nb2 = intval($matNb[$i]);
                $x = $id . ":" . $nb1;
                $y = $id . ":" . $nb2;
                $tmp = $m->getIntervention();
                $rep = ($tmp != null) ? str_replace($x, $y, $tmp) : $x . ":" . $y;
                $m->setIntervention($rep);
                $m->commit();
                $m->updateQuantiter();
              }
            }
          }
        }
        
      }
    }
  }
	
	public static function getAllMateriels($raw = false) {
		$all = null;
		$query = "select id from ".T_MATERIEL." order by id asc";
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] =  $raw ? $row['id'] : new Materiel($row['id']);
		}
		return $all;
	}
  
  public static function getAllMaterielsByInter($codis,$raw = false) {
		$all = null;
		$query = "select id from ".T_MATERIEL." order by id asc";
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
      $m = new Materiel($row['id']);
      $inters = $m->getIntervention();
      if($inters != null){
        $tmp = explode(";",$inters);
        foreach($tmp as $t){
          $parts = explode(":",$t);
          $c1 = intval($parts[0]);
          if($codis == $c1){
            $all[] =  $raw ? $c1 : $m;
          }
        }
      }
		}
		return $all;
	}
	
  
	public static function getListMateriels($raw = true) {
		$all = null;
		$query = "select `id`, `libelle`, `centre` from ".T_MATERIEL." order by libelle asc";
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] =  $raw ? array("id"=>$row['id'],"centre"=>$row['centre'],"libelle"=>$row['libelle']) : new Materiel($row['id']);
		}
		return $all;
	}
	
	public static function getMaterielByGroupOrderPosition() {
		$all = null;
		Mysql::Connect();
		$query = "select distinct m.groupe,g.libelle from ".T_MATERIEL." m join ".T_GROUPES." g on m.groupe = g.id order by g.position asc";
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] =  new Groupes($row['groupe']);
		}
		return $all;
	}
	
	public static function getAllMaterielsByGroup($groupe = 0, $centre = 1, $raw = false) {
		$all = null;
		Mysql::Connect();
		$query = "select * from ".T_MATERIEL." where `groupe`= ? order by `centre` asc";
		$result = Mysql::query($query,$groupe);
		while($row = $result->fetch()) {
			$all[] = $raw ? $row['id'] : new Materiel($row['id']);
		}
		return $all;
	}
}

?>