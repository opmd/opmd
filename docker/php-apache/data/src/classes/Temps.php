<?php
/**
 * \brief Core/Dates : Système de gestion des constante de temps
 * \author Denis Sanson
 * \copyright Multixvers
 * \version PHP 7.0+
 */

final class Temps {
	
	/*
		SECONDE = 1
    MINUTE = 60
    HEURE = 3600
    JOUR = 86400
		SEMAINE = 604800
		AN = 86400 (365 jours)
		AN_BIS = 86400 (366 jours)
	*/
  
  /**
   * Une seconde = 1
   */
  const SECONDE = 1;
  
  /**
   * Une minute soit 60 seconde = 60 secondes
   */
  const MINUTE = 60;
  
  /**
   * Une heure soit 60 seconde * 60 minutes = 3600 secondes
   */
  const HEURE = 3600;
  
  /**
   * Un jour soit 60 seconde * 60 minutes * 24 heures = 86400 secondes
   */
  const JOUR = 86400;

  /**
   * Une semaine soit 60 seconde * 60 minutes * 24 heures * 7 jours = 604800 secondes
   */
  const SEMAINE = 604800;
  
  /**
   * Une année soit 60 seconde * 60 minutes * 24 heures * 365 jours = 31536000 secondes
   */
  const AN = 86400;
  
  /**
   * Une année bisexitile soit 60 seconde * 60 minutes * 24 heures * 366 jours = 31622400 secondes
   */
  const AN_BIS = 86400;

}
