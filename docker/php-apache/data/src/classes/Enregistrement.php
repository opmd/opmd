<?php

/*
* Fichier de class Enregistrement
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class Enregistrement {
	//attributs
	protected $table;
	protected $id;
	protected $row;
	protected $bdd;
	
	//constructeur
	public function __construct($table, $id = -1, $id_field = 'id') {
		$this->table = $table;
		$this->id = -1;
		$this->bdd = Mysql::Connect();

		if(is_numeric($id) && $id != -1) {
			//get data from db
			$sql = "select * from {$table} where {$id_field} = {$id}";
			$req = $this->bdd->query($sql);
			$this->row = $req->fetch();
			if($this->row) {
				$this->id = $this->row[$id_field];	//useless, i know
			}
		}
	}
	
	public function getId() { return $this->id; }
	public function exists() { return !$this->notExists(); }
	public function notExists() {return $this->id == -1; }
	
	//commit
	public function commit() {}
	
	//delete
	public function delete() {}
}

?>