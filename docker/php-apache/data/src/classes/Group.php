<?php

/*
* Fichier de class Group
* Fichier crée le 01/05/2014
* Auteur : Denis Sanson alias Multixvers
*/

class Group extends Enregistrement {
    //attributs
    private $nom;

    //constructeur
    public function __construct($id) {
      parent::__construct(T_GROUPS, $id);
      if($this->row) {
        $this->nom = $this->row['nom'];
      }
    }
    
    //accesseurs get
    public function getNom() { return $this->nom; }
  
    //accesseurs set
    public function setNom( $nom = null) { $this->nom = $nom; }
    
    //accesseurs bonus
    public static function getAllGroups() {
      $all = null;
      $query = 'select id from '.T_GROUPS.' where id > 1 order by id';
      Mysql::Connect();
      $result = Mysql::query($query);
      while($row = $result->fetch()) {
        $all[] = new Group($row['id']);
      }
      return $all;
    }
    
    //commit
    public function commit() {
      if($this->notExists()) {
        $query = "insert into ".T_GROUPS."(nom) values(?)";
        $result = Mysql::query($query, $this->nom);
      } else {
        $query = "update ".T_GROUPS." set nom = ? where id = ?";
        $result = Mysql::query($query,$this->nom, $this->id);
      }
      
    }
    
    //delete
    public function delete() {
      if($this->exists()) {
        $query = "delete from ".T_GROUPS." where id=?";
        $result = Mysql::query($query,$this->id);
      }
    }
  
}

?>