<?php

/*
* Fichier de class Permanence
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class Permanence extends Enregistrement {
	//attributs
	private $tmp_id;
	private $nature;
	private $debut;
	private $fin;
	
	//constructeur
	public function __construct($id) {
		parent::__construct(T_PERMANENCE, $id);
		if($this->row) {
			$this->nature = $this->row['nature'];
			$this->debut = $this->row['debut'];
			$this->fin = $this->row['fin'];
		} else {
			$this->tmp_id = $id;
		}
	}
	
	//accesseurs get
	public function getNature() { return $this->nature; }
	public function getDebut() { return $this->debut; }
	public function getFin() { return $this->fin; }
	
	//accesseurs set
	public function setNature($v = null) { $this->nature = $v; }
	public function setDebut($v = null) { $this->debut = $v; }
	public function setFin($v = null) { $this->fin = $v; }
	
	//accesseurs bonus
	public static function getAllPermanences() {
		$all = null;
		$query = 'select id from '.T_PERMANENCE.' order by id asc';
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = new Permanence($row['id']);
		}
		return $all;
	}
	
	public static function getCurrentPermanence() {
		$curr_id = '';
		$query = 'select id from '.T_PERMANENCE.' where fin is null';
		Mysql::Connect();
		$result = Mysql::query($query);
		if($row = $result->fetch()) {
			$curr_id = $row['id'];
		}
		return new Permanence($curr_id);
	}
	
	public static function isTimeOverlappingPermanence($dt = 0) {
		$nb = 0;
		$query = "select p.id from ".T_PERMANENCE." p where p.debut <= ? and ? <= p.fin";
		Mysql::Connect();
		$result = Mysql::query($query,$dt,$dt);
		$nb = $result->fetchColumn();
		return ($nb != 0);
	}
	
	public function getHorairesRenforts() {
		$all = null;
		$query = "select h.id from ".T_PERMANENCE." p, ".T_HORAIRE." h where p.id = ? and (p.debut <= h.arrivee and ifnull(h.depart, UNIX_TIMESTAMP()-1) <= ifnull(p.fin, UNIX_TIMESTAMP()) or ifnull(h.depart, UNIX_TIMESTAMP()) > p.debut and h.arrivee < ifnull(p.fin, UNIX_TIMESTAMP())) order by h.arrivee asc";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = new Horaire($row['id']);
		}
		return $all; 
	}
	
	//commit
	public function commit() {
		$fin = ($this->fin=='') ? null : $this->fin;
		if($this->notExists()) {
			$query = "insert into ".T_PERMANENCE."(id, nature, debut, fin) values(?, ?, ?, ?)";
			$result = Mysql::query($query,$this->tmp_id,$this->nature,$this->debut,$fin);
			$this->id = $this->bdd->lastInsertId(); 
		} else {
			$query = "update ".T_PERMANENCE." set nature = ?, debut = ?, fin = ? where id = ?";
			$result = Mysql::query($query,$this->nature,$this->debut,$fin,$this->id);
		}
	}
}

?>