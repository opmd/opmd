<?php
/**
 * \brief : Système de gestion pour générer les PDF, les télécharger et les imprimer
 * \author Denis Sanson
 * \copyright Multixvers
 * \version PHP 7.0+
 */
final class Pdf {
		
  public static function savePDF($html,$dir,$file){
    // on créer le PDF depuis le HTML
    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $options = array(
      "enable_unicode"           => DOMPDF_UNICODE_ENABLED,
      "enable_php"               => DOMPDF_ENABLE_PHP,
      "enable_remote"            => DOMPDF_ENABLE_REMOTE,
      "enable_css_float"         => DOMPDF_ENABLE_CSS_FLOAT,
      "enable_javascript"        => DOMPDF_ENABLE_JAVASCRIPT,
      "enable_html5_parser"      => DOMPDF_ENABLE_HTML5PARSER,
      "enable_font_subsetting"   => DOMPDF_ENABLE_FONTSUBSETTING
     );
    $dompdf->set_options($options);
    $dompdf->render();
    $pdf = $dompdf->output();
    file_put_contents($dir.$file,$pdf);
  }


  public static function genererPDF($html){

    // on créer le PDF depuis le HTML
    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $options = array(
      "enable_unicode"           => DOMPDF_UNICODE_ENABLED,
      "enable_php"               => DOMPDF_ENABLE_PHP,
      "enable_remote"            => DOMPDF_ENABLE_REMOTE,
      "enable_css_float"         => DOMPDF_ENABLE_CSS_FLOAT,
      "enable_javascript"        => DOMPDF_ENABLE_JAVASCRIPT,
      "enable_html5_parser"      => DOMPDF_ENABLE_HTML5PARSER,
      "enable_font_subsetting"   => DOMPDF_ENABLE_FONTSUBSETTING
     );
    $dompdf->set_options($options);
    $dompdf->render();
    $pdf = $dompdf->output('','S');

    return $pdf;  
  }

  public static function afficher($dir,$file, $js = false){
    $pdf = $dir.$file;
    if($js == true){
      return URL.$pdf;
    } else {
      header('Content-type: application/pdf');
      readfile($pdf);
    }
  }

  public static function imprimer($dir,$file, $js = false){
    $pdf = $dir.$file;
    if($js == true){
      return URL.$pdf;
    } else {
      $code = '<iframe id="printPDF" src='.$pdf.' width="100%" no-border="true" height="100%" hidden="hidden"></iframe>';
      $code .= '<script type="text/javascript">document.getElementById("printPDF").contentWindow.print();</script>';
      echo $code;
    }
  }

  public static function telecharger($dir,$file, $js = false){
    $pdf = $dir.$file;
    if($js == true){
      return URL.$pdf;
    } else {
      header('Content-type: application/pdf');
      header('Content-Disposition: attachment; filename="'.$file.'"');
      readfile($pdf);
    }
  }
    
    
}

