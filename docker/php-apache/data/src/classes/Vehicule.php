<?php

/*
* Fichier de class Vehicule
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class Vehicule extends Enregistrement {
	//attributs
	private $libelle;
	private $intervention;
	private $etat;
	private $centre;
	private $_postes;
	private $groupe;
	private $visible;
	
	//constructeur
	public function __construct($id) {
		parent::__construct(T_VEHICULE, $id);
		
		$this->etat = 10;
		if($this->row) {
			$this->libelle = $this->row['libelle'];
			$this->intervention = $this->row['intervention'];
			$this->etat = $this->row['etat'];
			$this->centre = $this->row['centre'];
			$this->groupe = $this->row['groupe'];
			$this->visible = $this->row['visible'];
		}
	}
	
	//accesseurs get
	public function getLibelle() { return $this->libelle; }
	public function getIntervention($raw = false) { return $raw ? $this->intervention : new Intervention($this->intervention); }
	public function getEtat() { return new EtatVehicule($this->etat); }
	public function getCentre($raw = false) { return $raw ? $this->centre : new Centre($this->centre); }
	public function getGroupe($raw = false) { return $raw ? $this->groupe : new Groupes($this->groupe); }
	public function getVisible() { return $this->visible; }
	
	//accesseurs set
	public function setLibelle($libelle = null) { $this->libelle = $libelle; }
	public function setIntervention($intervention = null) { $this->intervention = $intervention; }
	public function setEtat($etat = null) { $this->etat = $etat; }
	public function setCentre($centre = null) { $this->centre = $centre; }
	public function setPostes($postes = null) { $this->_postes = $postes; }
	public function setGroupe($groupe = null) { $this->groupe = $groupe; }
	public function setVisible($visible = null) { $this->visible = $visible; }
	
	//accesseurs bonus
	public function getPersonnel($raw = false) {
		$all = null;
		$query = "select personnel from ".T_AFFECTATION_POSTE." where vehicule = ? and personnel <> 0";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = $raw ? $row['personnel'] : new Personnel($row['personnel']);
		}
		return $all;
	}
	
	public function getPostes($raw = false) {
		$all = null;
		$query = "select poste from ".T_AFFECTATION_POSTE." where vehicule=?";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = $raw ? $row['poste'] : new PosteVehicule($row['poste']);
		}
		return $all;
	}
	
	public function getAffectations($raw = false) {
		$all = null;
		$query = "select poste, personnel from ".T_AFFECTATION_POSTE." where vehicule=?";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[$row['poste']] = $row['personnel'];
		}
		return $all;
	}
	
	public function contient($id_personnel = -1) {
		$b = false;
		$query = "select 1 from ".T_AFFECTATION_POSTE." where vehicule=? and personnel=?";
		$result = Mysql::query($query,$this->id,$id_personnel);
		if($result->fetch()) {
			$b = true;
		}
		return $b;
	}
	
	public function setAffectation($id_poste = -1, $id_personnel = -1) {
		$msg = "ok";
		$query = "update ".T_AFFECTATION_POSTE." set personnel = ? where vehicule = ? and poste = ?";
		$result = Mysql::query($query,$id_personnel,$this->id,$id_poste);
		$query = "update ".T_AFFECTATION_POSTE." set personnel = 0 where personnel = ? and vehicule <> ? and poste <> ?";
		$result = Mysql::query($query,$id_personnel,$this->id,$id_poste);
		return $msg;;
	}
	
	public static function viderAllVehicules($centre = '-1') {
		Mysql::Connect();
		$query = "update ".T_AFFECTATION_POSTE." a, ".T_VEHICULE." v set a.personnel = null where a.vehicule = v.id and v.centre = ?";
		$result = Mysql::query($query,$centre);
	}
	
	//commit
	public function commit() {
		
		if($this->notExists()) {	
				$query = "insert into ".T_VEHICULE." (libelle, intervention, etat, centre, groupe, visible) values(?, ?, ?, ?, ?, ?)";
				$result = Mysql::query($query,$this->libelle,$this->intervention,$this->etat,$this->centre,$this->groupe,$this->visible);
				$this->id = $this->bdd->lastInsertId(); 
		} else {
			$query = "update ".T_VEHICULE." set libelle = ?, intervention = ?, etat = ?, centre = ?, groupe = ?, visible = ? where id = ?";
			$result = Mysql::query($query,$this->libelle,$this->intervention,$this->etat,$this->centre,$this->groupe,$this->visible,$this->id);	
		}
		
		if($this->exists()) {
			//postes
			if(is_array($this->_postes)) {
				// get personnel before change
				$personnel = array();
				$query = "select poste, personnel from ".T_AFFECTATION_POSTE." where vehicule=?";
				$result = Mysql::query($query,$this->id);
				while($row = $result->fetch()) {
					$personnel[$row['poste']] = $row['personnel'];
				}
				
				$query = "delete from ".T_AFFECTATION_POSTE." where vehicule=?";
				$result = Mysql::query($query,$this->id);
				
				//add new ones with eventual old value
				foreach($this->_postes as $poste) {
					if($poste > 0) {
						$query = "insert into ".T_AFFECTATION_POSTE."(vehicule, poste, personnel) values(?, ?, ?)";
						$result = Mysql::query($query,$this->id,$poste,$personnel[$poste]);
					}
				}
			}
		}
		
		
	}
	
	//delete
	public function delete() {
		if($this->exists()) {
			$query = "delete from ".T_AFFECTATION_POSTE." where vehicule=?";
			$result = Mysql::query($query,$this->id);

			$query = "delete from ".T_VEHICULE." where id=?";
			$result = Mysql::query($query,$this->id);
		}
	}
	
	public static function getAllVehiculesGrouper($raw = false) {
		$all = null;
		Mysql::Connect();
		$query = "select * from ".T_VEHICULE." order by `groupe` asc";
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] =  $raw ? $row['id'] : new Vehicule($row['id']);
		}
		return $all;
	}
	
	public static function getVehiculeByGroupOrderPosition() {
		$all = null;
		Mysql::Connect();
		$query = "select distinct v.groupe,g.libelle from ".T_VEHICULE." v join ".T_GROUPES." g on v.groupe = g.id order by g.position asc";
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] =  new Groupes($row['groupe']);
		}
		return $all;
	}
	
	
	public static function getAllVehiculesByGroup($groupe = 1, $centre = 1, $raw = false) {
		$all = null;
		Mysql::Connect();
		$query = "select * from ".T_VEHICULE." where `groupe`= ? order by `centre` asc";
		$result = Mysql::query($query,$groupe);
		while($row = $result->fetch()) {
			$all[] = $raw ? $row['id'] : new Vehicule($row['id']);
		}
		return $all;
	}
	
	
	
	public static function getAllVehicules($raw = false) {
		$all = null;
		Mysql::Connect();
		$query = "select * from ".T_VEHICULE." order by centre, libelle asc";
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
		//$all[] = $raw ? array("id"=>$row['id'],"centre"=>$row['centre'],"groupe"=>$row['groupe']) : new Vehicule($row['id']);
			$all[] = new Vehicule($row['id']);
		}
		return $all;
	}
	
}

?>